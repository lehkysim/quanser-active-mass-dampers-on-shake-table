    % SETUP_STII_2xAMD1
%
% Shake Table II + Active Mass Damper Two-Floor, Two-Cart (2xAMD-1)
% Vibration Control Lab: Design of an observer-based state-feedback Controller
% 
% SETUP_STII_2xAMD1 sets the AMD-1 system model parameters accordingly to 
% the user-defined configuration and can also set the controller 
% parameters.
%
% Copyright (C) 2017 Quanser Consulting Inc.
% Quanser Consulting Inc.
%
clear;
%
%% USER-DEFINED 2xAMD-1 CONFIGURATION
% Type of motorized cart: set to 'IP01', 'IP02'
CART_TYPE = 'IP02';
% Type of Cart Load: set to 'NO_WEIGHT', 'ONE_WEIGHT', 'TWO_WEIGHT'
CART_LOAD_TYPE = 'TWO_WEIGHT';
% Turn on or off the safety watchdog on the cart position: set it to 1 , or 0 
XC_LIM_ENABLE = 1;       % safety watchdog turned ON
%XC_LIM_ENABLE = 0;      % safety watchdog turned OFF
% Safety Limits on the cart displacement (m)
XC_MAX = 0.08;            % cart displacement maximum safety position
XC_MIN = - XC_MAX;        % cart displacement minimum safety position
% Amplifier Gain used: set according to Gain Switch on VoltPAQ
K_AMD_AMP = 1;
% Amplifier Type: set to 'VoltPAQ'
AMP_TYPE = 'VoltPAQ';
%
%% USER-DEFINED ST II CONFIGURATION
% Enter revision of Shake Table II: REV1, REV2, REV3, or REV4.
% CAUTION: Only change this if you know for sure what revision the shake
% table being used is!
STII_REV = 'REV4';
% 
%% USER-DEFINED CONTROLLER DESIGN
% Type of Controller: set it to 'AUTO', 'MANUAL'  
CONTROLLER_TYPE = 'AUTO';    % PV + observer + state-feedback controller design: automatic mode
%CONTROLLER_TYPE = 'MANUAL';    % controller design: manual mode
% Design a Proportional-Velocity (PV) controller with following specs:
PO = 10;         % spec #1: maximum of percent overshoot
tp = 0.15;     % spec #2: time of first peak (s)
% 
% State-feedback controller design: calculate K using LQR
% X = [xc1 xc2 xf1 xf2 xc1_dot xc2_dot xf1_dot xf2_dot]
Q = diag([50 15 250 220 1 0.3 250 200]);
R = [0.5 0; 0 0.35];
% desired full-order observer poles
OP = [ -55, -60, -65, -70, -75, -80, -85, -90 ] + 35; 
% 
%% SHAKE TABLE II PARAMETERS
% shake table II parameters
[SWEEP_MAX, P_MAX, VEL_MAX, ACC_MAX, VMAX_AMP, IMAX_AMP, wd, zd, wf, zf, wa, za, kp, kd, Kf, K_AMP, K_CURR, K_ENC, K_S0, K_S1, K_S2, K_MS2G, K_M2IN, K_G2MS, dbnc_threshold, dbnc_rise_time, hil_timeout ] = d_shake_table_II ();
% 
%% 2xAMD-1 MODEL
% initial state variables (at t=0)
X0 = [ 0; 0; 0; 0; 0; 0; 0; 0 ];
% Cart Encoder Resolution
global K_EC; %#ok
% Accelerometer gain (m/s^2)
global K_ACC; %#ok
% global At Bt Ct Dt Ab Bb Cb Db
[ Rm, Jm, Kt, Km, Kg, Mc, r_mp, Beq, Mf1, Kf1, Mf2, Kf2, VMAX_AMP_AMD, IMAX_AMP_AMD  ] = config_2xamd1( CART_TYPE, CART_LOAD_TYPE, AMP_TYPE );
Eff_m = 1;
Eff_g = 1;
% Design PV position control for carts (to keep them at a fixed position)
[ Kp, Kv ] = d_amd_pv( Rm, Jm, Kt, Eff_m, Km, Kg, Eff_g, Mc, r_mp, Beq, PO, tp );
% set state-space of 2xAMD-1 system
Two_AMD1_ABCD_eqns;
%
%% DISPLAY and CONTROL DESIGN
if strcmp ( CONTROLLER_TYPE, 'AUTO' )
    % PV controller design: automatic mode
    % Automatically design a Proportional-Velocity (PV) controller as per the user-specifications above
    % Automatically design the full-order observer and calculate the state-feedback gain vector as per the desired pole locations
    [ Kp_amd, Kv_amd, Kamd, G ] = d_2xamd1( Rm, Jm, Kt, Km, Kg, Mc, r_mp, Beq, PO, tp, A, B, C, D, OP, Q, R, X0 );
    % Display the calculated gains
    disp( ' ' )
    disp( 'Calculated PV controller gains: ' )
    disp( [ 'Kp = ' num2str( Kp_amd ) ' V/m' ] )
    disp( [ 'Kv = ' num2str( Kv_amd ) ' V.s/m' ] )
    disp( ' ' )
    disp( 'Calculated full-order observer matrix: ' )
    G %#ok
    disp( 'Calculated state-feedback gain vector: ' )
    Kamd %#ok
elseif strcmp ( CONTROLLER_TYPE, 'MANUAL' )
    disp( ' ' )
    disp( 'STATUS: manual mode' ) 
    disp( 'The model parameters of your AMD-2 system have been set.' )
    disp( 'You can now design your control system.' )
    disp( ' ' )
else
    error( 'Error: Please set the type of controller that you wish to implement.' )
end
% 
%% USER-DEFINED ROBUST CONTROLLER DESIGN
% model uncertainty
tau = 5;
r0 = 0.1;
r_inf = 2;
W = tf([tau, r0],[tau/r_inf, 1]);
% nominal performance
omega = 20;
a = 1e-6;
M = 2;
mag = 1e-3;
Wp = makeweight(a,[2*pi*omega,mag],M);
% input uncertainty
% tau = 0;
% r0 = 0;
% r_inf = 0;
% Wu = tf([tau, r0],[tau/r_inf, 1]);
Wu = 0;
%
sys = ss(A,B,C,D);
[K_mix_syn,~] = mixsyn(sys,W,Wu,Wp);
%