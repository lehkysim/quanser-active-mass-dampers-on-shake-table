
# Quanser Active Mass Dampers on Shake Table
This is a repository of the **Active Mass Dampers on Shake Table** from Quanser containing all necessary files and instructions on how to turn on, initialize, and control this system. The system is controlled using an LQR stabilizing regulator and a Mixed-sensitivity controller. 

The system control was designated at FEE CTU by Pavel Svoboda, Noemi Vaculínová, Šimon Lehký, and Michal Reiser. All files in the repository are arranged into three folders:
- `/m` folder contains the necessary Matlab files for the model initialization
- `/sim` folder contains the Simulink model to be controlled
- `/q_2xamd1_stII_q8_usb_quarc_win64` folder contains all auxiliary files for controlling the hardware model in the laboratory

Follow the instructions below for the system setup. The original repository with more information and details can be found [here](https://gitlab.fel.cvut.cz/aa4cc/edu-ctrl-lab-models/quanser-amd/).

## Controllers
The hardware model can be controlled either by an LQR controller or a Mixed-sensitivity controller. In the Simulink file `/sim/q_2xamd1_stII_q8_usb.slx`, one can choose which controller to use by switching the Control Mode switch in the *Control Block*. 

### LQR controller
The original repository already offered a working LQR controller using the `lqr()` function in Matlab. As its design proved insufficient for the model control, we redesigned the controller's **Q** and **R** matrices. 
> **Tip**: If any change to the LQR controller is desired, one can adjust the matrices at lines **47**-**48** in `/m/setup_stII_2xamd1.m`.

### Mixed-sensitivity controller
The mixed-sensitivity controller was designated using `mixsyn()` function in Matlab with three weighting matrices **W**, **Wp** and **Wu**
- the model uncertainty **W** was modelled as a transfer function with `tf()`
- the nominal performance **Wp** was modelled using `makeweight()` function
- the input uncertainty **Wu** was set to zero 
> **Tip**: If any change to the Mixed-sensitivity controller is desired, one can adjust the matrices at lines **100**-**115** in `/m/setup_stII_2xamd1.m`.

## How to use the system
1) Clone this repository and start Matlab and Simulink.
2) Turn on the **Quanser AMPAQ-PWM** Single Channel PWM Power amplifier and the Quanser VoltPAQ-X2 Linear Voltage amplifier. Ensure that the **Quanser Q8-USB** Data Acquisition board is also powered. Check that all cables are firmly plugged in.
3) Before every run, be sure to push the movable carts (the two AMDs) to the middle positions on their floors and use the blue turning knob on the shake table to set the moving stage to the *home position* – reaching the home position is indicated by a green LED on the AMPAQ-PWM amplifier (the experiment will not start if the table is not in the home position).
4) In Matlab, run `setup_stII_2xamd1.m`. This will initialize all necessary variables for the hardware model.
5) Open `q_2xamd1_stII_q8_usb.slx`, the Simulink model runs in External mode. Click the green triangle subtitled *Monitor & Tune* to start the experiment in the Hardware tab. This may take a few seconds, and then the experiment starts.
> **Tip**: If the Simulink model is started without turning on the hardware components in 2), one will get such an error message:

*Error occurred while executing External Mode MEX-file 'quarc_comm':
An operating system specific kernel-level driver for the specified card could not be found. The card or driver may not be installed. The kernel-level driver is the driver that gets installed when the operating system detects the hardware. Check Device Manager to see if the card is present or recognized by the operating system. Also verify that you have selected the correct card in the HIL Initialize block or hil_open function.*

## Troubleshooting
If you run into any problem, follow the [Troubleshooting guide](https://gitlab.fel.cvut.cz/aa4cc/edu-ctrl-lab-models/quanser-amd/-/blob/master/TROUBLESHOOTING.md).