/*
 * q_2xamd1_stII_q8_usb_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "q_2xamd1_stII_q8_usb".
 *
 * Model version              : 6.24
 * Simulink Coder version : 9.5 (R2021a) 14-Nov-2020
 * C source code generated on : Wed May 15 16:05:53 2024
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_q_2xamd1_stII_q8_usb_types_h_
#define RTW_HEADER_q_2xamd1_stII_q8_usb_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"

/* Model Code Variants */

/* Parameters (default storage) */
typedef struct P_q_2xamd1_stII_q8_usb_T_ P_q_2xamd1_stII_q8_usb_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_q_2xamd1_stII_q8_usb_T RT_MODEL_q_2xamd1_stII_q8_usb_T;

#endif                            /* RTW_HEADER_q_2xamd1_stII_q8_usb_types_h_ */
