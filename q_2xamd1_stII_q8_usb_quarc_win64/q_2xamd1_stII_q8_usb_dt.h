/*
 * q_2xamd1_stII_q8_usb_dt.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "q_2xamd1_stII_q8_usb".
 *
 * Model version              : 6.24
 * Simulink Coder version : 9.5 (R2021a) 14-Nov-2020
 * C source code generated on : Wed May 15 16:05:53 2024
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(void*),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(t_uint64),
  sizeof(t_card),
  sizeof(t_task),
  sizeof(t_boolean)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "t_uint64",
  "t_card",
  "t_task",
  "t_boolean"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[0]), 0, 0, 104 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_B.HILReadTimebase_o3[0]), 8, 0, 17 }
  ,

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[0]), 0, 0, 106 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.ToHostFile_PointsWritten), 14, 0, 1 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.HILInitialize_Card), 15, 0, 1 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.HILReadTimebase_Task), 16, 0, 1 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time), 0, 0, 4 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.HILWrite_PWORK), 11, 0, 16 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.HILInitialize_ClockModes[0]), 6, 0, 54 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedChans[0]), 7, 0,
    10 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK), 10, 0, 4 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_PrevRese), 2, 0,
    11 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag), 3, 0, 2 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.HILInitialize_DOBits[0]), 8, 0, 9 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.HILWrite_DigitalBuffer[0]), 17, 0, 2 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_DW.RampSignal_MODE), 8, 0, 4 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  16U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&q_2xamd1_stII_q8_usb_P.A[0]), 0, 0, 210 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_P.HILWrite_analog_channels[0]), 7, 0, 5 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_P.StopwithMessage_message_icon), 2, 0, 9 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_P.unity_Value), 0, 0, 448 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_P.HILInitialize_CKChannels[0]), 6, 0, 10 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_P.HILInitialize_AIChannels[0]), 7, 0, 452 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_P.Constant_Value_h2), 8, 0, 49 },

  { (char_T *)(&q_2xamd1_stII_q8_usb_P.HILReadTimebase_OverflowMode), 3, 0, 38 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  8U,
  rtPTransitions
};

/* [EOF] q_2xamd1_stII_q8_usb_dt.h */
