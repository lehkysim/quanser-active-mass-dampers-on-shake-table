  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 8;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (q_2xamd1_stII_q8_usb_P)
    ;%
      section.nData     = 48;
      section.data(48)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.A
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.B
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 64;
	
	  ;% q_2xamd1_stII_q8_usb_P.C
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 80;
	
	  ;% q_2xamd1_stII_q8_usb_P.D
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 112;
	
	  ;% q_2xamd1_stII_q8_usb_P.G
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 120;
	
	  ;% q_2xamd1_stII_q8_usb_P.K_AMD_AMP
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 152;
	
	  ;% q_2xamd1_stII_q8_usb_P.K_AMP
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 153;
	
	  ;% q_2xamd1_stII_q8_usb_P.K_EC
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 154;
	
	  ;% q_2xamd1_stII_q8_usb_P.K_ENC
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 155;
	
	  ;% q_2xamd1_stII_q8_usb_P.K_G2MS
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 156;
	
	  ;% q_2xamd1_stII_q8_usb_P.K_MS2G
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 157;
	
	  ;% q_2xamd1_stII_q8_usb_P.Kamd
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 158;
	
	  ;% q_2xamd1_stII_q8_usb_P.Kp
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 174;
	
	  ;% q_2xamd1_stII_q8_usb_P.Kv
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 175;
	
	  ;% q_2xamd1_stII_q8_usb_P.P_MAX
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 176;
	
	  ;% q_2xamd1_stII_q8_usb_P.SWEEP_MAX
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 177;
	
	  ;% q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 178;
	
	  ;% q_2xamd1_stII_q8_usb_P.XC_LIM_ENABLE
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 179;
	
	  ;% q_2xamd1_stII_q8_usb_P.XC_MAX
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 180;
	
	  ;% q_2xamd1_stII_q8_usb_P.XC_MIN
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 181;
	
	  ;% q_2xamd1_stII_q8_usb_P.dbnc_rise_time
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 182;
	
	  ;% q_2xamd1_stII_q8_usb_P.dbnc_threshold
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 183;
	
	  ;% q_2xamd1_stII_q8_usb_P.kd
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 184;
	
	  ;% q_2xamd1_stII_q8_usb_P.kp
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 185;
	
	  ;% q_2xamd1_stII_q8_usb_P.wa
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 186;
	
	  ;% q_2xamd1_stII_q8_usb_P.wd
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 187;
	
	  ;% q_2xamd1_stII_q8_usb_P.za
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 188;
	
	  ;% q_2xamd1_stII_q8_usb_P.zd
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 189;
	
	  ;% q_2xamd1_stII_q8_usb_P.RepeatingChirp_A
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 190;
	
	  ;% q_2xamd1_stII_q8_usb_P.RepeatingChirp_D
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 191;
	
	  ;% q_2xamd1_stII_q8_usb_P.RepeatingChirp_T
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 192;
	
	  ;% q_2xamd1_stII_q8_usb_P.After2sec_const
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 193;
	
	  ;% q_2xamd1_stII_q8_usb_P.CompareToConstant_const
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 194;
	
	  ;% q_2xamd1_stII_q8_usb_P.Startat1sec_const
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 195;
	
	  ;% q_2xamd1_stII_q8_usb_P.StallMonitor_control_threshold
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 196;
	
	  ;% q_2xamd1_stII_q8_usb_P.StallMonitor_duration_s
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 197;
	
	  ;% q_2xamd1_stII_q8_usb_P.BiasRemoval_end_time
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 198;
	
	  ;% q_2xamd1_stII_q8_usb_P.RepeatingChirp_f1
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 199;
	
	  ;% q_2xamd1_stII_q8_usb_P.RepeatingChirp_f2
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 200;
	
	  ;% q_2xamd1_stII_q8_usb_P.debouncerleft_fall_time
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 201;
	
	  ;% q_2xamd1_stII_q8_usb_P.debouncerright_fall_time
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 202;
	
	  ;% q_2xamd1_stII_q8_usb_P.VelocitySetpointWeight_gain
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 203;
	
	  ;% q_2xamd1_stII_q8_usb_P.VelocitySetpointWeight_gain_l
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 204;
	
	  ;% q_2xamd1_stII_q8_usb_P.Amplitudecm_gain
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 205;
	
	  ;% q_2xamd1_stII_q8_usb_P.FrequencyHz_gain
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 206;
	
	  ;% q_2xamd1_stII_q8_usb_P.StallMonitor_motion_threshold
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 207;
	
	  ;% q_2xamd1_stII_q8_usb_P.BiasRemoval_start_time
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 208;
	
	  ;% q_2xamd1_stII_q8_usb_P.BiasRemoval_switch_id
	  section.data(48).logicalSrcIdx = 47;
	  section.data(48).dtTransOffset = 209;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.HILWrite_analog_channels
	  section.data(1).logicalSrcIdx = 48;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILWrite_digital_channels
	  section.data(2).logicalSrcIdx = 49;
	  section.data(2).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 9;
      section.data(9)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessage_message_icon
	  section.data(1).logicalSrcIdx = 50;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessage1_message_icon
	  section.data(2).logicalSrcIdx = 51;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessage_message_icon_i
	  section.data(3).logicalSrcIdx = 52;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessage1_message_icon_e
	  section.data(4).logicalSrcIdx = 53;
	  section.data(4).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessageAmpFault_message
	  section.data(5).logicalSrcIdx = 54;
	  section.data(5).dtTransOffset = 4;
	
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessage_message_icon_e
	  section.data(6).logicalSrcIdx = 55;
	  section.data(6).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessageLeftLimit_messag
	  section.data(7).logicalSrcIdx = 56;
	  section.data(7).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_P.StopwithMessageRightLimit_messa
	  section.data(8).logicalSrcIdx = 57;
	  section.data(8).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_P.Encodernotreading_message_icon
	  section.data(9).logicalSrcIdx = 58;
	  section.data(9).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 97;
      section.data(97)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.unity_Value
	  section.data(1).logicalSrcIdx = 59;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.UnitDelay_InitialCondition
	  section.data(2).logicalSrcIdx = 60;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_P.Sumk1n1xk_InitialCondition
	  section.data(3).logicalSrcIdx = 61;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_P.zero_Y0
	  section.data(4).logicalSrcIdx = 62;
	  section.data(4).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value
	  section.data(5).logicalSrcIdx = 63;
	  section.data(5).dtTransOffset = 4;
	
	  ;% q_2xamd1_stII_q8_usb_P.Vbiased_Y0
	  section.data(6).logicalSrcIdx = 64;
	  section.data(6).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_P.Vunbiased_Y0
	  section.data(7).logicalSrcIdx = 65;
	  section.data(7).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_P.Stepstart_time_Y0
	  section.data(8).logicalSrcIdx = 66;
	  section.data(8).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_P.Stepstart_time_YFinal
	  section.data(9).logicalSrcIdx = 67;
	  section.data(9).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_P.Stepend_time_Y0
	  section.data(10).logicalSrcIdx = 68;
	  section.data(10).dtTransOffset = 9;
	
	  ;% q_2xamd1_stII_q8_usb_P.Stepend_time_YFinal
	  section.data(11).logicalSrcIdx = 69;
	  section.data(11).dtTransOffset = 10;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_g
	  section.data(12).logicalSrcIdx = 70;
	  section.data(12).dtTransOffset = 11;
	
	  ;% q_2xamd1_stII_q8_usb_P.Integrator_IC
	  section.data(13).logicalSrcIdx = 71;
	  section.data(13).dtTransOffset = 12;
	
	  ;% q_2xamd1_stII_q8_usb_P.Gain_Gain
	  section.data(14).logicalSrcIdx = 72;
	  section.data(14).dtTransOffset = 13;
	
	  ;% q_2xamd1_stII_q8_usb_P.On_Value
	  section.data(15).logicalSrcIdx = 73;
	  section.data(15).dtTransOffset = 14;
	
	  ;% q_2xamd1_stII_q8_usb_P.Off_Value
	  section.data(16).logicalSrcIdx = 74;
	  section.data(16).dtTransOffset = 15;
	
	  ;% q_2xamd1_stII_q8_usb_P.PositionSetpointm_Value
	  section.data(17).logicalSrcIdx = 75;
	  section.data(17).dtTransOffset = 16;
	
	  ;% q_2xamd1_stII_q8_usb_P.VelocitySetpointms_Value
	  section.data(18).logicalSrcIdx = 76;
	  section.data(18).dtTransOffset = 17;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_OOTerminate
	  section.data(19).logicalSrcIdx = 77;
	  section.data(19).dtTransOffset = 18;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_OOExit
	  section.data(20).logicalSrcIdx = 78;
	  section.data(20).dtTransOffset = 19;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_OOStart
	  section.data(21).logicalSrcIdx = 79;
	  section.data(21).dtTransOffset = 20;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_OOEnter
	  section.data(22).logicalSrcIdx = 80;
	  section.data(22).dtTransOffset = 21;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOFinal
	  section.data(23).logicalSrcIdx = 81;
	  section.data(23).dtTransOffset = 22;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POFinal
	  section.data(24).logicalSrcIdx = 82;
	  section.data(24).dtTransOffset = 23;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AIHigh
	  section.data(25).logicalSrcIdx = 83;
	  section.data(25).dtTransOffset = 24;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AILow
	  section.data(26).logicalSrcIdx = 84;
	  section.data(26).dtTransOffset = 25;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOHigh
	  section.data(27).logicalSrcIdx = 85;
	  section.data(27).dtTransOffset = 26;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOLow
	  section.data(28).logicalSrcIdx = 86;
	  section.data(28).dtTransOffset = 27;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOInitial
	  section.data(29).logicalSrcIdx = 87;
	  section.data(29).dtTransOffset = 28;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOWatchdog
	  section.data(30).logicalSrcIdx = 88;
	  section.data(30).dtTransOffset = 29;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POFrequency
	  section.data(31).logicalSrcIdx = 89;
	  section.data(31).dtTransOffset = 30;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POLeading
	  section.data(32).logicalSrcIdx = 90;
	  section.data(32).dtTransOffset = 31;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POTrailing
	  section.data(33).logicalSrcIdx = 91;
	  section.data(33).dtTransOffset = 32;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POInitial
	  section.data(34).logicalSrcIdx = 92;
	  section.data(34).dtTransOffset = 33;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POWatchdog
	  section.data(35).logicalSrcIdx = 93;
	  section.data(35).dtTransOffset = 34;
	
	  ;% q_2xamd1_stII_q8_usb_P.To_Samples_Gain
	  section.data(36).logicalSrcIdx = 94;
	  section.data(36).dtTransOffset = 35;
	
	  ;% q_2xamd1_stII_q8_usb_P.To_Samples1_Gain
	  section.data(37).logicalSrcIdx = 95;
	  section.data(37).dtTransOffset = 36;
	
	  ;% q_2xamd1_stII_q8_usb_P.debouncer_block_init_state
	  section.data(38).logicalSrcIdx = 96;
	  section.data(38).dtTransOffset = 37;
	
	  ;% q_2xamd1_stII_q8_usb_P.To_Samples_Gain_c
	  section.data(39).logicalSrcIdx = 97;
	  section.data(39).dtTransOffset = 38;
	
	  ;% q_2xamd1_stII_q8_usb_P.To_Samples1_Gain_n
	  section.data(40).logicalSrcIdx = 98;
	  section.data(40).dtTransOffset = 39;
	
	  ;% q_2xamd1_stII_q8_usb_P.debouncer_block_init_state_c
	  section.data(41).logicalSrcIdx = 99;
	  section.data(41).dtTransOffset = 40;
	
	  ;% q_2xamd1_stII_q8_usb_P.ShakerIISetpointSelection_Value
	  section.data(42).logicalSrcIdx = 100;
	  section.data(42).dtTransOffset = 41;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI
	  section.data(43).logicalSrcIdx = 101;
	  section.data(43).dtTransOffset = 42;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VI
	  section.data(44).logicalSrcIdx = 102;
	  section.data(44).dtTransOffset = 43;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_Hold
	  section.data(45).logicalSrcIdx = 103;
	  section.data(45).dtTransOffset = 44;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax
	  section.data(46).logicalSrcIdx = 104;
	  section.data(46).dtTransOffset = 45;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax
	  section.data(47).logicalSrcIdx = 105;
	  section.data(47).dtTransOffset = 46;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI_c
	  section.data(48).logicalSrcIdx = 106;
	  section.data(48).dtTransOffset = 47;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VI_h
	  section.data(49).logicalSrcIdx = 107;
	  section.data(49).dtTransOffset = 48;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_Hold_e
	  section.data(50).logicalSrcIdx = 108;
	  section.data(50).dtTransOffset = 49;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax_j
	  section.data(51).logicalSrcIdx = 109;
	  section.data(51).dtTransOffset = 50;
	
	  ;% q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax_a
	  section.data(52).logicalSrcIdx = 110;
	  section.data(52).dtTransOffset = 51;
	
	  ;% q_2xamd1_stII_q8_usb_P.x0_Value
	  section.data(53).logicalSrcIdx = 111;
	  section.data(53).dtTransOffset = 52;
	
	  ;% q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim
	  section.data(54).logicalSrcIdx = 112;
	  section.data(54).dtTransOffset = 53;
	
	  ;% q_2xamd1_stII_q8_usb_P.VoltageLimit_PTime
	  section.data(55).logicalSrcIdx = 113;
	  section.data(55).dtTransOffset = 54;
	
	  ;% q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim
	  section.data(56).logicalSrcIdx = 114;
	  section.data(56).dtTransOffset = 55;
	
	  ;% q_2xamd1_stII_q8_usb_P.VoltageLimit_CTime
	  section.data(57).logicalSrcIdx = 115;
	  section.data(57).dtTransOffset = 56;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_g3
	  section.data(58).logicalSrcIdx = 116;
	  section.data(58).dtTransOffset = 57;
	
	  ;% q_2xamd1_stII_q8_usb_P.Integrator_IC_e
	  section.data(59).logicalSrcIdx = 117;
	  section.data(59).dtTransOffset = 58;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_e
	  section.data(60).logicalSrcIdx = 118;
	  section.data(60).dtTransOffset = 59;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILWatchdog_Timeout
	  section.data(61).logicalSrcIdx = 119;
	  section.data(61).dtTransOffset = 60;
	
	  ;% q_2xamd1_stII_q8_usb_P.x0_Value_f
	  section.data(62).logicalSrcIdx = 120;
	  section.data(62).dtTransOffset = 61;
	
	  ;% q_2xamd1_stII_q8_usb_P.Foraccelerationtobesamedirectio
	  section.data(63).logicalSrcIdx = 121;
	  section.data(63).dtTransOffset = 62;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_A_pr
	  section.data(64).logicalSrcIdx = 122;
	  section.data(64).dtTransOffset = 63;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_B_pr
	  section.data(65).logicalSrcIdx = 123;
	  section.data(65).dtTransOffset = 319;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_C_pr
	  section.data(66).logicalSrcIdx = 124;
	  section.data(66).dtTransOffset = 383;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_InitialCondition
	  section.data(67).logicalSrcIdx = 125;
	  section.data(67).dtTransOffset = 415;
	
	  ;% q_2xamd1_stII_q8_usb_P.mtocm_Gain
	  section.data(68).logicalSrcIdx = 126;
	  section.data(68).dtTransOffset = 416;
	
	  ;% q_2xamd1_stII_q8_usb_P.x0_Value_h
	  section.data(69).logicalSrcIdx = 127;
	  section.data(69).dtTransOffset = 417;
	
	  ;% q_2xamd1_stII_q8_usb_P.mtomm_Gain
	  section.data(70).logicalSrcIdx = 128;
	  section.data(70).dtTransOffset = 418;
	
	  ;% q_2xamd1_stII_q8_usb_P.mtomm1_Gain
	  section.data(71).logicalSrcIdx = 129;
	  section.data(71).dtTransOffset = 419;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_b
	  section.data(72).logicalSrcIdx = 130;
	  section.data(72).dtTransOffset = 420;
	
	  ;% q_2xamd1_stII_q8_usb_P.Integrator2_IC
	  section.data(73).logicalSrcIdx = 131;
	  section.data(73).dtTransOffset = 421;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_a
	  section.data(74).logicalSrcIdx = 132;
	  section.data(74).dtTransOffset = 422;
	
	  ;% q_2xamd1_stII_q8_usb_P.Kp_c2741_Gain
	  section.data(75).logicalSrcIdx = 133;
	  section.data(75).dtTransOffset = 423;
	
	  ;% q_2xamd1_stII_q8_usb_P.Kp_c2741_Gain_m
	  section.data(76).logicalSrcIdx = 134;
	  section.data(76).dtTransOffset = 424;
	
	  ;% q_2xamd1_stII_q8_usb_P.DiscreteTimeIntegrator_gainval
	  section.data(77).logicalSrcIdx = 135;
	  section.data(77).dtTransOffset = 425;
	
	  ;% q_2xamd1_stII_q8_usb_P.DiscreteTimeIntegrator_IC
	  section.data(78).logicalSrcIdx = 136;
	  section.data(78).dtTransOffset = 426;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_d
	  section.data(79).logicalSrcIdx = 137;
	  section.data(79).dtTransOffset = 427;
	
	  ;% q_2xamd1_stII_q8_usb_P.Integrator2_IC_i
	  section.data(80).logicalSrcIdx = 138;
	  section.data(80).dtTransOffset = 428;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_h
	  section.data(81).logicalSrcIdx = 139;
	  section.data(81).dtTransOffset = 429;
	
	  ;% q_2xamd1_stII_q8_usb_P.Integrator2_IC_c
	  section.data(82).logicalSrcIdx = 140;
	  section.data(82).dtTransOffset = 430;
	
	  ;% q_2xamd1_stII_q8_usb_P.UPMInitTimes_Value
	  section.data(83).logicalSrcIdx = 141;
	  section.data(83).dtTransOffset = 431;
	
	  ;% q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_NumCoef
	  section.data(84).logicalSrcIdx = 142;
	  section.data(84).dtTransOffset = 432;
	
	  ;% q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_DenCoef
	  section.data(85).logicalSrcIdx = 143;
	  section.data(85).dtTransOffset = 434;
	
	  ;% q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_InitialStat
	  section.data(86).logicalSrcIdx = 144;
	  section.data(86).dtTransOffset = 436;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant3_Value
	  section.data(87).logicalSrcIdx = 145;
	  section.data(87).dtTransOffset = 437;
	
	  ;% q_2xamd1_stII_q8_usb_P.DeadZone_Start
	  section.data(88).logicalSrcIdx = 146;
	  section.data(88).dtTransOffset = 438;
	
	  ;% q_2xamd1_stII_q8_usb_P.SineWaveFunction_Amp
	  section.data(89).logicalSrcIdx = 147;
	  section.data(89).dtTransOffset = 439;
	
	  ;% q_2xamd1_stII_q8_usb_P.SineWaveFunction_Bias
	  section.data(90).logicalSrcIdx = 148;
	  section.data(90).dtTransOffset = 440;
	
	  ;% q_2xamd1_stII_q8_usb_P.SineWaveFunction_Freq
	  section.data(91).logicalSrcIdx = 149;
	  section.data(91).dtTransOffset = 441;
	
	  ;% q_2xamd1_stII_q8_usb_P.SineWaveFunction_Phase
	  section.data(92).logicalSrcIdx = 150;
	  section.data(92).dtTransOffset = 442;
	
	  ;% q_2xamd1_stII_q8_usb_P.SineSweepAmplitudem_Gain
	  section.data(93).logicalSrcIdx = 151;
	  section.data(93).dtTransOffset = 443;
	
	  ;% q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_InitialPh
	  section.data(94).logicalSrcIdx = 152;
	  section.data(94).dtTransOffset = 444;
	
	  ;% q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_Amplitude
	  section.data(95).logicalSrcIdx = 153;
	  section.data(95).dtTransOffset = 445;
	
	  ;% q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_Frequency
	  section.data(96).logicalSrcIdx = 154;
	  section.data(96).dtTransOffset = 446;
	
	  ;% q_2xamd1_stII_q8_usb_P.cmtom_Gain
	  section.data(97).logicalSrcIdx = 155;
	  section.data(97).dtTransOffset = 447;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_CKChannels
	  section.data(1).logicalSrcIdx = 156;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOWatchdog
	  section.data(2).logicalSrcIdx = 157;
	  section.data(2).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_EIInitial
	  section.data(3).logicalSrcIdx = 158;
	  section.data(3).dtTransOffset = 4;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POModes
	  section.data(4).logicalSrcIdx = 159;
	  section.data(4).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POConfiguration
	  section.data(5).logicalSrcIdx = 160;
	  section.data(5).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POAlignment
	  section.data(6).logicalSrcIdx = 161;
	  section.data(6).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POPolarity
	  section.data(7).logicalSrcIdx = 162;
	  section.data(7).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_Clock
	  section.data(8).logicalSrcIdx = 163;
	  section.data(8).dtTransOffset = 9;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(5) = section;
      clear section
      
      section.nData     = 20;
      section.data(20)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AIChannels
	  section.data(1).logicalSrcIdx = 164;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOChannels
	  section.data(2).logicalSrcIdx = 165;
	  section.data(2).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DIChannels
	  section.data(3).logicalSrcIdx = 166;
	  section.data(3).dtTransOffset = 16;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOChannels
	  section.data(4).logicalSrcIdx = 167;
	  section.data(4).dtTransOffset = 21;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_EIChannels
	  section.data(5).logicalSrcIdx = 168;
	  section.data(5).dtTransOffset = 29;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_EIQuadrature
	  section.data(6).logicalSrcIdx = 169;
	  section.data(6).dtTransOffset = 37;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels
	  section.data(7).logicalSrcIdx = 170;
	  section.data(7).dtTransOffset = 38;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_SamplesInBuffer
	  section.data(8).logicalSrcIdx = 171;
	  section.data(8).dtTransOffset = 46;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_AnalogChannels
	  section.data(9).logicalSrcIdx = 172;
	  section.data(9).dtTransOffset = 47;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_EncoderChannels
	  section.data(10).logicalSrcIdx = 173;
	  section.data(10).dtTransOffset = 50;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_DigitalChannels
	  section.data(11).logicalSrcIdx = 174;
	  section.data(11).dtTransOffset = 53;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_OtherChannels
	  section.data(12).logicalSrcIdx = 175;
	  section.data(12).dtTransOffset = 58;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_A_ir
	  section.data(13).logicalSrcIdx = 176;
	  section.data(13).dtTransOffset = 59;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_A_jc
	  section.data(14).logicalSrcIdx = 177;
	  section.data(14).dtTransOffset = 315;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_B_ir
	  section.data(15).logicalSrcIdx = 178;
	  section.data(15).dtTransOffset = 332;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_B_jc
	  section.data(16).logicalSrcIdx = 179;
	  section.data(16).dtTransOffset = 396;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_C_ir
	  section.data(17).logicalSrcIdx = 180;
	  section.data(17).dtTransOffset = 401;
	
	  ;% q_2xamd1_stII_q8_usb_P.StateSpace_C_jc
	  section.data(18).logicalSrcIdx = 181;
	  section.data(18).dtTransOffset = 433;
	
	  ;% q_2xamd1_stII_q8_usb_P.ToHostFile_Decimation
	  section.data(19).logicalSrcIdx = 182;
	  section.data(19).dtTransOffset = 450;
	
	  ;% q_2xamd1_stII_q8_usb_P.ToHostFile_BitRate
	  section.data(20).logicalSrcIdx = 183;
	  section.data(20).dtTransOffset = 451;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(6) = section;
      clear section
      
      section.nData     = 49;
      section.data(49)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_h2
	  section.data(1).logicalSrcIdx = 184;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_gt
	  section.data(2).logicalSrcIdx = 185;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_p
	  section.data(3).logicalSrcIdx = 186;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_k
	  section.data(4).logicalSrcIdx = 187;
	  section.data(4).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_l
	  section.data(5).logicalSrcIdx = 188;
	  section.data(5).dtTransOffset = 4;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_c
	  section.data(6).logicalSrcIdx = 189;
	  section.data(6).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_kw
	  section.data(7).logicalSrcIdx = 190;
	  section.data(7).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_c1
	  section.data(8).logicalSrcIdx = 191;
	  section.data(8).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_P.Constant_Value_di
	  section.data(9).logicalSrcIdx = 192;
	  section.data(9).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_Active
	  section.data(10).logicalSrcIdx = 193;
	  section.data(10).dtTransOffset = 9;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOTerminate
	  section.data(11).logicalSrcIdx = 194;
	  section.data(11).dtTransOffset = 10;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOExit
	  section.data(12).logicalSrcIdx = 195;
	  section.data(12).dtTransOffset = 11;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOTerminate
	  section.data(13).logicalSrcIdx = 196;
	  section.data(13).dtTransOffset = 12;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOExit
	  section.data(14).logicalSrcIdx = 197;
	  section.data(14).dtTransOffset = 13;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POTerminate
	  section.data(15).logicalSrcIdx = 198;
	  section.data(15).dtTransOffset = 14;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POExit
	  section.data(16).logicalSrcIdx = 199;
	  section.data(16).dtTransOffset = 15;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_CKPStart
	  section.data(17).logicalSrcIdx = 200;
	  section.data(17).dtTransOffset = 16;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_CKPEnter
	  section.data(18).logicalSrcIdx = 201;
	  section.data(18).dtTransOffset = 17;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_CKStart
	  section.data(19).logicalSrcIdx = 202;
	  section.data(19).dtTransOffset = 18;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_CKEnter
	  section.data(20).logicalSrcIdx = 203;
	  section.data(20).dtTransOffset = 19;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AIPStart
	  section.data(21).logicalSrcIdx = 204;
	  section.data(21).dtTransOffset = 20;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AIPEnter
	  section.data(22).logicalSrcIdx = 205;
	  section.data(22).dtTransOffset = 21;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOPStart
	  section.data(23).logicalSrcIdx = 206;
	  section.data(23).dtTransOffset = 22;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOPEnter
	  section.data(24).logicalSrcIdx = 207;
	  section.data(24).dtTransOffset = 23;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOStart
	  section.data(25).logicalSrcIdx = 208;
	  section.data(25).dtTransOffset = 24;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOEnter
	  section.data(26).logicalSrcIdx = 209;
	  section.data(26).dtTransOffset = 25;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_AOReset
	  section.data(27).logicalSrcIdx = 210;
	  section.data(27).dtTransOffset = 26;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOPStart
	  section.data(28).logicalSrcIdx = 211;
	  section.data(28).dtTransOffset = 27;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOPEnter
	  section.data(29).logicalSrcIdx = 212;
	  section.data(29).dtTransOffset = 28;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOStart
	  section.data(30).logicalSrcIdx = 213;
	  section.data(30).dtTransOffset = 29;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOEnter
	  section.data(31).logicalSrcIdx = 214;
	  section.data(31).dtTransOffset = 30;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOReset
	  section.data(32).logicalSrcIdx = 215;
	  section.data(32).dtTransOffset = 31;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_EIPStart
	  section.data(33).logicalSrcIdx = 216;
	  section.data(33).dtTransOffset = 32;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_EIPEnter
	  section.data(34).logicalSrcIdx = 217;
	  section.data(34).dtTransOffset = 33;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_EIStart
	  section.data(35).logicalSrcIdx = 218;
	  section.data(35).dtTransOffset = 34;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_EIEnter
	  section.data(36).logicalSrcIdx = 219;
	  section.data(36).dtTransOffset = 35;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POPStart
	  section.data(37).logicalSrcIdx = 220;
	  section.data(37).dtTransOffset = 36;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POPEnter
	  section.data(38).logicalSrcIdx = 221;
	  section.data(38).dtTransOffset = 37;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POStart
	  section.data(39).logicalSrcIdx = 222;
	  section.data(39).dtTransOffset = 38;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POEnter
	  section.data(40).logicalSrcIdx = 223;
	  section.data(40).dtTransOffset = 39;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_POReset
	  section.data(41).logicalSrcIdx = 224;
	  section.data(41).dtTransOffset = 40;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_OOReset
	  section.data(42).logicalSrcIdx = 225;
	  section.data(42).dtTransOffset = 41;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOFinal
	  section.data(43).logicalSrcIdx = 226;
	  section.data(43).dtTransOffset = 42;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILInitialize_DOInitial
	  section.data(44).logicalSrcIdx = 227;
	  section.data(44).dtTransOffset = 43;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_Active
	  section.data(45).logicalSrcIdx = 228;
	  section.data(45).dtTransOffset = 44;
	
	  ;% q_2xamd1_stII_q8_usb_P.CalibrateSignalOFF_Value
	  section.data(46).logicalSrcIdx = 229;
	  section.data(46).dtTransOffset = 45;
	
	  ;% q_2xamd1_stII_q8_usb_P.EnableSignalON_Value
	  section.data(47).logicalSrcIdx = 230;
	  section.data(47).dtTransOffset = 46;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILWrite_Active
	  section.data(48).logicalSrcIdx = 231;
	  section.data(48).dtTransOffset = 47;
	
	  ;% q_2xamd1_stII_q8_usb_P.HILWatchdog_Active
	  section.data(49).logicalSrcIdx = 232;
	  section.data(49).dtTransOffset = 48;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(7) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_P.HILReadTimebase_OverflowMode
	  section.data(1).logicalSrcIdx = 233;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_P.AMDControlMode_CurrentSetting
	  section.data(2).logicalSrcIdx = 234;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_P.ControlMode_CurrentSetting
	  section.data(3).logicalSrcIdx = 235;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_P.ToHostFile_file_name
	  section.data(4).logicalSrcIdx = 236;
	  section.data(4).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_P.ToHostFile_VarName
	  section.data(5).logicalSrcIdx = 237;
	  section.data(5).dtTransOffset = 32;
	
	  ;% q_2xamd1_stII_q8_usb_P.ToHostFile_FileFormat
	  section.data(6).logicalSrcIdx = 238;
	  section.data(6).dtTransOffset = 37;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(8) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 2;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (q_2xamd1_stII_q8_usb_B)
    ;%
      section.nData     = 64;
      section.data(64)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion5
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion2
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 4;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion3
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion5_h
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion2_b
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion3_a
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 9;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 10;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 11;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 12;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 13;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 14;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3_b
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 15;
	
	  ;% q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4_k
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 16;
	
	  ;% q_2xamd1_stII_q8_usb_B.SetpointSwitch
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 17;
	
	  ;% q_2xamd1_stII_q8_usb_B.EncoderCalibrationmcount
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 19;
	
	  ;% q_2xamd1_stII_q8_usb_B.ProportionalGainVm
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 20;
	
	  ;% q_2xamd1_stII_q8_usb_B.x0
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 21;
	
	  ;% q_2xamd1_stII_q8_usb_B.Add
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 22;
	
	  ;% q_2xamd1_stII_q8_usb_B.VoltageLimit
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 23;
	
	  ;% q_2xamd1_stII_q8_usb_B.AMDControlMode
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 24;
	
	  ;% q_2xamd1_stII_q8_usb_B.Integrator
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 25;
	
	  ;% q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 33;
	
	  ;% q_2xamd1_stII_q8_usb_B.AmplifierGainPreCompensation
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 35;
	
	  ;% q_2xamd1_stII_q8_usb_B.Conversiontom1
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 37;
	
	  ;% q_2xamd1_stII_q8_usb_B.Conversiontom2
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 38;
	
	  ;% q_2xamd1_stII_q8_usb_B.x0_f
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 39;
	
	  ;% q_2xamd1_stII_q8_usb_B.floor1gtoms2
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 40;
	
	  ;% q_2xamd1_stII_q8_usb_B.floor1gtoms1
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 41;
	
	  ;% q_2xamd1_stII_q8_usb_B.ControlMode
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 42;
	
	  ;% q_2xamd1_stII_q8_usb_B.mtocm
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 44;
	
	  ;% q_2xamd1_stII_q8_usb_B.x0_o
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 46;
	
	  ;% q_2xamd1_stII_q8_usb_B.ms2tog
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 47;
	
	  ;% q_2xamd1_stII_q8_usb_B.mtomm
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 48;
	
	  ;% q_2xamd1_stII_q8_usb_B.mtomm1
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 49;
	
	  ;% q_2xamd1_stII_q8_usb_B.Product
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 50;
	
	  ;% q_2xamd1_stII_q8_usb_B.Product1
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 53;
	
	  ;% q_2xamd1_stII_q8_usb_B.Kp_c2741
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 56;
	
	  ;% q_2xamd1_stII_q8_usb_B.Kp_c2741_a
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 58;
	
	  ;% q_2xamd1_stII_q8_usb_B.Abs1
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 60;
	
	  ;% q_2xamd1_stII_q8_usb_B.DiscreteTimeIntegrator
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 61;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 62;
	
	  ;% q_2xamd1_stII_q8_usb_B.countsstoms
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 63;
	
	  ;% q_2xamd1_stII_q8_usb_B.Product_m
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 64;
	
	  ;% q_2xamd1_stII_q8_usb_B.Product1_g
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 65;
	
	  ;% q_2xamd1_stII_q8_usb_B.Product_k
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 66;
	
	  ;% q_2xamd1_stII_q8_usb_B.Product1_f
	  section.data(48).logicalSrcIdx = 47;
	  section.data(48).dtTransOffset = 67;
	
	  ;% q_2xamd1_stII_q8_usb_B.DataTypeConversion_b
	  section.data(49).logicalSrcIdx = 48;
	  section.data(49).dtTransOffset = 68;
	
	  ;% q_2xamd1_stII_q8_usb_B.DiscreteTransferFcn
	  section.data(50).logicalSrcIdx = 49;
	  section.data(50).dtTransOffset = 69;
	
	  ;% q_2xamd1_stII_q8_usb_B.SweepPositionLimitm
	  section.data(51).logicalSrcIdx = 51;
	  section.data(51).dtTransOffset = 70;
	
	  ;% q_2xamd1_stII_q8_usb_B.cmtom
	  section.data(52).logicalSrcIdx = 52;
	  section.data(52).dtTransOffset = 71;
	
	  ;% q_2xamd1_stII_q8_usb_B.PositionLimit
	  section.data(53).logicalSrcIdx = 53;
	  section.data(53).dtTransOffset = 72;
	
	  ;% q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace
	  section.data(54).logicalSrcIdx = 54;
	  section.data(54).dtTransOffset = 73;
	
	  ;% q_2xamd1_stII_q8_usb_B.Sum3
	  section.data(55).logicalSrcIdx = 55;
	  section.data(55).dtTransOffset = 77;
	
	  ;% q_2xamd1_stII_q8_usb_B.Gain
	  section.data(56).logicalSrcIdx = 56;
	  section.data(56).dtTransOffset = 85;
	
	  ;% q_2xamd1_stII_q8_usb_B.Product_e
	  section.data(57).logicalSrcIdx = 57;
	  section.data(57).dtTransOffset = 86;
	
	  ;% q_2xamd1_stII_q8_usb_B.Switch
	  section.data(58).logicalSrcIdx = 58;
	  section.data(58).dtTransOffset = 87;
	
	  ;% q_2xamd1_stII_q8_usb_B.VVavg
	  section.data(59).logicalSrcIdx = 59;
	  section.data(59).dtTransOffset = 90;
	
	  ;% q_2xamd1_stII_q8_usb_B.Vin
	  section.data(60).logicalSrcIdx = 60;
	  section.data(60).dtTransOffset = 93;
	
	  ;% q_2xamd1_stII_q8_usb_B.Constant
	  section.data(61).logicalSrcIdx = 61;
	  section.data(61).dtTransOffset = 96;
	
	  ;% q_2xamd1_stII_q8_usb_B.Count
	  section.data(62).logicalSrcIdx = 62;
	  section.data(62).dtTransOffset = 97;
	
	  ;% q_2xamd1_stII_q8_usb_B.Sum
	  section.data(63).logicalSrcIdx = 63;
	  section.data(63).dtTransOffset = 98;
	
	  ;% q_2xamd1_stII_q8_usb_B.div
	  section.data(64).logicalSrcIdx = 64;
	  section.data(64).dtTransOffset = 101;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_B.HILReadTimebase_o3
	  section.data(1).logicalSrcIdx = 65;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_B.debouncer_block
	  section.data(2).logicalSrcIdx = 66;
	  section.data(2).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_B.debouncer_block_p
	  section.data(3).logicalSrcIdx = 67;
	  section.data(3).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_B.RelationalOperator
	  section.data(4).logicalSrcIdx = 68;
	  section.data(4).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_B.LogicalOperator1
	  section.data(5).logicalSrcIdx = 69;
	  section.data(5).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_B.RelationalOperator2
	  section.data(6).logicalSrcIdx = 70;
	  section.data(6).dtTransOffset = 9;
	
	  ;% q_2xamd1_stII_q8_usb_B.Compare
	  section.data(7).logicalSrcIdx = 71;
	  section.data(7).dtTransOffset = 10;
	
	  ;% q_2xamd1_stII_q8_usb_B.HiddenBuf_InsertedFor_RampSigna
	  section.data(8).logicalSrcIdx = 72;
	  section.data(8).dtTransOffset = 11;
	
	  ;% q_2xamd1_stII_q8_usb_B.LogicalOperator
	  section.data(9).logicalSrcIdx = 73;
	  section.data(9).dtTransOffset = 12;
	
	  ;% q_2xamd1_stII_q8_usb_B.CheckMaximumAllowedPosition
	  section.data(10).logicalSrcIdx = 74;
	  section.data(10).dtTransOffset = 13;
	
	  ;% q_2xamd1_stII_q8_usb_B.CheckMinimumAllowedPosition
	  section.data(11).logicalSrcIdx = 75;
	  section.data(11).dtTransOffset = 14;
	
	  ;% q_2xamd1_stII_q8_usb_B.CheckMaximumAllowedPosition_g
	  section.data(12).logicalSrcIdx = 76;
	  section.data(12).dtTransOffset = 15;
	
	  ;% q_2xamd1_stII_q8_usb_B.CheckMinimumAllowedPosition_g
	  section.data(13).logicalSrcIdx = 77;
	  section.data(13).dtTransOffset = 16;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(2) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 14;
    sectIdxOffset = 2;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (q_2xamd1_stII_q8_usb_DW)
    ;%
      section.nData     = 22;
      section.data(22)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_DSTATE
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_states
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_DW.UnitDelay_DSTATE
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 9;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMinimums
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 12;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMaximums
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 20;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMinimums
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 28;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMaximums
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 36;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 44;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_FilterFrequency
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 52;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 60;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 68;
	
	  ;% q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 76;
	
	  ;% q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 87;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 98;
	
	  ;% q_2xamd1_stII_q8_usb_DW.TimeStampA
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 101;
	
	  ;% q_2xamd1_stII_q8_usb_DW.LastUAtTimeA
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 102;
	
	  ;% q_2xamd1_stII_q8_usb_DW.TimeStampB
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 103;
	
	  ;% q_2xamd1_stII_q8_usb_DW.LastUAtTimeB
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 104;
	
	  ;% q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_tmp
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 105;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.ToHostFile_PointsWritten
	  section.data(1).logicalSrcIdx = 22;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_Card
	  section.data(1).logicalSrcIdx = 23;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.HILReadTimebase_Task
	  section.data(1).logicalSrcIdx = 24;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time
	  section.data(1).logicalSrcIdx = 25;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time
	  section.data(2).logicalSrcIdx = 26;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_DW.VoltageLimit_RWORK.Mean
	  section.data(3).logicalSrcIdx = 27;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp
	  section.data(4).logicalSrcIdx = 28;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
      section.nData     = 15;
      section.data(15)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.HILWrite_PWORK
	  section.data(1).logicalSrcIdx = 29;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILWatchdog_PWORK
	  section.data(2).logicalSrcIdx = 30;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Scope_PWORK.LoggedData
	  section.data(3).logicalSrcIdx = 31;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Scope1_PWORK.LoggedData
	  section.data(4).logicalSrcIdx = 32;
	  section.data(4).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_DW.TableMotorVoltageV_PWORK.LoggedData
	  section.data(5).logicalSrcIdx = 33;
	  section.data(5).dtTransOffset = 4;
	
	  ;% q_2xamd1_stII_q8_usb_DW.TablePositioncm_PWORK.LoggedData
	  section.data(6).logicalSrcIdx = 34;
	  section.data(6).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_DW.ToHostFile_PWORK
	  section.data(7).logicalSrcIdx = 35;
	  section.data(7).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_DW.VmV_PWORK.LoggedData
	  section.data(8).logicalSrcIdx = 36;
	  section.data(8).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_DW.x_ddotg_PWORK.LoggedData
	  section.data(9).logicalSrcIdx = 37;
	  section.data(9).dtTransOffset = 9;
	
	  ;% q_2xamd1_stII_q8_usb_DW.xf1mm_PWORK.LoggedData
	  section.data(10).logicalSrcIdx = 38;
	  section.data(10).dtTransOffset = 10;
	
	  ;% q_2xamd1_stII_q8_usb_DW.xf1_ddotms2_PWORK.LoggedData
	  section.data(11).logicalSrcIdx = 39;
	  section.data(11).dtTransOffset = 11;
	
	  ;% q_2xamd1_stII_q8_usb_DW.xf2mm_PWORK.LoggedData
	  section.data(12).logicalSrcIdx = 40;
	  section.data(12).dtTransOffset = 12;
	
	  ;% q_2xamd1_stII_q8_usb_DW.xf2_ddotms2_PWORK.LoggedData
	  section.data(13).logicalSrcIdx = 41;
	  section.data(13).dtTransOffset = 13;
	
	  ;% q_2xamd1_stII_q8_usb_DW.PVControlxc1mm_PWORK.LoggedData
	  section.data(14).logicalSrcIdx = 42;
	  section.data(14).dtTransOffset = 14;
	
	  ;% q_2xamd1_stII_q8_usb_DW.PVControlxc2mm_PWORK.LoggedData
	  section.data(15).logicalSrcIdx = 43;
	  section.data(15).dtTransOffset = 15;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(6) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_ClockModes
	  section.data(1).logicalSrcIdx = 44;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_DOStates
	  section.data(2).logicalSrcIdx = 45;
	  section.data(2).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_QuadratureModes
	  section.data(3).logicalSrcIdx = 46;
	  section.data(3).dtTransOffset = 11;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_InitialEICounts
	  section.data(4).logicalSrcIdx = 47;
	  section.data(4).dtTransOffset = 19;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_POModeValues
	  section.data(5).logicalSrcIdx = 48;
	  section.data(5).dtTransOffset = 27;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_POAlignValues
	  section.data(6).logicalSrcIdx = 49;
	  section.data(6).dtTransOffset = 35;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_POPolarityVals
	  section.data(7).logicalSrcIdx = 50;
	  section.data(7).dtTransOffset = 43;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILReadTimebase_EncoderBuffer
	  section.data(8).logicalSrcIdx = 51;
	  section.data(8).dtTransOffset = 51;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(7) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedChans
	  section.data(1).logicalSrcIdx = 52;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.ToHostFile_SamplesCount
	  section.data(2).logicalSrcIdx = 53;
	  section.data(2).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_DW.ToHostFile_ArrayNameLength
	  section.data(3).logicalSrcIdx = 54;
	  section.data(3).dtTransOffset = 9;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(8) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK
	  section.data(1).logicalSrcIdx = 55;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State
	  section.data(2).logicalSrcIdx = 56;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_i
	  section.data(3).logicalSrcIdx = 57;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_p
	  section.data(4).logicalSrcIdx = 58;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(9) = section;
      clear section
      
      section.nData     = 11;
      section.data(11)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_PrevRese
	  section.data(1).logicalSrcIdx = 59;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.RampSignal_SubsysRanBC
	  section.data(2).logicalSrcIdx = 60;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Checkifstageathome_SubsysRanBC
	  section.data(3).logicalSrcIdx = 61;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Checkforampfault_SubsysRanBC
	  section.data(4).logicalSrcIdx = 62;
	  section.data(4).dtTransOffset = 3;
	
	  ;% q_2xamd1_stII_q8_usb_DW.TopCartPositionWatchdog_SubsysR
	  section.data(5).logicalSrcIdx = 63;
	  section.data(5).dtTransOffset = 4;
	
	  ;% q_2xamd1_stII_q8_usb_DW.BottomCartPositionWatchdog_Subs
	  section.data(6).logicalSrcIdx = 64;
	  section.data(6).dtTransOffset = 5;
	
	  ;% q_2xamd1_stII_q8_usb_DW.SwitchCase_ActiveSubsystem
	  section.data(7).logicalSrcIdx = 65;
	  section.data(7).dtTransOffset = 6;
	
	  ;% q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem2_Subs
	  section.data(8).logicalSrcIdx = 66;
	  section.data(8).dtTransOffset = 7;
	
	  ;% q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem1_Subs
	  section.data(9).logicalSrcIdx = 67;
	  section.data(9).dtTransOffset = 8;
	
	  ;% q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem_Subsy
	  section.data(10).logicalSrcIdx = 68;
	  section.data(10).dtTransOffset = 9;
	
	  ;% q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_SubsysRanB
	  section.data(11).logicalSrcIdx = 69;
	  section.data(11).dtTransOffset = 10;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(10) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag
	  section.data(1).logicalSrcIdx = 70;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag_b
	  section.data(2).logicalSrcIdx = 71;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(11) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.HILInitialize_DOBits
	  section.data(1).logicalSrcIdx = 72;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.HILWatchdog_IsStarted
	  section.data(2).logicalSrcIdx = 73;
	  section.data(2).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.HILWrite_DigitalBuffer
	  section.data(1).logicalSrcIdx = 74;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(13) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% q_2xamd1_stII_q8_usb_DW.RampSignal_MODE
	  section.data(1).logicalSrcIdx = 75;
	  section.data(1).dtTransOffset = 0;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Checkifstageathome_MODE
	  section.data(2).logicalSrcIdx = 76;
	  section.data(2).dtTransOffset = 1;
	
	  ;% q_2xamd1_stII_q8_usb_DW.Checkforampfault_MODE
	  section.data(3).logicalSrcIdx = 77;
	  section.data(3).dtTransOffset = 2;
	
	  ;% q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_MODE
	  section.data(4).logicalSrcIdx = 78;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(14) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 4222123335;
  targMap.checksum1 = 3457791298;
  targMap.checksum2 = 2254480788;
  targMap.checksum3 = 3193314352;

