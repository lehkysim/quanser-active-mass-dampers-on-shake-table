/*
 * q_2xamd1_stII_q8_usb.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "q_2xamd1_stII_q8_usb".
 *
 * Model version              : 6.24
 * Simulink Coder version : 9.5 (R2021a) 14-Nov-2020
 * C source code generated on : Wed May 15 16:05:53 2024
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "q_2xamd1_stII_q8_usb.h"
#include "q_2xamd1_stII_q8_usb_private.h"
#include "q_2xamd1_stII_q8_usb_dt.h"

/* Block signals (default storage) */
B_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_B;

/* Continuous states */
X_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_X;

/* Block states (default storage) */
DW_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_DW;

/* Real-time model */
static RT_MODEL_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_M_;
RT_MODEL_q_2xamd1_stII_q8_usb_T *const q_2xamd1_stII_q8_usb_M =
  &q_2xamd1_stII_q8_usb_M_;

/*
 * This function updates continuous states using the ODE4 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE4_IntgData *id = (ODE4_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T *f3 = id->f[3];
  real_T temp;
  int_T i;
  int_T nXc = 35;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  q_2xamd1_stII_q8_usb_derivatives();

  /* f1 = f(t + (h/2), y + (h/2)*f0) */
  temp = 0.5 * h;
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (temp*f0[i]);
  }

  rtsiSetT(si, t + temp);
  rtsiSetdX(si, f1);
  q_2xamd1_stII_q8_usb_output();
  q_2xamd1_stII_q8_usb_derivatives();

  /* f2 = f(t + (h/2), y + (h/2)*f1) */
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (temp*f1[i]);
  }

  rtsiSetdX(si, f2);
  q_2xamd1_stII_q8_usb_output();
  q_2xamd1_stII_q8_usb_derivatives();

  /* f3 = f(t + h, y + h*f2) */
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (h*f2[i]);
  }

  rtsiSetT(si, tnew);
  rtsiSetdX(si, f3);
  q_2xamd1_stII_q8_usb_output();
  q_2xamd1_stII_q8_usb_derivatives();

  /* tnew = t + h
     ynew = y + (h/6)*(f0 + 2*f1 + 2*f2 + 2*f3) */
  temp = h / 6.0;
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + temp*(f0[i] + 2.0*f1[i] + 2.0*f2[i] + f3[i]);
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

real_T rt_modd_snf(real_T u0, real_T u1)
{
  real_T q;
  real_T y;
  boolean_T yEq;
  y = u0;
  if (u1 == 0.0) {
    if (u0 == 0.0) {
      y = u1;
    }
  } else if (rtIsNaN(u0) || rtIsNaN(u1) || rtIsInf(u0)) {
    y = (rtNaN);
  } else if (u0 == 0.0) {
    y = 0.0 / u1;
  } else if (rtIsInf(u1)) {
    if ((u1 < 0.0) != (u0 < 0.0)) {
      y = u1;
    }
  } else {
    y = fmod(u0, u1);
    yEq = (y == 0.0);
    if ((!yEq) && (u1 > floor(u1))) {
      q = fabs(u0 / u1);
      yEq = !(fabs(q - floor(q + 0.5)) > DBL_EPSILON * q);
    }

    if (yEq) {
      y = u1 * 0.0;
    } else if ((u0 < 0.0) != (u1 < 0.0)) {
      y += u1;
    }
  }

  return y;
}

/* Model output function */
void q_2xamd1_stII_q8_usb_output(void)
{
  /* local block i/o variables */
  uint8_T rtb_Compare;
  uint8_T rtb_Compare_f;
  real_T rtb_HILReadTimebase_o2[3];
  real_T rtb_HILReadTimebase_o4;
  real_T rtb_SmoothSignalGenerator;
  boolean_T rtb_HILWatchdog;
  real_T tmp[16];
  real_T tmp_0[8];
  real_T tmp_1[8];
  real_T tmp_2[4];
  real_T tmp_3[4];
  real_T rtb_StateFeedbackGainVector[2];
  real_T rtb_StateSpace[2];
  real_T rtb_Integrator1_k_tmp;
  real_T rtb_SliderGain;
  real_T rtb_SliderGain_l;
  real_T rtb_SliderGain_m;
  real_T rtb_Sum_j_idx_2;
  real_T u0;
  real_T *lastU;
  int32_T i;
  int32_T i_0;
  uint32_T ri;
  int8_T rtAction;
  int8_T rtPrevAction;
  boolean_T rtb_Compare_lt;
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* set solver stop time */
    if (!(q_2xamd1_stII_q8_usb_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&q_2xamd1_stII_q8_usb_M->solverInfo,
                            ((q_2xamd1_stII_q8_usb_M->Timing.clockTickH0 + 1) *
        q_2xamd1_stII_q8_usb_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&q_2xamd1_stII_q8_usb_M->solverInfo,
                            ((q_2xamd1_stII_q8_usb_M->Timing.clockTick0 + 1) *
        q_2xamd1_stII_q8_usb_M->Timing.stepSize0 +
        q_2xamd1_stII_q8_usb_M->Timing.clockTickH0 *
        q_2xamd1_stII_q8_usb_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    q_2xamd1_stII_q8_usb_M->Timing.t[0] = rtsiGetT
      (&q_2xamd1_stII_q8_usb_M->solverInfo);
  }

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_SubsysRanB);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem_Subsy);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem1_Subs);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem2_Subs);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.BottomCartPositionWatchdog_Subs);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.TopCartPositionWatchdog_SubsysR);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.Checkforampfault_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.Checkifstageathome_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(q_2xamd1_stII_q8_usb_DW.RampSignal_SubsysRanBC);
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* S-Function (hil_read_timebase_block): '<S7>/HIL Read Timebase' */

    /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL Read Timebase (hil_read_timebase_block) */
    {
      t_error result;
      result = hil_task_read(q_2xamd1_stII_q8_usb_DW.HILReadTimebase_Task, 1,
        &q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[0],
        &q_2xamd1_stII_q8_usb_DW.HILReadTimebase_EncoderBuffer[0],
        (t_boolean *)&q_2xamd1_stII_q8_usb_B.HILReadTimebase_o3[0],
        &rtb_HILReadTimebase_o4
        );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
      } else {
        rtb_HILReadTimebase_o2[0] =
          q_2xamd1_stII_q8_usb_DW.HILReadTimebase_EncoderBuffer[0];
        rtb_HILReadTimebase_o2[1] =
          q_2xamd1_stII_q8_usb_DW.HILReadTimebase_EncoderBuffer[1];
        rtb_HILReadTimebase_o2[2] =
          q_2xamd1_stII_q8_usb_DW.HILReadTimebase_EncoderBuffer[2];
      }
    }
  }

  /* Clock: '<S7>/Clock' incorporates:
   *  Clock: '<S10>/Clock'
   *  Clock: '<S56>/Clock'
   *  Clock: '<S57>/Clock'
   *  Derivative: '<S49>/Derivative'
   */
  rtb_Integrator1_k_tmp = q_2xamd1_stII_q8_usb_M->Timing.t[0];

  /* RelationalOperator: '<S30>/Compare' incorporates:
   *  Clock: '<S7>/Clock'
   *  Constant: '<S30>/Constant'
   */
  rtb_Compare = (uint8_T)(rtb_Integrator1_k_tmp >
    q_2xamd1_stII_q8_usb_P.After2sec_const);

  /* Outputs for Enabled SubSystem: '<S7>/Check for amp fault' incorporates:
   *  EnablePort: '<S31>/Enable'
   */
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    q_2xamd1_stII_q8_usb_DW.Checkforampfault_MODE = (rtb_Compare > 0);
  }

  if (q_2xamd1_stII_q8_usb_DW.Checkforampfault_MODE) {
    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      /* S-Function (hil_watchdog_block): '<S35>/HIL Watchdog' incorporates:
       *  Constant: '<S40>/Constant'
       *  RelationalOperator: '<S40>/Compare'
       */
      rtb_HILWatchdog = (q_2xamd1_stII_q8_usb_B.HILReadTimebase_o3[3] !=
                         q_2xamd1_stII_q8_usb_P.Constant_Value_l);

      /* Stop: '<S39>/Stop Simulation' */
      if (rtb_HILWatchdog) {
        rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
      }

      /* End of Stop: '<S39>/Stop Simulation' */
    }

    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      srUpdateBC(q_2xamd1_stII_q8_usb_DW.Checkforampfault_SubsysRanBC);
    }
  }

  /* End of Outputs for SubSystem: '<S7>/Check for amp fault' */

  /* RelationalOperator: '<S33>/Compare' incorporates:
   *  Clock: '<S7>/Clock'
   *  Constant: '<S33>/Constant'
   */
  rtb_Compare_f = (uint8_T)(rtb_Integrator1_k_tmp <
    q_2xamd1_stII_q8_usb_P.CompareToConstant_const);

  /* Outputs for Enabled SubSystem: '<S7>/Check if stage at home' incorporates:
   *  EnablePort: '<S32>/Enable'
   */
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    q_2xamd1_stII_q8_usb_DW.Checkifstageathome_MODE = (rtb_Compare_f > 0);
  }

  if (q_2xamd1_stII_q8_usb_DW.Checkifstageathome_MODE) {
    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      /* Logic: '<S32>/Logical Operator' */
      q_2xamd1_stII_q8_usb_B.LogicalOperator =
        !q_2xamd1_stII_q8_usb_B.HILReadTimebase_o3[1];

      /* Stop: '<S41>/Stop Simulation' incorporates:
       *  Constant: '<S42>/Constant'
       *  RelationalOperator: '<S42>/Compare'
       */
      if (q_2xamd1_stII_q8_usb_B.LogicalOperator !=
          q_2xamd1_stII_q8_usb_P.Constant_Value_c) {
        rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
      }

      /* End of Stop: '<S41>/Stop Simulation' */
    }

    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      srUpdateBC(q_2xamd1_stII_q8_usb_DW.Checkifstageathome_SubsysRanBC);
    }
  }

  /* End of Outputs for SubSystem: '<S7>/Check if stage at home' */
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* DataTypeConversion: '<S43>/Data Type Conversion5' */
    q_2xamd1_stII_q8_usb_B.DataTypeConversion5 =
      q_2xamd1_stII_q8_usb_B.HILReadTimebase_o3[0];

    /* Gain: '<S43>/To_Samples' incorporates:
     *  Constant: '<S43>/debounce_rise_time'
     */
    rtb_SliderGain_l = q_2xamd1_stII_q8_usb_P.To_Samples_Gain *
      q_2xamd1_stII_q8_usb_P.dbnc_rise_time;

    /* DataTypeConversion: '<S43>/Data Type Conversion' */
    u0 = floor(rtb_SliderGain_l);
    if (rtIsNaN(u0) || rtIsInf(u0)) {
      u0 = 0.0;
    } else {
      u0 = fmod(u0, 4.294967296E+9);
    }

    /* DataTypeConversion: '<S43>/Data Type Conversion2' incorporates:
     *  DataTypeConversion: '<S43>/Data Type Conversion'
     */
    q_2xamd1_stII_q8_usb_B.DataTypeConversion2 = u0 < 0.0 ? -(int32_T)(uint32_T)
      -u0 : (int32_T)(uint32_T)u0;

    /* Gain: '<S43>/To_Samples1' incorporates:
     *  Constant: '<S43>/debounce_fall_time'
     */
    rtb_SliderGain_l = q_2xamd1_stII_q8_usb_P.To_Samples1_Gain *
      q_2xamd1_stII_q8_usb_P.debouncerleft_fall_time;

    /* DataTypeConversion: '<S43>/Data Type Conversion1' */
    u0 = floor(rtb_SliderGain_l);
    if (rtIsNaN(u0) || rtIsInf(u0)) {
      u0 = 0.0;
    } else {
      u0 = fmod(u0, 4.294967296E+9);
    }

    /* DataTypeConversion: '<S43>/Data Type Conversion3' incorporates:
     *  DataTypeConversion: '<S43>/Data Type Conversion1'
     */
    q_2xamd1_stII_q8_usb_B.DataTypeConversion3 = u0 < 0.0 ? -(int32_T)(uint32_T)
      -u0 : (int32_T)(uint32_T)u0;

    /* S-Function (debouncer_block): '<S43>/debouncer_block' incorporates:
     *  Constant: '<S43>/threshold'
     */
    {
      if (q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[2] == 0.0 ) {
        q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[2] =
          q_2xamd1_stII_q8_usb_P.debouncer_block_init_state;
        if (q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[2] == 2.0 )
          q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] =
            q_2xamd1_stII_q8_usb_B.DataTypeConversion3;
        else
          q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] =
            q_2xamd1_stII_q8_usb_B.DataTypeConversion2;
        q_2xamd1_stII_q8_usb_B.debouncer_block =
          q_2xamd1_stII_q8_usb_P.debouncer_block_init_state - 1 > 0.0 ? true :
          false;
      } else {
        if (q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] > 0.0 )
          q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] -= 1.0;
        else {
          q_2xamd1_stII_q8_usb_B.debouncer_block =
            q_2xamd1_stII_q8_usb_B.DataTypeConversion5 >
            q_2xamd1_stII_q8_usb_P.dbnc_threshold ? true : false;
          if (q_2xamd1_stII_q8_usb_B.debouncer_block == 1.0 )
            q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] =
              q_2xamd1_stII_q8_usb_B.DataTypeConversion3;
          else
            q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] =
              q_2xamd1_stII_q8_usb_B.DataTypeConversion2;
        }
      }

      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[0] =
        q_2xamd1_stII_q8_usb_B.debouncer_block;
    }

    /* Stop: '<S37>/Stop Simulation' incorporates:
     *  Constant: '<S45>/Constant'
     *  RelationalOperator: '<S45>/Compare'
     */
    if (q_2xamd1_stII_q8_usb_B.debouncer_block !=
        q_2xamd1_stII_q8_usb_P.Constant_Value_kw) {
      rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
    }

    /* End of Stop: '<S37>/Stop Simulation' */

    /* DataTypeConversion: '<S44>/Data Type Conversion5' */
    q_2xamd1_stII_q8_usb_B.DataTypeConversion5_h =
      q_2xamd1_stII_q8_usb_B.HILReadTimebase_o3[2];

    /* Gain: '<S44>/To_Samples' incorporates:
     *  Constant: '<S44>/debounce_rise_time'
     */
    rtb_SliderGain_l = q_2xamd1_stII_q8_usb_P.To_Samples_Gain_c *
      q_2xamd1_stII_q8_usb_P.dbnc_rise_time;

    /* DataTypeConversion: '<S44>/Data Type Conversion' */
    u0 = floor(rtb_SliderGain_l);
    if (rtIsNaN(u0) || rtIsInf(u0)) {
      u0 = 0.0;
    } else {
      u0 = fmod(u0, 4.294967296E+9);
    }

    /* DataTypeConversion: '<S44>/Data Type Conversion2' incorporates:
     *  DataTypeConversion: '<S44>/Data Type Conversion'
     */
    q_2xamd1_stII_q8_usb_B.DataTypeConversion2_b = u0 < 0.0 ? -(int32_T)
      (uint32_T)-u0 : (int32_T)(uint32_T)u0;

    /* Gain: '<S44>/To_Samples1' incorporates:
     *  Constant: '<S44>/debounce_fall_time'
     */
    rtb_SliderGain_l = q_2xamd1_stII_q8_usb_P.To_Samples1_Gain_n *
      q_2xamd1_stII_q8_usb_P.debouncerright_fall_time;

    /* DataTypeConversion: '<S44>/Data Type Conversion1' */
    u0 = floor(rtb_SliderGain_l);
    if (rtIsNaN(u0) || rtIsInf(u0)) {
      u0 = 0.0;
    } else {
      u0 = fmod(u0, 4.294967296E+9);
    }

    /* DataTypeConversion: '<S44>/Data Type Conversion3' incorporates:
     *  DataTypeConversion: '<S44>/Data Type Conversion1'
     */
    q_2xamd1_stII_q8_usb_B.DataTypeConversion3_a = u0 < 0.0 ? -(int32_T)
      (uint32_T)-u0 : (int32_T)(uint32_T)u0;

    /* S-Function (debouncer_block): '<S44>/debouncer_block' incorporates:
     *  Constant: '<S44>/threshold'
     */
    {
      if (q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[2] == 0.0 ) {
        q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[2] =
          q_2xamd1_stII_q8_usb_P.debouncer_block_init_state_c;
        if (q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[2] == 2.0 )
          q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] =
            q_2xamd1_stII_q8_usb_B.DataTypeConversion3_a;
        else
          q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] =
            q_2xamd1_stII_q8_usb_B.DataTypeConversion2_b;
        q_2xamd1_stII_q8_usb_B.debouncer_block_p =
          q_2xamd1_stII_q8_usb_P.debouncer_block_init_state_c - 1 > 0.0 ? true :
          false;
      } else {
        if (q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] > 0.0 )
          q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] -= 1.0;
        else {
          q_2xamd1_stII_q8_usb_B.debouncer_block_p =
            q_2xamd1_stII_q8_usb_B.DataTypeConversion5_h >
            q_2xamd1_stII_q8_usb_P.dbnc_threshold ? true : false;
          if (q_2xamd1_stII_q8_usb_B.debouncer_block_p == 1.0 )
            q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] =
              q_2xamd1_stII_q8_usb_B.DataTypeConversion3_a;
          else
            q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] =
              q_2xamd1_stII_q8_usb_B.DataTypeConversion2_b;
        }
      }

      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[0] =
        q_2xamd1_stII_q8_usb_B.debouncer_block_p;
    }

    /* Stop: '<S38>/Stop Simulation' incorporates:
     *  Constant: '<S46>/Constant'
     *  RelationalOperator: '<S46>/Compare'
     */
    if (q_2xamd1_stII_q8_usb_B.debouncer_block_p !=
        q_2xamd1_stII_q8_usb_P.Constant_Value_c1) {
      rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
    }

    /* End of Stop: '<S38>/Stop Simulation' */
  }

  /* S-Function (continuous_sigmoid_block): '<S10>/Continuous Sigmoid' */
  /* S-Function Block: q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Continuous Sigmoid (continuous_sigmoid_block) */
  {
    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      {
        /* Determine the correct step size for the output ports */

        /*  Check if the target position has changed or if it is the first
           simulation step, in which case we need to compute the initial
           trajectory. */
        if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag == 1 ||
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time == 0) {
          /*  Read the new position and reset the flag */
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_XD]
            = q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Target;
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag = 0;

          /*  Calculate a single step using the new trajectory to account for the target
             change that occured at the previous sample (i.e., there is a one sample delay
             between the point where the target position changes and the trajectory changes
             based on the new target)    */
          if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time == 0) {
            /*  Do not update the trajectory since we are at time 0 in simulation
               (leave t = 0)   */
          } else {
            /*  Set the relative time to one step size so that a single step
               is computed for the new trajectory  */

            /*  Set the default values in sigmoid_parameters to the trajectory
               state at the last time step.    */
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X0]
              = q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PPos;
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_V0]
              = q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PVel;
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time = 0.0005;
          }

          /*  Calculate the new trajectory    */
          sigmoid_calculate_trajectory
            (&q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[0], 0,
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X0],
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_V0],
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MAcc,
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MVel);
        }

        /* Mark the target as not reached */
        q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4 = 0;
        if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time <
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T1])
        {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_AP];
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_V0]
            + q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_AP]
            * q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X0]
            + (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_V0]
               + 0.5 *
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_AP]
               * q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time) *
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time;
        } else if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time <
                   q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T2])
        {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3 = 0;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_VP];
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X1]
            + q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_VP]
            * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T1]);
        } else if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time <
                   q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T3])
        {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3 =
            -q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_AP];
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_VP]
            - q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_AP]
            * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T2]);
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X2]
            + (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_VP]
               - 0.5 *
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_AP]
               * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time -
                  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T2]))
            * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T2]);
        } else {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3 = 0;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2 = 0;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1 =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_XD];/* always attainable */

          /* Mark the target as reached after the hold-off period */
          if ((q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T3])
              >=
              q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_HOLD_OFF])
            q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4 = 1;/* Assert Done signal */
        }

        /*  Save the current position/velocity to be used next time step (possibly) */
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PPos =
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PVel =
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2;

        /*  Increment the relative time RWork to the NEXT time step */
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time += 0.0005;
      }
    }
  }

  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* Gain: '<S62>/Slider Gain' */
    rtb_SliderGain = q_2xamd1_stII_q8_usb_P.VelocitySetpointWeight_gain *
      q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2;
  }

  /* S-Function (continuous_sigmoid_block): '<S9>/Continuous Sigmoid' */
  /* S-Function Block: q_2xamd1_stII_q8_usb/Sine Sweep Setpoint/Continuous Sigmoid (continuous_sigmoid_block) */
  {
    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      {
        /* Determine the correct step size for the output ports */

        /*  Check if the target position has changed or if it is the first
           simulation step, in which case we need to compute the initial
           trajectory. */
        if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag_b == 1 ||
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time == 0) {
          /*  Read the new position and reset the flag */
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_XD]
            = q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Target;
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag_b = 0;

          /*  Calculate a single step using the new trajectory to account for the target
             change that occured at the previous sample (i.e., there is a one sample delay
             between the point where the target position changes and the trajectory changes
             based on the new target)    */
          if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time == 0) {
            /*  Do not update the trajectory since we are at time 0 in simulation
               (leave t = 0)   */
          } else {
            /*  Set the relative time to one step size so that a single step
               is computed for the new trajectory  */

            /*  Set the default values in sigmoid_parameters to the trajectory
               state at the last time step.    */
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X0]
              = q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PPos;
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_V0]
              = q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PVel;
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time = 0.0005;
          }

          /*  Calculate the new trajectory    */
          sigmoid_calculate_trajectory
            (&q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[0], 0,
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X0],
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_V0],
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MAcc,
             q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MVel);
        }

        /* Mark the target as not reached */
        q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4_k = 0;
        if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time <
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T1])
        {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3_b =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_AP];
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_V0]
            + q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_AP]
            * q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X0]
            + (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_V0]
               + 0.5 *
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_AP]
               * q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time) *
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time;
        } else if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time <
                   q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T2])
        {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3_b = 0;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_VP];
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X1]
            + q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_VP]
            * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T1]);
        } else if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time <
                   q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T3])
        {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3_b =
            -q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_AP];
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_VP]
            - q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_AP]
            * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T2]);
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X2]
            + (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_VP]
               - 0.5 *
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_AP]
               * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time -
                  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T2]))
            * (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T2]);
        } else {
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3_b = 0;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m = 0;
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g =
            q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_XD];/* always attainable */

          /* Mark the target as reached after the hold-off period */
          if ((q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time -
               q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T3])
              >=
              q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_HOLD_OFF])
            q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4_k = 1;/* Assert Done signal */
        }

        /*  Save the current position/velocity to be used next time step (possibly) */
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PPos =
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PVel =
          q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m;

        /*  Increment the relative time RWork to the NEXT time step */
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time += 0.0005;
      }
    }
  }

  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* MultiPortSwitch: '<Root>/Setpoint Switch' incorporates:
     *  Constant: '<Root>/Shaker II Setpoint Selection'
     */
    switch ((int32_T)q_2xamd1_stII_q8_usb_P.ShakerIISetpointSelection_Value) {
     case 1:
      /* MultiPortSwitch: '<Root>/Setpoint Switch' incorporates:
       *  Constant: '<S3>/Position Setpoint (m)'
       *  Constant: '<S3>/Velocity Setpoint (m//s)'
       */
      q_2xamd1_stII_q8_usb_B.SetpointSwitch[0] =
        q_2xamd1_stII_q8_usb_P.PositionSetpointm_Value;
      q_2xamd1_stII_q8_usb_B.SetpointSwitch[1] =
        q_2xamd1_stII_q8_usb_P.VelocitySetpointms_Value;
      break;

     case 2:
      /* MultiPortSwitch: '<Root>/Setpoint Switch' */
      q_2xamd1_stII_q8_usb_B.SetpointSwitch[0] =
        q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1;
      q_2xamd1_stII_q8_usb_B.SetpointSwitch[1] = rtb_SliderGain;
      break;

     default:
      /* MultiPortSwitch: '<Root>/Setpoint Switch' incorporates:
       *  Gain: '<S58>/Slider Gain'
       */
      q_2xamd1_stII_q8_usb_B.SetpointSwitch[0] =
        q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g;
      q_2xamd1_stII_q8_usb_B.SetpointSwitch[1] =
        q_2xamd1_stII_q8_usb_P.VelocitySetpointWeight_gain_l *
        q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m;
      break;
    }

    /* End of MultiPortSwitch: '<Root>/Setpoint Switch' */

    /* Gain: '<S49>/Encoder Calibration (m//count)' */
    q_2xamd1_stII_q8_usb_B.EncoderCalibrationmcount =
      q_2xamd1_stII_q8_usb_P.K_ENC * rtb_HILReadTimebase_o2[0];

    /* Sum: '<S47>/Sum' */
    rtb_SliderGain_l = q_2xamd1_stII_q8_usb_B.SetpointSwitch[0] -
      q_2xamd1_stII_q8_usb_B.EncoderCalibrationmcount;

    /* Gain: '<S47>/Proportional Gain (V//m)' */
    q_2xamd1_stII_q8_usb_B.ProportionalGainVm = q_2xamd1_stII_q8_usb_P.kp *
      rtb_SliderGain_l;

    /* Constant: '<S51>/x0' */
    q_2xamd1_stII_q8_usb_B.x0 = q_2xamd1_stII_q8_usb_P.x0_Value;
  }

  /* Integrator: '<S51>/Integrator1' */
  if (q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK != 0) {
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE = q_2xamd1_stII_q8_usb_B.x0;
  }

  /* Sum: '<S47>/Add' incorporates:
   *  Gain: '<S47>/Derivative Gain (V-s//m)'
   *  Integrator: '<S51>/Integrator1'
   *  Sum: '<S47>/Sum1'
   */
  q_2xamd1_stII_q8_usb_B.Add = (q_2xamd1_stII_q8_usb_B.SetpointSwitch[1] -
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE) * q_2xamd1_stII_q8_usb_P.kd +
    q_2xamd1_stII_q8_usb_B.ProportionalGainVm;
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* S-Function (multistage_limiter_block): '<S48>/Voltage Limit' */
    {
      double value;
      value = fabs(q_2xamd1_stII_q8_usb_B.Add);
      switch (q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State)
      {
       case 0:
        if (value > q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim) {
          q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State = 1;
          q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.Count = 0;
          q_2xamd1_stII_q8_usb_DW.VoltageLimit_RWORK.Mean = value;
          if (value > q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim)
            q_2xamd1_stII_q8_usb_B.VoltageLimit = (q_2xamd1_stII_q8_usb_B.Add >
              0) ? q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim :
              -q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim;
          else
            q_2xamd1_stII_q8_usb_B.VoltageLimit = q_2xamd1_stII_q8_usb_B.Add;
        } else
          q_2xamd1_stII_q8_usb_B.VoltageLimit = q_2xamd1_stII_q8_usb_B.Add;
        break;

       case 1:
        q_2xamd1_stII_q8_usb_DW.VoltageLimit_RWORK.Mean += value;
        q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.Count++;
        if (q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.Count * 0.0005 >=
            q_2xamd1_stII_q8_usb_P.VoltageLimit_PTime) {
          q_2xamd1_stII_q8_usb_DW.VoltageLimit_RWORK.Mean /=
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.Count + 1;
          if (q_2xamd1_stII_q8_usb_DW.VoltageLimit_RWORK.Mean >
              q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim && value >
              q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim) {
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State = 2;
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.Count = 0;
            q_2xamd1_stII_q8_usb_B.VoltageLimit = (q_2xamd1_stII_q8_usb_B.Add >
              0) ? q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim :
              -q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim;
          } else {
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State = 0;
            q_2xamd1_stII_q8_usb_B.VoltageLimit = q_2xamd1_stII_q8_usb_B.Add;
          }
        } else if (value > q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim)
          q_2xamd1_stII_q8_usb_B.VoltageLimit = (q_2xamd1_stII_q8_usb_B.Add > 0)
            ? q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim :
            -q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim;
        else
          q_2xamd1_stII_q8_usb_B.VoltageLimit = q_2xamd1_stII_q8_usb_B.Add;
        break;

       case 2:
        if (++q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.Count * 0.0005 >=
            q_2xamd1_stII_q8_usb_P.VoltageLimit_CTime) {
          if (value > q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim) {
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State = 1;
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.Count = 0;
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_RWORK.Mean = value;
            if (value > q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim)
              q_2xamd1_stII_q8_usb_B.VoltageLimit = (q_2xamd1_stII_q8_usb_B.Add >
                0) ? q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim :
                -q_2xamd1_stII_q8_usb_P.VoltageLimit_PLim;
            else
              q_2xamd1_stII_q8_usb_B.VoltageLimit = q_2xamd1_stII_q8_usb_B.Add;
          } else {
            q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State = 0;
            q_2xamd1_stII_q8_usb_B.VoltageLimit = q_2xamd1_stII_q8_usb_B.Add;
          }
        } else if (value > q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim)
          q_2xamd1_stII_q8_usb_B.VoltageLimit = (q_2xamd1_stII_q8_usb_B.Add > 0)
            ? q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim :
            -q_2xamd1_stII_q8_usb_P.VoltageLimit_CLim;
        else
          q_2xamd1_stII_q8_usb_B.VoltageLimit = q_2xamd1_stII_q8_usb_B.Add;
        break;
      }
    }

    /* Gain: '<S48>/Inverse Amp  Gain (V//A)' */
    rtb_SliderGain_l = 1.0 / q_2xamd1_stII_q8_usb_P.K_AMP *
      q_2xamd1_stII_q8_usb_B.VoltageLimit;

    /* ManualSwitch: '<Root>/AMD Control Mode' */
    if (q_2xamd1_stII_q8_usb_P.AMDControlMode_CurrentSetting == 1) {
      /* ManualSwitch: '<Root>/AMD Control Mode' incorporates:
       *  Constant: '<Root>/On'
       */
      q_2xamd1_stII_q8_usb_B.AMDControlMode = q_2xamd1_stII_q8_usb_P.On_Value;
    } else {
      /* ManualSwitch: '<Root>/AMD Control Mode' incorporates:
       *  Constant: '<Root>/Off'
       */
      q_2xamd1_stII_q8_usb_B.AMDControlMode = q_2xamd1_stII_q8_usb_P.Off_Value;
    }

    /* End of ManualSwitch: '<Root>/AMD Control Mode' */
  }

  /* Integrator: '<S63>/Integrator' */
  memcpy(&q_2xamd1_stII_q8_usb_B.Integrator[0],
         &q_2xamd1_stII_q8_usb_X.Integrator_CSTATE[0], sizeof(real_T) << 3U);

  /* Gain: '<S11>/State-Feedback  Gain Vector' */
  for (i = 0; i < 16; i++) {
    tmp[i] = -q_2xamd1_stII_q8_usb_P.Kamd[i];
  }

  for (i = 0; i < 2; i++) {
    rtb_StateFeedbackGainVector[i] = 0.0;
    for (i_0 = 0; i_0 < 8; i_0++) {
      rtb_StateFeedbackGainVector[i] += tmp[(i_0 << 1) + i] *
        q_2xamd1_stII_q8_usb_B.Integrator[i_0];
    }
  }

  /* End of Gain: '<S11>/State-Feedback  Gain Vector' */

  /* MultiPortSwitch: '<S11>/Mode Switch' incorporates:
   *  Constant: '<S5>/Constant'
   *  Constant: '<S6>/Constant'
   *  Gain: '<S5>/Kp_c: 274.62'
   *  Gain: '<S5>/Kv_c: 5.56'
   *  Gain: '<S6>/Kp_c: 274.62'
   *  Gain: '<S6>/Kv_c: 5.56'
   *  Sum: '<S5>/Sum2'
   *  Sum: '<S5>/Sum3'
   *  Sum: '<S6>/Sum2'
   *  Sum: '<S6>/Sum3'
   */
  if ((int32_T)q_2xamd1_stII_q8_usb_B.AMDControlMode == 1) {
    rtb_StateSpace[0] = (q_2xamd1_stII_q8_usb_P.Constant_Value_g3 -
                         q_2xamd1_stII_q8_usb_B.Integrator[0]) *
      q_2xamd1_stII_q8_usb_P.Kp - q_2xamd1_stII_q8_usb_P.Kv *
      q_2xamd1_stII_q8_usb_B.Integrator[4];
    rtb_StateSpace[1] = (q_2xamd1_stII_q8_usb_P.Constant_Value_e -
                         q_2xamd1_stII_q8_usb_B.Integrator[1]) *
      q_2xamd1_stII_q8_usb_P.Kp - q_2xamd1_stII_q8_usb_P.Kv *
      q_2xamd1_stII_q8_usb_B.Integrator[5];
  } else {
    rtb_StateSpace[0] = rtb_StateFeedbackGainVector[0];
    rtb_StateSpace[1] = rtb_StateFeedbackGainVector[1];
  }

  /* End of MultiPortSwitch: '<S11>/Mode Switch' */

  /* Gain: '<S1>/Amplifier Gain Pre-Compensation' */
  rtb_SliderGain = 1.0 / q_2xamd1_stII_q8_usb_P.K_AMD_AMP;

  /* Saturate: '<Root>/Amplifier Voltage Saturation' */
  if (rtb_StateSpace[0] > q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD) {
    /* Saturate: '<Root>/Amplifier Voltage Saturation' */
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[0] =
      q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD;
  } else if (rtb_StateSpace[0] < -q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD) {
    /* Saturate: '<Root>/Amplifier Voltage Saturation' */
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[0] =
      -q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD;
  } else {
    /* Saturate: '<Root>/Amplifier Voltage Saturation' */
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[0] = rtb_StateSpace[0];
  }

  /* Gain: '<S1>/Amplifier Gain Pre-Compensation' */
  q_2xamd1_stII_q8_usb_B.AmplifierGainPreCompensation[0] = rtb_SliderGain *
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[0];

  /* Saturate: '<Root>/Amplifier Voltage Saturation' */
  if (rtb_StateSpace[1] > q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD) {
    /* Saturate: '<Root>/Amplifier Voltage Saturation' */
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[1] =
      q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD;
  } else if (rtb_StateSpace[1] < -q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD) {
    /* Saturate: '<Root>/Amplifier Voltage Saturation' */
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[1] =
      -q_2xamd1_stII_q8_usb_P.VMAX_AMP_AMD;
  } else {
    /* Saturate: '<Root>/Amplifier Voltage Saturation' */
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[1] = rtb_StateSpace[1];
  }

  /* Gain: '<S1>/Amplifier Gain Pre-Compensation' */
  q_2xamd1_stII_q8_usb_B.AmplifierGainPreCompensation[1] = rtb_SliderGain *
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[1];
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* S-Function (hil_write_block): '<S7>/HIL Write' incorporates:
     *  Constant: '<S34>/Calibrate Signal = OFF'
     *  Constant: '<S34>/Enable Signal = ON'
     */

    /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL Write (hil_write_block) */
    {
      t_error result;
      q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer[0] = rtb_SliderGain_l;
      q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer[1] =
        q_2xamd1_stII_q8_usb_B.AmplifierGainPreCompensation[0];
      q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer[2] =
        q_2xamd1_stII_q8_usb_B.AmplifierGainPreCompensation[1];
      q_2xamd1_stII_q8_usb_DW.HILWrite_DigitalBuffer[0] =
        (q_2xamd1_stII_q8_usb_P.CalibrateSignalOFF_Value != 0);
      q_2xamd1_stII_q8_usb_DW.HILWrite_DigitalBuffer[1] =
        (q_2xamd1_stII_q8_usb_P.EnableSignalON_Value != 0);
      result = hil_write(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
                         q_2xamd1_stII_q8_usb_P.HILWrite_analog_channels, 3U,
                         NULL, 0U,
                         q_2xamd1_stII_q8_usb_P.HILWrite_digital_channels, 2U,
                         NULL, 0U,
                         &q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer[0],
                         NULL,
                         &q_2xamd1_stII_q8_usb_DW.HILWrite_DigitalBuffer[0],
                         NULL
                         );
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
      }
    }

    /* S-Function (hil_watchdog_block): '<S35>/HIL Watchdog' */

    /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL STII Watchdog/HIL Watchdog (hil_watchdog_block) */
    {
      t_error result;
      if (q_2xamd1_stII_q8_usb_DW.HILWatchdog_IsStarted) {
        result = hil_watchdog_reload(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
      } else {
        result = hil_watchdog_start(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
          q_2xamd1_stII_q8_usb_P.HILWatchdog_Timeout);
        if (result == 0) {
          q_2xamd1_stII_q8_usb_DW.HILWatchdog_IsStarted = true;
          result = 1;                  /* indicate watchdog has not expired */
        }
      }

      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
      }

      rtb_HILWatchdog = (result == 0);
    }

    /* S-Function (stop_with_error_block): '<S35>/Stop with Error' */

    /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL STII Watchdog/Stop with Error (stop_with_error_block) */
    {
      if (rtb_HILWatchdog) {
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M,
                          "Stopped by Watchdog - controller began running too slow.");
        return;
      }
    }

    /* Gain: '<S1>/Conversion to m1' */
    q_2xamd1_stII_q8_usb_B.Conversiontom1 = q_2xamd1_stII_q8_usb_P.K_EC *
      rtb_HILReadTimebase_o2[1];

    /* Gain: '<S1>/Conversion to m2' */
    q_2xamd1_stII_q8_usb_B.Conversiontom2 = q_2xamd1_stII_q8_usb_P.K_EC *
      rtb_HILReadTimebase_o2[2];

    /* Constant: '<S16>/x0' */
    q_2xamd1_stII_q8_usb_B.x0_f = q_2xamd1_stII_q8_usb_P.x0_Value_f;
  }

  /* Integrator: '<S16>/Integrator1' */
  if (q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_i != 0) {
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[0] = q_2xamd1_stII_q8_usb_B.x0_f;
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[1] = q_2xamd1_stII_q8_usb_B.x0_f;
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[2] = q_2xamd1_stII_q8_usb_B.x0_f;
  }

  /* Gain: '<S1>/floor 1: g to m//s^2' incorporates:
   *  Gain: '<S12>/For acceleration to be same direction as position'
   *  Integrator: '<S16>/Integrator1'
   *  Sum: '<S1>/a1 - a0'
   */
  q_2xamd1_stII_q8_usb_B.floor1gtoms2 =
    (q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[1] -
     q_2xamd1_stII_q8_usb_P.Foraccelerationtobesamedirectio *
     q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[0]) *
    q_2xamd1_stII_q8_usb_P.K_G2MS;

  /* Gain: '<S1>/floor 1: g to m//s^1' incorporates:
   *  Integrator: '<S16>/Integrator1'
   *  Sum: '<S1>/a2 - a1'
   */
  q_2xamd1_stII_q8_usb_B.floor1gtoms1 =
    (q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[2] -
     q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[1]) *
    q_2xamd1_stII_q8_usb_P.K_G2MS;
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
  }

  /* StateSpace: '<S11>/State-Space' */
  rtb_StateSpace[0] = 0.0;
  rtb_StateSpace[1] = 0.0;
  for (i = 0; i < 16; i++) {
    for (ri = q_2xamd1_stII_q8_usb_P.StateSpace_C_jc[(uint32_T)i]; ri <
         q_2xamd1_stII_q8_usb_P.StateSpace_C_jc[i + 1U]; ri++) {
      rtb_StateSpace[q_2xamd1_stII_q8_usb_P.StateSpace_C_ir[ri]] +=
        q_2xamd1_stII_q8_usb_P.StateSpace_C_pr[ri] *
        q_2xamd1_stII_q8_usb_X.StateSpace_CSTATE[(uint32_T)i];
    }
  }

  /* End of StateSpace: '<S11>/State-Space' */

  /* ManualSwitch: '<S11>/Control Mode' */
  if (q_2xamd1_stII_q8_usb_P.ControlMode_CurrentSetting == 1) {
    /* ManualSwitch: '<S11>/Control Mode' */
    q_2xamd1_stII_q8_usb_B.ControlMode[0] = rtb_StateFeedbackGainVector[0];
    q_2xamd1_stII_q8_usb_B.ControlMode[1] = rtb_StateFeedbackGainVector[1];
  } else {
    /* ManualSwitch: '<S11>/Control Mode' */
    q_2xamd1_stII_q8_usb_B.ControlMode[0] = rtb_StateSpace[0];
    q_2xamd1_stII_q8_usb_B.ControlMode[1] = rtb_StateSpace[1];
  }

  /* End of ManualSwitch: '<S11>/Control Mode' */
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* Gain: '<Root>/m to cm' */
    q_2xamd1_stII_q8_usb_B.mtocm[0] = q_2xamd1_stII_q8_usb_P.mtocm_Gain *
      q_2xamd1_stII_q8_usb_B.SetpointSwitch[0];
    q_2xamd1_stII_q8_usb_B.mtocm[1] = q_2xamd1_stII_q8_usb_P.mtocm_Gain *
      q_2xamd1_stII_q8_usb_B.EncoderCalibrationmcount;

    /* Constant: '<S52>/x0' */
    q_2xamd1_stII_q8_usb_B.x0_o = q_2xamd1_stII_q8_usb_P.x0_Value_h;
  }

  /* Integrator: '<S52>/Integrator1' */
  if (q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_p != 0) {
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_jt = q_2xamd1_stII_q8_usb_B.x0_o;
  }

  /* Gain: '<S49>/m//s^2 to g' incorporates:
   *  Integrator: '<S52>/Integrator1'
   */
  q_2xamd1_stII_q8_usb_B.ms2tog = q_2xamd1_stII_q8_usb_P.K_MS2G *
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_jt;
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
  }

  /* Gain: '<Root>/m to mm' */
  q_2xamd1_stII_q8_usb_B.mtomm = q_2xamd1_stII_q8_usb_P.mtomm_Gain *
    q_2xamd1_stII_q8_usb_B.Integrator[2];
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
  }

  /* Gain: '<Root>/m to mm1' */
  q_2xamd1_stII_q8_usb_B.mtomm1 = q_2xamd1_stII_q8_usb_P.mtomm1_Gain *
    q_2xamd1_stII_q8_usb_B.Integrator[3];
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* Outputs for Enabled SubSystem: '<S1>/Top Cart Position Watchdog' incorporates:
     *  EnablePort: '<S14>/Enable'
     */
    /* Outputs for Enabled SubSystem: '<S1>/Bottom Cart Position Watchdog' incorporates:
     *  EnablePort: '<S13>/Enable'
     */
    /* Constant: '<S1>/Constant1' */
    if (q_2xamd1_stII_q8_usb_P.XC_LIM_ENABLE > 0.0) {
      /* RelationalOperator: '<S13>/Check Maximum Allowed Position' incorporates:
       *  Constant: '<S13>/X MAX'
       */
      q_2xamd1_stII_q8_usb_B.CheckMaximumAllowedPosition_g =
        (q_2xamd1_stII_q8_usb_P.XC_MAX < q_2xamd1_stII_q8_usb_B.Conversiontom1);

      /* Stop: '<S22>/Stop Simulation' incorporates:
       *  Constant: '<S24>/Constant'
       *  RelationalOperator: '<S24>/Compare'
       */
      if (q_2xamd1_stII_q8_usb_B.CheckMaximumAllowedPosition_g !=
          q_2xamd1_stII_q8_usb_P.Constant_Value_h2) {
        rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
      }

      /* End of Stop: '<S22>/Stop Simulation' */

      /* RelationalOperator: '<S13>/Check Minimum Allowed Position' incorporates:
       *  Constant: '<S13>/X IN'
       */
      q_2xamd1_stII_q8_usb_B.CheckMinimumAllowedPosition_g =
        (q_2xamd1_stII_q8_usb_B.Conversiontom1 < q_2xamd1_stII_q8_usb_P.XC_MIN);

      /* Stop: '<S23>/Stop Simulation' incorporates:
       *  Constant: '<S25>/Constant'
       *  RelationalOperator: '<S25>/Compare'
       */
      if (q_2xamd1_stII_q8_usb_B.CheckMinimumAllowedPosition_g !=
          q_2xamd1_stII_q8_usb_P.Constant_Value_gt) {
        rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
      }

      /* End of Stop: '<S23>/Stop Simulation' */
      if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
        srUpdateBC(q_2xamd1_stII_q8_usb_DW.BottomCartPositionWatchdog_Subs);
      }

      /* RelationalOperator: '<S14>/Check Maximum Allowed Position' incorporates:
       *  Constant: '<S14>/X MAX'
       */
      q_2xamd1_stII_q8_usb_B.CheckMaximumAllowedPosition =
        (q_2xamd1_stII_q8_usb_P.XC_MAX < q_2xamd1_stII_q8_usb_B.Conversiontom2);

      /* Stop: '<S26>/Stop Simulation' incorporates:
       *  Constant: '<S28>/Constant'
       *  RelationalOperator: '<S28>/Compare'
       */
      if (q_2xamd1_stII_q8_usb_B.CheckMaximumAllowedPosition !=
          q_2xamd1_stII_q8_usb_P.Constant_Value_p) {
        rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
      }

      /* End of Stop: '<S26>/Stop Simulation' */

      /* RelationalOperator: '<S14>/Check Minimum Allowed Position' incorporates:
       *  Constant: '<S14>/X IN'
       */
      q_2xamd1_stII_q8_usb_B.CheckMinimumAllowedPosition =
        (q_2xamd1_stII_q8_usb_B.Conversiontom2 < q_2xamd1_stII_q8_usb_P.XC_MIN);

      /* Stop: '<S27>/Stop Simulation' incorporates:
       *  Constant: '<S29>/Constant'
       *  RelationalOperator: '<S29>/Compare'
       */
      if (q_2xamd1_stII_q8_usb_B.CheckMinimumAllowedPosition !=
          q_2xamd1_stII_q8_usb_P.Constant_Value_k) {
        rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
      }

      /* End of Stop: '<S27>/Stop Simulation' */
      if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
        srUpdateBC(q_2xamd1_stII_q8_usb_DW.TopCartPositionWatchdog_SubsysR);
      }
    }

    /* End of Constant: '<S1>/Constant1' */
    /* End of Outputs for SubSystem: '<S1>/Bottom Cart Position Watchdog' */
    /* End of Outputs for SubSystem: '<S1>/Top Cart Position Watchdog' */

    /* Outputs for Atomic SubSystem: '<S12>/Bias Removal' */
    /* Step: '<S15>/Step: start_time' incorporates:
     *  Step: '<S15>/Step: end_time'
     */
    u0 = q_2xamd1_stII_q8_usb_M->Timing.t[1];

    /* Outputs for Atomic SubSystem: '<S12>/Bias Removal' */
    if (u0 < q_2xamd1_stII_q8_usb_P.BiasRemoval_start_time) {
      /* S-Function (smooth_signal_generator_block): '<S10>/Smooth Signal Generator' incorporates:
       *  Step: '<S15>/Step: start_time'
       */
      rtb_SmoothSignalGenerator = q_2xamd1_stII_q8_usb_P.Stepstart_time_Y0;
    } else {
      /* S-Function (smooth_signal_generator_block): '<S10>/Smooth Signal Generator' incorporates:
       *  Step: '<S15>/Step: start_time'
       */
      rtb_SmoothSignalGenerator = q_2xamd1_stII_q8_usb_P.Stepstart_time_YFinal;
    }

    /* End of Step: '<S15>/Step: start_time' */

    /* Step: '<S15>/Step: end_time' */
    if (u0 < q_2xamd1_stII_q8_usb_P.BiasRemoval_end_time) {
      rtb_SliderGain_m = q_2xamd1_stII_q8_usb_P.Stepend_time_Y0;
    } else {
      rtb_SliderGain_m = q_2xamd1_stII_q8_usb_P.Stepend_time_YFinal;
    }

    /* End of Outputs for SubSystem: '<S12>/Bias Removal' */

    /* Outputs for Enabled SubSystem: '<S15>/Enabled Moving Average' incorporates:
     *  EnablePort: '<S17>/Enable'
     */
    /* Logic: '<S15>/Logical Operator' incorporates:
     *  Logic: '<S15>/Logical Operator1'
     */
    if ((rtb_SmoothSignalGenerator != 0.0) && (!(rtb_SliderGain_m != 0.0))) {
      if (!q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_MODE) {
        /* InitializeConditions for UnitDelay: '<S21>/Unit Delay' */
        q_2xamd1_stII_q8_usb_DW.UnitDelay_DSTATE =
          q_2xamd1_stII_q8_usb_P.UnitDelay_InitialCondition;

        /* InitializeConditions for UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )' */
        q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[0] =
          q_2xamd1_stII_q8_usb_P.Sumk1n1xk_InitialCondition;
        q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[1] =
          q_2xamd1_stII_q8_usb_P.Sumk1n1xk_InitialCondition;
        q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[2] =
          q_2xamd1_stII_q8_usb_P.Sumk1n1xk_InitialCondition;
        q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_MODE = true;
      }

      /* Sum: '<S21>/Count' incorporates:
       *  Constant: '<S21>/unity'
       *  UnitDelay: '<S21>/Unit Delay'
       */
      q_2xamd1_stII_q8_usb_B.Count = q_2xamd1_stII_q8_usb_P.unity_Value +
        q_2xamd1_stII_q8_usb_DW.UnitDelay_DSTATE;

      /* Sum: '<S17>/Sum' incorporates:
       *  UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )'
       */
      q_2xamd1_stII_q8_usb_B.Sum[0] = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1
        [0] + q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[0];

      /* Product: '<S17>/div' */
      q_2xamd1_stII_q8_usb_B.div[0] = q_2xamd1_stII_q8_usb_B.Sum[0] /
        q_2xamd1_stII_q8_usb_B.Count;

      /* Sum: '<S17>/Sum' incorporates:
       *  UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )'
       */
      q_2xamd1_stII_q8_usb_B.Sum[1] = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1
        [1] + q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[1];

      /* Product: '<S17>/div' */
      q_2xamd1_stII_q8_usb_B.div[1] = q_2xamd1_stII_q8_usb_B.Sum[1] /
        q_2xamd1_stII_q8_usb_B.Count;

      /* Sum: '<S17>/Sum' incorporates:
       *  UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )'
       */
      q_2xamd1_stII_q8_usb_B.Sum[2] = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1
        [2] + q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[2];

      /* Product: '<S17>/div' */
      q_2xamd1_stII_q8_usb_B.div[2] = q_2xamd1_stII_q8_usb_B.Sum[2] /
        q_2xamd1_stII_q8_usb_B.Count;
      srUpdateBC(q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_SubsysRanB);
    } else {
      q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_MODE = false;
    }

    /* End of Logic: '<S15>/Logical Operator' */
    /* End of Outputs for SubSystem: '<S15>/Enabled Moving Average' */

    /* Sum: '<S15>/Sum' */
    rtb_SliderGain = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[0] -
      q_2xamd1_stII_q8_usb_B.div[0];
    rtb_SliderGain_l = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[1] -
      q_2xamd1_stII_q8_usb_B.div[1];
    rtb_Sum_j_idx_2 = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[2] -
      q_2xamd1_stII_q8_usb_B.div[2];

    /* SwitchCase: '<S15>/Switch Case' incorporates:
     *  Constant: '<S18>/Constant'
     *  Inport: '<S19>/Vin'
     *  Inport: '<S20>/V-Vavg'
     */
    rtPrevAction = q_2xamd1_stII_q8_usb_DW.SwitchCase_ActiveSubsystem;
    rtAction = -1;

    /* Outputs for Atomic SubSystem: '<S12>/Bias Removal' */
    if (q_2xamd1_stII_q8_usb_P.BiasRemoval_switch_id < 0.0) {
      u0 = ceil(q_2xamd1_stII_q8_usb_P.BiasRemoval_switch_id);
    } else {
      u0 = floor(q_2xamd1_stII_q8_usb_P.BiasRemoval_switch_id);
    }

    /* End of Outputs for SubSystem: '<S12>/Bias Removal' */
    if (rtIsNaN(u0) || rtIsInf(u0)) {
      u0 = 0.0;
    } else {
      u0 = fmod(u0, 4.294967296E+9);
    }

    switch (u0 < 0.0 ? -(int32_T)(uint32_T)-u0 : (int32_T)(uint32_T)u0) {
     case 1:
      rtAction = 0;
      break;

     case 2:
      rtAction = 1;
      break;

     case 3:
      rtAction = 2;
      break;
    }

    q_2xamd1_stII_q8_usb_DW.SwitchCase_ActiveSubsystem = rtAction;
    if (rtPrevAction != rtAction) {
      switch (rtPrevAction) {
       case 0:
        /* Disable for Outport: '<S18>/zero' incorporates:
         *  Constant: '<S18>/Constant'
         * */
        q_2xamd1_stII_q8_usb_B.Constant = q_2xamd1_stII_q8_usb_P.zero_Y0;
        break;

       case 1:
        /* Disable for Outport: '<S19>/Vbiased' incorporates:
         *  Inport: '<S19>/Vin'
         * */
        q_2xamd1_stII_q8_usb_B.Vin[0] = q_2xamd1_stII_q8_usb_P.Vbiased_Y0;
        q_2xamd1_stII_q8_usb_B.Vin[1] = q_2xamd1_stII_q8_usb_P.Vbiased_Y0;
        q_2xamd1_stII_q8_usb_B.Vin[2] = q_2xamd1_stII_q8_usb_P.Vbiased_Y0;
        break;

       case 2:
        /* Disable for Outport: '<S20>/Vunbiased' incorporates:
         *  Inport: '<S20>/V-Vavg'
         * */
        q_2xamd1_stII_q8_usb_B.VVavg[0] = q_2xamd1_stII_q8_usb_P.Vunbiased_Y0;
        q_2xamd1_stII_q8_usb_B.VVavg[1] = q_2xamd1_stII_q8_usb_P.Vunbiased_Y0;
        q_2xamd1_stII_q8_usb_B.VVavg[2] = q_2xamd1_stII_q8_usb_P.Vunbiased_Y0;
        break;
      }
    }

    switch (rtAction) {
     case 0:
      /* Outputs for IfAction SubSystem: '<S15>/Switch Case Action Subsystem' incorporates:
       *  ActionPort: '<S18>/Action Port'
       */
      q_2xamd1_stII_q8_usb_B.Constant = q_2xamd1_stII_q8_usb_P.Constant_Value;
      srUpdateBC(q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem_Subsy);

      /* End of Outputs for SubSystem: '<S15>/Switch Case Action Subsystem' */
      break;

     case 1:
      /* Outputs for IfAction SubSystem: '<S15>/Switch Case Action Subsystem1' incorporates:
       *  ActionPort: '<S19>/Action Port'
       */
      q_2xamd1_stII_q8_usb_B.Vin[0] = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1
        [0];
      q_2xamd1_stII_q8_usb_B.Vin[1] = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1
        [1];
      q_2xamd1_stII_q8_usb_B.Vin[2] = q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1
        [2];
      srUpdateBC(q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem1_Subs);

      /* End of Outputs for SubSystem: '<S15>/Switch Case Action Subsystem1' */
      break;

     case 2:
      /* Outputs for IfAction SubSystem: '<S15>/Switch Case Action Subsystem2' incorporates:
       *  ActionPort: '<S20>/Action Port'
       */
      q_2xamd1_stII_q8_usb_B.VVavg[0] = rtb_SliderGain;
      q_2xamd1_stII_q8_usb_B.VVavg[1] = rtb_SliderGain_l;
      q_2xamd1_stII_q8_usb_B.VVavg[2] = rtb_Sum_j_idx_2;
      srUpdateBC(q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem2_Subs);

      /* End of Outputs for SubSystem: '<S15>/Switch Case Action Subsystem2' */
      break;
    }

    /* End of SwitchCase: '<S15>/Switch Case' */

    /* Switch: '<S15>/Switch' incorporates:
     *  Logic: '<S15>/Logical Operator2'
     */
    if (!(rtb_SliderGain_m != 0.0)) {
      /* Switch: '<S15>/Switch' incorporates:
       *  Sum: '<S15>/Sum1'
       */
      q_2xamd1_stII_q8_usb_B.Switch[0] = (q_2xamd1_stII_q8_usb_B.Constant +
        q_2xamd1_stII_q8_usb_B.Vin[0]) + q_2xamd1_stII_q8_usb_B.VVavg[0];
      q_2xamd1_stII_q8_usb_B.Switch[1] = (q_2xamd1_stII_q8_usb_B.Constant +
        q_2xamd1_stII_q8_usb_B.Vin[1]) + q_2xamd1_stII_q8_usb_B.VVavg[1];
      q_2xamd1_stII_q8_usb_B.Switch[2] = (q_2xamd1_stII_q8_usb_B.Constant +
        q_2xamd1_stII_q8_usb_B.Vin[2]) + q_2xamd1_stII_q8_usb_B.VVavg[2];
    } else {
      /* Switch: '<S15>/Switch' */
      q_2xamd1_stII_q8_usb_B.Switch[0] = rtb_SliderGain;
      q_2xamd1_stII_q8_usb_B.Switch[1] = rtb_SliderGain_l;
      q_2xamd1_stII_q8_usb_B.Switch[2] = rtb_Sum_j_idx_2;
    }

    /* End of Switch: '<S15>/Switch' */
    /* End of Outputs for SubSystem: '<S12>/Bias Removal' */

    /* Gain: '<S2>/Slider Gain' incorporates:
     *  Constant: '<Root>/Constant'
     */
    rtb_SliderGain_l = q_2xamd1_stII_q8_usb_P.Amplitudecm_gain *
      q_2xamd1_stII_q8_usb_P.Constant_Value_a;

    /* Gain: '<S4>/Slider Gain' incorporates:
     *  Constant: '<Root>/Constant'
     */
    rtb_SliderGain_m = q_2xamd1_stII_q8_usb_P.FrequencyHz_gain *
      q_2xamd1_stII_q8_usb_P.Constant_Value_a;
  }

  /* Product: '<S16>/Product' incorporates:
   *  Constant: '<S16>/Constant'
   *  Constant: '<S16>/wn'
   *  Constant: '<S16>/zt'
   *  Integrator: '<S16>/Integrator1'
   *  Integrator: '<S16>/Integrator2'
   *  Product: '<S16>/Product2'
   *  Sum: '<S16>/Sum'
   *  Sum: '<S16>/Sum1'
   */
  q_2xamd1_stII_q8_usb_B.Product[0] = ((q_2xamd1_stII_q8_usb_B.Switch[0] -
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[0]) -
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[0] *
    q_2xamd1_stII_q8_usb_P.Constant_Value_b * q_2xamd1_stII_q8_usb_P.za) *
    q_2xamd1_stII_q8_usb_P.wa;

  /* Product: '<S16>/Product1' incorporates:
   *  Constant: '<S16>/wn'
   *  Integrator: '<S16>/Integrator2'
   */
  q_2xamd1_stII_q8_usb_B.Product1[0] = q_2xamd1_stII_q8_usb_P.wa *
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[0];

  /* Product: '<S16>/Product' incorporates:
   *  Constant: '<S16>/Constant'
   *  Constant: '<S16>/wn'
   *  Constant: '<S16>/zt'
   *  Integrator: '<S16>/Integrator1'
   *  Integrator: '<S16>/Integrator2'
   *  Product: '<S16>/Product2'
   *  Sum: '<S16>/Sum'
   *  Sum: '<S16>/Sum1'
   */
  q_2xamd1_stII_q8_usb_B.Product[1] = ((q_2xamd1_stII_q8_usb_B.Switch[1] -
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[1]) -
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[1] *
    q_2xamd1_stII_q8_usb_P.Constant_Value_b * q_2xamd1_stII_q8_usb_P.za) *
    q_2xamd1_stII_q8_usb_P.wa;

  /* Product: '<S16>/Product1' incorporates:
   *  Constant: '<S16>/wn'
   *  Integrator: '<S16>/Integrator2'
   */
  q_2xamd1_stII_q8_usb_B.Product1[1] = q_2xamd1_stII_q8_usb_P.wa *
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[1];

  /* Product: '<S16>/Product' incorporates:
   *  Constant: '<S16>/Constant'
   *  Constant: '<S16>/wn'
   *  Constant: '<S16>/zt'
   *  Integrator: '<S16>/Integrator1'
   *  Integrator: '<S16>/Integrator2'
   *  Product: '<S16>/Product2'
   *  Sum: '<S16>/Sum'
   *  Sum: '<S16>/Sum1'
   */
  q_2xamd1_stII_q8_usb_B.Product[2] = ((q_2xamd1_stII_q8_usb_B.Switch[2] -
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[2]) -
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[2] *
    q_2xamd1_stII_q8_usb_P.Constant_Value_b * q_2xamd1_stII_q8_usb_P.za) *
    q_2xamd1_stII_q8_usb_P.wa;

  /* Product: '<S16>/Product1' incorporates:
   *  Constant: '<S16>/wn'
   *  Integrator: '<S16>/Integrator2'
   */
  q_2xamd1_stII_q8_usb_B.Product1[2] = q_2xamd1_stII_q8_usb_P.wa *
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[2];

  /* Gain: '<S5>/Kp_c: 274.1' incorporates:
   *  Constant: '<S5>/Constant'
   */
  q_2xamd1_stII_q8_usb_B.Kp_c2741[0] = q_2xamd1_stII_q8_usb_P.Kp_c2741_Gain *
    q_2xamd1_stII_q8_usb_P.Constant_Value_g3;
  q_2xamd1_stII_q8_usb_B.Kp_c2741[1] = q_2xamd1_stII_q8_usb_P.Kp_c2741_Gain *
    q_2xamd1_stII_q8_usb_B.Integrator[0];
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
  }

  /* Gain: '<S6>/Kp_c: 274.1' incorporates:
   *  Constant: '<S6>/Constant'
   */
  q_2xamd1_stII_q8_usb_B.Kp_c2741_a[0] = q_2xamd1_stII_q8_usb_P.Kp_c2741_Gain_m *
    q_2xamd1_stII_q8_usb_P.Constant_Value_e;
  q_2xamd1_stII_q8_usb_B.Kp_c2741_a[1] = q_2xamd1_stII_q8_usb_P.Kp_c2741_Gain_m *
    q_2xamd1_stII_q8_usb_B.Integrator[1];
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
  }

  /* Abs: '<S50>/Abs1' incorporates:
   *  Integrator: '<S51>/Integrator1'
   */
  q_2xamd1_stII_q8_usb_B.Abs1 = fabs(q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE);
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* S-Function (smooth_signal_generator_block): '<S10>/Smooth Signal Generator' incorporates:
     *  Abs: '<S54>/Abs1'
     */
    rtb_SmoothSignalGenerator = fabs(q_2xamd1_stII_q8_usb_B.Abs1);

    /* RelationalOperator: '<S54>/Relational Operator' incorporates:
     *  Constant: '<S54>/motion threshold'
     */
    q_2xamd1_stII_q8_usb_B.RelationalOperator = (rtb_SmoothSignalGenerator <=
      q_2xamd1_stII_q8_usb_P.StallMonitor_motion_threshold);
  }

  /* Logic: '<S54>/Logical Operator' incorporates:
   *  Abs: '<S50>/Abs'
   *  Constant: '<S54>/control threshold'
   *  RelationalOperator: '<S54>/Relational Operator1'
   */
  rtb_Compare_lt = ((fabs(q_2xamd1_stII_q8_usb_B.Add) >=
                     q_2xamd1_stII_q8_usb_P.StallMonitor_control_threshold) &&
                    q_2xamd1_stII_q8_usb_B.RelationalOperator);

  /* Logic: '<S54>/Logical Operator1' */
  q_2xamd1_stII_q8_usb_B.LogicalOperator1 = !rtb_Compare_lt;
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* DiscreteIntegrator: '<S54>/Discrete-Time Integrator' */
    if (q_2xamd1_stII_q8_usb_B.LogicalOperator1 &&
        (q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_PrevRese <= 0)) {
      q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_DSTATE =
        q_2xamd1_stII_q8_usb_P.DiscreteTimeIntegrator_IC;
    }

    /* DiscreteIntegrator: '<S54>/Discrete-Time Integrator' */
    q_2xamd1_stII_q8_usb_B.DiscreteTimeIntegrator =
      q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_DSTATE;

    /* RelationalOperator: '<S54>/Relational Operator2' incorporates:
     *  Constant: '<S54>/control threshold1'
     */
    q_2xamd1_stII_q8_usb_B.RelationalOperator2 =
      (q_2xamd1_stII_q8_usb_B.DiscreteTimeIntegrator >=
       q_2xamd1_stII_q8_usb_P.StallMonitor_duration_s);

    /* Stop: '<S53>/Stop Simulation' incorporates:
     *  Constant: '<S55>/Constant'
     *  RelationalOperator: '<S55>/Compare'
     */
    if (q_2xamd1_stII_q8_usb_B.RelationalOperator2 !=
        q_2xamd1_stII_q8_usb_P.Constant_Value_di) {
      rtmSetStopRequested(q_2xamd1_stII_q8_usb_M, 1);
    }

    /* End of Stop: '<S53>/Stop Simulation' */

    /* Gain: '<S49>/counts//s to m//s' */
    q_2xamd1_stII_q8_usb_B.countsstoms = q_2xamd1_stII_q8_usb_P.K_ENC *
      rtb_HILReadTimebase_o4;
  }

  /* DataTypeConversion: '<S54>/Data Type Conversion' */
  q_2xamd1_stII_q8_usb_B.DataTypeConversion = rtb_Compare_lt;

  /* Derivative: '<S49>/Derivative' */
  if ((q_2xamd1_stII_q8_usb_DW.TimeStampA >= rtb_Integrator1_k_tmp) &&
      (q_2xamd1_stII_q8_usb_DW.TimeStampB >= rtb_Integrator1_k_tmp)) {
    rtb_SliderGain = 0.0;
  } else {
    rtb_SliderGain = q_2xamd1_stII_q8_usb_DW.TimeStampA;
    lastU = &q_2xamd1_stII_q8_usb_DW.LastUAtTimeA;
    if (q_2xamd1_stII_q8_usb_DW.TimeStampA < q_2xamd1_stII_q8_usb_DW.TimeStampB)
    {
      if (q_2xamd1_stII_q8_usb_DW.TimeStampB < rtb_Integrator1_k_tmp) {
        rtb_SliderGain = q_2xamd1_stII_q8_usb_DW.TimeStampB;
        lastU = &q_2xamd1_stII_q8_usb_DW.LastUAtTimeB;
      }
    } else if (q_2xamd1_stII_q8_usb_DW.TimeStampA >= rtb_Integrator1_k_tmp) {
      rtb_SliderGain = q_2xamd1_stII_q8_usb_DW.TimeStampB;
      lastU = &q_2xamd1_stII_q8_usb_DW.LastUAtTimeB;
    }

    rtb_SliderGain = (q_2xamd1_stII_q8_usb_B.countsstoms - *lastU) /
      (rtb_Integrator1_k_tmp - rtb_SliderGain);
  }

  /* Product: '<S51>/Product' incorporates:
   *  Constant: '<S51>/Constant'
   *  Constant: '<S51>/wn'
   *  Constant: '<S51>/zt'
   *  Integrator: '<S51>/Integrator1'
   *  Integrator: '<S51>/Integrator2'
   *  Product: '<S51>/Product2'
   *  Sum: '<S51>/Sum'
   *  Sum: '<S51>/Sum1'
   */
  q_2xamd1_stII_q8_usb_B.Product_m = ((q_2xamd1_stII_q8_usb_B.countsstoms -
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE) -
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE_i *
    q_2xamd1_stII_q8_usb_P.Constant_Value_d * q_2xamd1_stII_q8_usb_P.zd) *
    q_2xamd1_stII_q8_usb_P.wd;

  /* Product: '<S51>/Product1' incorporates:
   *  Constant: '<S51>/wn'
   *  Integrator: '<S51>/Integrator2'
   */
  q_2xamd1_stII_q8_usb_B.Product1_g = q_2xamd1_stII_q8_usb_P.wd *
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE_i;

  /* Product: '<S52>/Product' incorporates:
   *  Constant: '<S52>/Constant'
   *  Constant: '<S52>/wn'
   *  Constant: '<S52>/zt'
   *  Integrator: '<S52>/Integrator1'
   *  Integrator: '<S52>/Integrator2'
   *  Product: '<S52>/Product2'
   *  Sum: '<S52>/Sum'
   *  Sum: '<S52>/Sum1'
   */
  q_2xamd1_stII_q8_usb_B.Product_k = ((rtb_SliderGain -
    q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_jt) -
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE_m *
    q_2xamd1_stII_q8_usb_P.Constant_Value_h * q_2xamd1_stII_q8_usb_P.za) *
    q_2xamd1_stII_q8_usb_P.wa;

  /* Product: '<S52>/Product1' incorporates:
   *  Constant: '<S52>/wn'
   *  Integrator: '<S52>/Integrator2'
   */
  q_2xamd1_stII_q8_usb_B.Product1_f = q_2xamd1_stII_q8_usb_P.wa *
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE_m;

  /* DataTypeConversion: '<S57>/Data Type Conversion' incorporates:
   *  Constant: '<S57>/UPM Init Time (s)'
   *  RelationalOperator: '<S57>/Relational Operator'
   */
  q_2xamd1_stII_q8_usb_B.DataTypeConversion_b = (rtb_Integrator1_k_tmp >=
    q_2xamd1_stII_q8_usb_P.UPMInitTimes_Value);
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* DiscreteTransferFcn: '<S59>/Discrete Transfer Fcn' */
    q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_tmp =
      (q_2xamd1_stII_q8_usb_B.DataTypeConversion_b -
       q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_DenCoef[1] *
       q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_states) /
      q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_DenCoef[0];

    /* DiscreteTransferFcn: '<S59>/Discrete Transfer Fcn' */
    q_2xamd1_stII_q8_usb_B.DiscreteTransferFcn =
      q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_NumCoef[0] *
      q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_tmp +
      q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_NumCoef[1] *
      q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_states;
  }

  /* Math: '<S56>/Math Function' incorporates:
   *  Constant: '<S56>/Constant4'
   */
  rtb_SliderGain = rt_modd_snf(rtb_Integrator1_k_tmp,
    q_2xamd1_stII_q8_usb_P.RepeatingChirp_T
    + q_2xamd1_stII_q8_usb_P.RepeatingChirp_D);

  /* DeadZone: '<S56>/Dead Zone' */
  if (rtb_SliderGain > q_2xamd1_stII_q8_usb_P.RepeatingChirp_D) {
    rtb_SliderGain -= q_2xamd1_stII_q8_usb_P.RepeatingChirp_D;
  } else if (rtb_SliderGain >= q_2xamd1_stII_q8_usb_P.DeadZone_Start) {
    rtb_SliderGain = 0.0;
  } else {
    rtb_SliderGain -= q_2xamd1_stII_q8_usb_P.DeadZone_Start;
  }

  /* End of DeadZone: '<S56>/Dead Zone' */

  /* Product: '<S9>/Product' incorporates:
   *  Constant: '<S56>/Constant2'
   *  Constant: '<S56>/Constant3'
   *  Gain: '<S56>/Gain'
   *  Gain: '<S56>/Gain1'
   *  Gain: '<S9>/Sine Sweep  Amplitude (m)'
   *  Product: '<S56>/Product1'
   *  Sin: '<S56>/Sine Wave Function'
   *  Sum: '<S56>/Sum1'
   */
  u0 = (sin(((q_2xamd1_stII_q8_usb_P.RepeatingChirp_f2 -
              q_2xamd1_stII_q8_usb_P.RepeatingChirp_f1) /
             q_2xamd1_stII_q8_usb_P.RepeatingChirp_T * rtb_SliderGain + 2.0 *
             q_2xamd1_stII_q8_usb_P.RepeatingChirp_f1) *
            (q_2xamd1_stII_q8_usb_P.Constant3_Value * rtb_SliderGain) *
            q_2xamd1_stII_q8_usb_P.SineWaveFunction_Freq +
            q_2xamd1_stII_q8_usb_P.SineWaveFunction_Phase) *
        q_2xamd1_stII_q8_usb_P.SineWaveFunction_Amp +
        q_2xamd1_stII_q8_usb_P.SineWaveFunction_Bias) *
    q_2xamd1_stII_q8_usb_P.RepeatingChirp_A *
    q_2xamd1_stII_q8_usb_P.SineSweepAmplitudem_Gain *
    q_2xamd1_stII_q8_usb_B.DiscreteTransferFcn;

  /* Saturate: '<S9>/Sweep Position  Limit (m)' */
  if (u0 > q_2xamd1_stII_q8_usb_P.SWEEP_MAX) {
    /* Saturate: '<S9>/Sweep Position  Limit (m)' */
    q_2xamd1_stII_q8_usb_B.SweepPositionLimitm =
      q_2xamd1_stII_q8_usb_P.SWEEP_MAX;
  } else if (u0 < -q_2xamd1_stII_q8_usb_P.SWEEP_MAX) {
    /* Saturate: '<S9>/Sweep Position  Limit (m)' */
    q_2xamd1_stII_q8_usb_B.SweepPositionLimitm =
      -q_2xamd1_stII_q8_usb_P.SWEEP_MAX;
  } else {
    /* Saturate: '<S9>/Sweep Position  Limit (m)' */
    q_2xamd1_stII_q8_usb_B.SweepPositionLimitm = u0;
  }

  /* End of Saturate: '<S9>/Sweep Position  Limit (m)' */

  /* RelationalOperator: '<S61>/Compare' incorporates:
   *  Constant: '<S61>/Constant'
   */
  q_2xamd1_stII_q8_usb_B.Compare = (rtb_Integrator1_k_tmp >=
    q_2xamd1_stII_q8_usb_P.Startat1sec_const);
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* SignalConversion generated from: '<S60>/Enable' */
    q_2xamd1_stII_q8_usb_B.HiddenBuf_InsertedFor_RampSigna =
      q_2xamd1_stII_q8_usb_B.Compare;

    /* S-Function (smooth_signal_generator_block): '<S10>/Smooth Signal Generator' */
    /* S-Function Block: q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Smooth Signal Generator (smooth_signal_generator_block) */
    {
      if (rtb_SliderGain_l < 0.0) {
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M,
                          "The input amplitude to the q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Smooth Signal Generator block is negative. Only non-negative amplitudes are supported.");
      } else if (rtb_SliderGain_m <= 0.0) {
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M,
                          "Invalid input frequency to the q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Smooth Signal Generator block. The frequency is negative or zero.");
      } else {
        real_T w = TWO_PI * rtb_SliderGain_m;
        boolean_T params_changed = (rtb_SliderGain_l !=
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp || w !=
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.W);

        /*
           Discrete-time:

           y_sin = sin(w*(t + dt)) = sin(w*t)*cos(w*dt) + cos(w*t)*sin(w*dt)
           y_cos = cos(w*(t + dt)) = cos(w*t)*cos(w*dt) - sin(w*t)*sin(w*dt)

           Since dt is fixed and known (the sampling period), we can compute the
           output at the next time step, t + dt, from the previous time step, t,
           simply by applying the formulae above where sin(w*dt) and cos(w*dt) are constant.

           Now, suppose the amplitude or frequency is changed. Then when the current
           value of the output is less than the new amplitude, we need to solve the
           equation:
           1. A1 * y_sin = A2 * sin(x)
           whence:
           x  = asin(A1*y_sin/A2)
           We can then compute a new value of sin(x) and cos(x).
         */
        real_T y_sin =
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t;
        real_T y_cos =
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t;

        /* Produce the output(s) */
        rtb_SmoothSignalGenerator =
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp * y_sin;

        /* Compute the new sin and cos values */
        q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t = y_sin *
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_dt + y_cos *
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_dt;
        q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t = y_cos *
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_dt - y_sin *
          q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_dt;

        /*
           If the amplitude or frequency parameter changes, then adjust the
           sine wave parameters such that the sine wave output is continuous.
         */
        if (params_changed) {
          real_T x;

          /*
             Only adjust the amplitude once the current amplitude crosses zero. Otherwise
             we get strange behaviour when continuously modifying the input amplitude and
             frequency using input signals.
           */
          if (y_sin == 0 ||
              q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t == 0 ||
              (y_sin < 0) !=
              (q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t < 0))
          {
            if (fabs(q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp *
                     q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t)
                < rtb_SliderGain_l) {
              x = asin(q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp *
                       q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t
                       / rtb_SliderGain_l);
              q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_dt = sin
                (w * 0.0005);
              q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_dt = cos
                (w * 0.0005);
              q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t = sin
                (x);

              /*
                 There are always two solutions for the phase. Choose the solution
                 that matches the slope as well as the amplitude.
               */
              if ((cos(x) < 0) !=
                  (q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t <
                   0)) {
                q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t =
                  -cos(x);
              } else {
                q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t =
                  cos(x);
              }
            } else {
              /* really low amplitudes where sawtooth wave goes further in one time step after crossing zero than the new amplitude */
              x = w * 0.0005;
              q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t = 0;

              /* Make sure cosine output is also continuous */
              if (q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t <
                  0) {
                q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t = -1;
              } else {
                q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t = +1;
              }

              q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_dt = sin
                (x);
              q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_dt = cos
                (x);
            }

            q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp =
              rtb_SliderGain_l;
            q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.W = w;
          }
        }
      }
    }

    /* Gain: '<S10>/cm to m' */
    q_2xamd1_stII_q8_usb_B.cmtom = q_2xamd1_stII_q8_usb_P.cmtom_Gain *
      rtb_SmoothSignalGenerator;

    /* Outputs for Enabled SubSystem: '<S10>/Ramp Signal' incorporates:
     *  EnablePort: '<S60>/Enable'
     */
    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      q_2xamd1_stII_q8_usb_DW.RampSignal_MODE =
        q_2xamd1_stII_q8_usb_B.HiddenBuf_InsertedFor_RampSigna;
    }

    /* End of Outputs for SubSystem: '<S10>/Ramp Signal' */
  }

  /* Outputs for Enabled SubSystem: '<S10>/Ramp Signal' incorporates:
   *  EnablePort: '<S60>/Enable'
   */
  if (q_2xamd1_stII_q8_usb_DW.RampSignal_MODE) {
    /* Gain: '<S60>/Gain' incorporates:
     *  Constant: '<S60>/Constant'
     *  Integrator: '<S60>/Integrator'
     *  Sum: '<S60>/Sum'
     */
    q_2xamd1_stII_q8_usb_B.Gain = (q_2xamd1_stII_q8_usb_P.Constant_Value_g -
      q_2xamd1_stII_q8_usb_X.Integrator_CSTATE_p) *
      q_2xamd1_stII_q8_usb_P.Gain_Gain;

    /* Product: '<S60>/Product' incorporates:
     *  Integrator: '<S60>/Integrator'
     */
    q_2xamd1_stII_q8_usb_B.Product_e = q_2xamd1_stII_q8_usb_B.cmtom *
      q_2xamd1_stII_q8_usb_X.Integrator_CSTATE_p;
    if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
      srUpdateBC(q_2xamd1_stII_q8_usb_DW.RampSignal_SubsysRanBC);
    }
  }

  /* End of Outputs for SubSystem: '<S10>/Ramp Signal' */

  /* Saturate: '<S10>/Position Limit' */
  if (q_2xamd1_stII_q8_usb_B.Product_e > q_2xamd1_stII_q8_usb_P.P_MAX) {
    /* Saturate: '<S10>/Position Limit' */
    q_2xamd1_stII_q8_usb_B.PositionLimit = q_2xamd1_stII_q8_usb_P.P_MAX;
  } else if (q_2xamd1_stII_q8_usb_B.Product_e < -q_2xamd1_stII_q8_usb_P.P_MAX) {
    /* Saturate: '<S10>/Position Limit' */
    q_2xamd1_stII_q8_usb_B.PositionLimit = -q_2xamd1_stII_q8_usb_P.P_MAX;
  } else {
    /* Saturate: '<S10>/Position Limit' */
    q_2xamd1_stII_q8_usb_B.PositionLimit = q_2xamd1_stII_q8_usb_B.Product_e;
  }

  /* End of Saturate: '<S10>/Position Limit' */

  /* SignalConversion generated from: '<S11>/State-Space' */
  q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[0] =
    q_2xamd1_stII_q8_usb_B.Conversiontom1;
  q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[1] =
    q_2xamd1_stII_q8_usb_B.Conversiontom2;
  q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[2] =
    q_2xamd1_stII_q8_usb_B.floor1gtoms2;
  q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[3] =
    q_2xamd1_stII_q8_usb_B.floor1gtoms1;

  /* Gain: '<S63>/Matrix Gain1' */
  for (i = 0; i < 8; i++) {
    tmp_0[i] = 0.0;
  }

  for (i_0 = 0; i_0 < 8; i_0++) {
    for (i = 0; i < 8; i++) {
      tmp_0[i] += q_2xamd1_stII_q8_usb_P.A[(i_0 << 3) + i] *
        q_2xamd1_stII_q8_usb_B.Integrator[i_0];
    }
  }

  /* End of Gain: '<S63>/Matrix Gain1' */

  /* Gain: '<S63>/Matrix Gain' */
  for (i = 0; i < 8; i++) {
    tmp_1[i] = q_2xamd1_stII_q8_usb_P.B[i + 8] *
      q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[1] +
      q_2xamd1_stII_q8_usb_P.B[i] *
      q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[0];
  }

  /* End of Gain: '<S63>/Matrix Gain' */
  for (i = 0; i < 4; i++) {
    /* Gain: '<S63>/Matrix Gain2' */
    tmp_2[i] = 0.0;
    for (i_0 = 0; i_0 < 8; i_0++) {
      tmp_2[i] += q_2xamd1_stII_q8_usb_P.C[(i_0 << 2) + i] *
        q_2xamd1_stII_q8_usb_B.Integrator[i_0];
    }

    /* End of Gain: '<S63>/Matrix Gain2' */

    /* Sum: '<S63>/Sum1' incorporates:
     *  Gain: '<S63>/Matrix Gain3'
     *  Sum: '<S63>/Sum2'
     */
    tmp_3[i] = q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[i] -
      ((q_2xamd1_stII_q8_usb_P.D[i + 4] *
        q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[1] +
        q_2xamd1_stII_q8_usb_P.D[i] *
        q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[0]) + tmp_2[i]);
  }

  for (i = 0; i < 8; i++) {
    /* Sum: '<S63>/Sum3' incorporates:
     *  Gain: '<S63>/Matrix Gain4'
     */
    q_2xamd1_stII_q8_usb_B.Sum3[i] = (((q_2xamd1_stII_q8_usb_P.G[i + 8] * tmp_3
      [1] + q_2xamd1_stII_q8_usb_P.G[i] * tmp_3[0]) + q_2xamd1_stII_q8_usb_P.G[i
      + 16] * tmp_3[2]) + q_2xamd1_stII_q8_usb_P.G[i + 24] * tmp_3[3]) +
      (tmp_0[i] + tmp_1[i]);
  }
}

/* Model update function */
void q_2xamd1_stII_q8_usb_update(void)
{
  real_T *lastU;

  /* Update for S-Function (continuous_sigmoid_block): '<S10>/Continuous Sigmoid' */
  if (1) {
    {
      if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Target !=
          q_2xamd1_stII_q8_usb_B.PositionLimit) {
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Target =
          q_2xamd1_stII_q8_usb_B.PositionLimit;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag = 1;
      }

      if (q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax !=
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MVel) {
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MVel =
          q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag = 1;
      }

      if (q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax !=
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MAcc) {
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MAcc =
          q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag = 1;
      }
    }
  }

  /* Update for S-Function (continuous_sigmoid_block): '<S9>/Continuous Sigmoid' */
  if (1) {
    {
      if (q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Target !=
          q_2xamd1_stII_q8_usb_B.SweepPositionLimitm) {
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Target =
          q_2xamd1_stII_q8_usb_B.SweepPositionLimitm;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag_b = 1;
      }

      if (q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax_j !=
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MVel) {
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MVel =
          q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax_j;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag_b = 1;
      }

      if (q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax_a !=
          q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MAcc) {
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MAcc =
          q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax_a;
        q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag_b = 1;
      }
    }
  }

  /* Update for Integrator: '<S51>/Integrator1' */
  q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK = 0;

  /* Update for Integrator: '<S16>/Integrator1' */
  q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_i = 0;

  /* Update for Integrator: '<S52>/Integrator1' */
  q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_p = 0;
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* Update for Atomic SubSystem: '<S12>/Bias Removal' */
    /* Update for Enabled SubSystem: '<S15>/Enabled Moving Average' incorporates:
     *  EnablePort: '<S17>/Enable'
     */
    if (q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_MODE) {
      /* Update for UnitDelay: '<S21>/Unit Delay' */
      q_2xamd1_stII_q8_usb_DW.UnitDelay_DSTATE = q_2xamd1_stII_q8_usb_B.Count;

      /* Update for UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )' */
      q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[0] = q_2xamd1_stII_q8_usb_B.Sum[0];
      q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[1] = q_2xamd1_stII_q8_usb_B.Sum[1];
      q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[2] = q_2xamd1_stII_q8_usb_B.Sum[2];
    }

    /* End of Update for SubSystem: '<S15>/Enabled Moving Average' */
    /* End of Update for SubSystem: '<S12>/Bias Removal' */

    /* Update for DiscreteIntegrator: '<S54>/Discrete-Time Integrator' */
    q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_DSTATE +=
      q_2xamd1_stII_q8_usb_P.DiscreteTimeIntegrator_gainval *
      q_2xamd1_stII_q8_usb_B.DataTypeConversion;
    q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_PrevRese = (int8_T)
      q_2xamd1_stII_q8_usb_B.LogicalOperator1;
  }

  /* Update for Derivative: '<S49>/Derivative' */
  if (q_2xamd1_stII_q8_usb_DW.TimeStampA == (rtInf)) {
    q_2xamd1_stII_q8_usb_DW.TimeStampA = q_2xamd1_stII_q8_usb_M->Timing.t[0];
    lastU = &q_2xamd1_stII_q8_usb_DW.LastUAtTimeA;
  } else if (q_2xamd1_stII_q8_usb_DW.TimeStampB == (rtInf)) {
    q_2xamd1_stII_q8_usb_DW.TimeStampB = q_2xamd1_stII_q8_usb_M->Timing.t[0];
    lastU = &q_2xamd1_stII_q8_usb_DW.LastUAtTimeB;
  } else if (q_2xamd1_stII_q8_usb_DW.TimeStampA <
             q_2xamd1_stII_q8_usb_DW.TimeStampB) {
    q_2xamd1_stII_q8_usb_DW.TimeStampA = q_2xamd1_stII_q8_usb_M->Timing.t[0];
    lastU = &q_2xamd1_stII_q8_usb_DW.LastUAtTimeA;
  } else {
    q_2xamd1_stII_q8_usb_DW.TimeStampB = q_2xamd1_stII_q8_usb_M->Timing.t[0];
    lastU = &q_2xamd1_stII_q8_usb_DW.LastUAtTimeB;
  }

  *lastU = q_2xamd1_stII_q8_usb_B.countsstoms;

  /* End of Update for Derivative: '<S49>/Derivative' */
  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    /* Update for DiscreteTransferFcn: '<S59>/Discrete Transfer Fcn' */
    q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_states =
      q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_tmp;
  }

  if (rtmIsMajorTimeStep(q_2xamd1_stII_q8_usb_M)) {
    rt_ertODEUpdateContinuousStates(&q_2xamd1_stII_q8_usb_M->solverInfo);
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++q_2xamd1_stII_q8_usb_M->Timing.clockTick0)) {
    ++q_2xamd1_stII_q8_usb_M->Timing.clockTickH0;
  }

  q_2xamd1_stII_q8_usb_M->Timing.t[0] = rtsiGetSolverStopTime
    (&q_2xamd1_stII_q8_usb_M->solverInfo);

  {
    /* Update absolute timer for sample time: [0.0005s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++q_2xamd1_stII_q8_usb_M->Timing.clockTick1)) {
      ++q_2xamd1_stII_q8_usb_M->Timing.clockTickH1;
    }

    q_2xamd1_stII_q8_usb_M->Timing.t[1] =
      q_2xamd1_stII_q8_usb_M->Timing.clockTick1 *
      q_2xamd1_stII_q8_usb_M->Timing.stepSize1 +
      q_2xamd1_stII_q8_usb_M->Timing.clockTickH1 *
      q_2xamd1_stII_q8_usb_M->Timing.stepSize1 * 4294967296.0;
  }
}

/* Derivatives for root system: '<Root>' */
void q_2xamd1_stII_q8_usb_derivatives(void)
{
  XDot_q_2xamd1_stII_q8_usb_T *_rtXdot;
  int32_T i;
  uint32_T ri;
  _rtXdot = ((XDot_q_2xamd1_stII_q8_usb_T *) q_2xamd1_stII_q8_usb_M->derivs);

  /* Derivatives for Integrator: '<S51>/Integrator1' */
  _rtXdot->Integrator1_CSTATE = q_2xamd1_stII_q8_usb_B.Product1_g;

  /* Derivatives for Integrator: '<S63>/Integrator' */
  memcpy(&_rtXdot->Integrator_CSTATE[0], &q_2xamd1_stII_q8_usb_B.Sum3[0], sizeof
         (real_T) << 3U);

  /* Derivatives for Integrator: '<S16>/Integrator1' */
  _rtXdot->Integrator1_CSTATE_j[0] = q_2xamd1_stII_q8_usb_B.Product1[0];
  _rtXdot->Integrator1_CSTATE_j[1] = q_2xamd1_stII_q8_usb_B.Product1[1];
  _rtXdot->Integrator1_CSTATE_j[2] = q_2xamd1_stII_q8_usb_B.Product1[2];

  /* Derivatives for StateSpace: '<S11>/State-Space' */
  for (i = 0; i < 16; i++) {
    _rtXdot->StateSpace_CSTATE[i] = 0.0;
  }

  for (i = 0; i < 16; i++) {
    for (ri = q_2xamd1_stII_q8_usb_P.StateSpace_A_jc[(uint32_T)i]; ri <
         q_2xamd1_stII_q8_usb_P.StateSpace_A_jc[i + 1U]; ri++) {
      _rtXdot->StateSpace_CSTATE[q_2xamd1_stII_q8_usb_P.StateSpace_A_ir[ri]] +=
        q_2xamd1_stII_q8_usb_P.StateSpace_A_pr[ri] *
        q_2xamd1_stII_q8_usb_X.StateSpace_CSTATE[(uint32_T)i];
    }
  }

  for (ri = q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[0U]; ri <
       q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[1U]; ri++) {
    _rtXdot->StateSpace_CSTATE[q_2xamd1_stII_q8_usb_P.StateSpace_B_ir[ri]] +=
      q_2xamd1_stII_q8_usb_P.StateSpace_B_pr[ri] *
      q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[0U];
  }

  for (ri = q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[1U]; ri <
       q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[2U]; ri++) {
    _rtXdot->StateSpace_CSTATE[q_2xamd1_stII_q8_usb_P.StateSpace_B_ir[ri]] +=
      q_2xamd1_stII_q8_usb_P.StateSpace_B_pr[ri] *
      q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[1U];
  }

  for (ri = q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[2U]; ri <
       q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[3U]; ri++) {
    _rtXdot->StateSpace_CSTATE[q_2xamd1_stII_q8_usb_P.StateSpace_B_ir[ri]] +=
      q_2xamd1_stII_q8_usb_P.StateSpace_B_pr[ri] *
      q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[2U];
  }

  for (ri = q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[3U]; ri <
       q_2xamd1_stII_q8_usb_P.StateSpace_B_jc[4U]; ri++) {
    _rtXdot->StateSpace_CSTATE[q_2xamd1_stII_q8_usb_P.StateSpace_B_ir[ri]] +=
      q_2xamd1_stII_q8_usb_P.StateSpace_B_pr[ri] *
      q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[3U];
  }

  /* End of Derivatives for StateSpace: '<S11>/State-Space' */

  /* Derivatives for Integrator: '<S52>/Integrator1' */
  _rtXdot->Integrator1_CSTATE_jt = q_2xamd1_stII_q8_usb_B.Product1_f;

  /* Derivatives for Integrator: '<S16>/Integrator2' */
  _rtXdot->Integrator2_CSTATE[0] = q_2xamd1_stII_q8_usb_B.Product[0];
  _rtXdot->Integrator2_CSTATE[1] = q_2xamd1_stII_q8_usb_B.Product[1];
  _rtXdot->Integrator2_CSTATE[2] = q_2xamd1_stII_q8_usb_B.Product[2];

  /* Derivatives for Integrator: '<S51>/Integrator2' */
  _rtXdot->Integrator2_CSTATE_i = q_2xamd1_stII_q8_usb_B.Product_m;

  /* Derivatives for Integrator: '<S52>/Integrator2' */
  _rtXdot->Integrator2_CSTATE_m = q_2xamd1_stII_q8_usb_B.Product_k;

  /* Derivatives for Enabled SubSystem: '<S10>/Ramp Signal' */
  if (q_2xamd1_stII_q8_usb_DW.RampSignal_MODE) {
    /* Derivatives for Integrator: '<S60>/Integrator' */
    _rtXdot->Integrator_CSTATE_p = q_2xamd1_stII_q8_usb_B.Gain;
  } else {
    ((XDot_q_2xamd1_stII_q8_usb_T *) q_2xamd1_stII_q8_usb_M->derivs)
      ->Integrator_CSTATE_p = 0.0;
  }

  /* End of Derivatives for SubSystem: '<S10>/Ramp Signal' */
}

/* Model initialize function */
void q_2xamd1_stII_q8_usb_initialize(void)
{
  /* Start for S-Function (hil_initialize_block): '<S7>/HIL Initialize' */

  /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q8_usb", "0", &q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options
      (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
       "update_rate=fast;ext_int_polarity=0;convert_polarity=1;watchdog_polarity=0;ext_int_watchdog=0;use_convert=0;pwm_immediate=0;decimation=1",
       137);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
      return;
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_AIPStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_AIPEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_AIMinimums =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMinimums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AIMinimums[i1] = (q_2xamd1_stII_q8_usb_P.HILInitialize_AILow);
        }
      }

      {
        int_T i1;
        real_T *dw_AIMaximums =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMaximums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AIMaximums[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_AIHigh;
        }
      }

      result = hil_set_analog_input_ranges
        (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
         q_2xamd1_stII_q8_usb_P.HILInitialize_AIChannels, 8U,
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMinimums[0],
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_AOPStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_AOPEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_AOMinimums =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMinimums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOMinimums[i1] = (q_2xamd1_stII_q8_usb_P.HILInitialize_AOLow);
        }
      }

      {
        int_T i1;
        real_T *dw_AOMaximums =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMaximums[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOMaximums[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_AOHigh;
        }
      }

      result = hil_set_analog_output_ranges
        (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
         q_2xamd1_stII_q8_usb_P.HILInitialize_AOChannels, 8U,
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMinimums[0],
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_AOStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_AOEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_AOVoltages =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOVoltages[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_AOInitial;
        }
      }

      result = hil_write_analog(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
        q_2xamd1_stII_q8_usb_P.HILInitialize_AOChannels, 8U,
        &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if (q_2xamd1_stII_q8_usb_P.HILInitialize_AOReset) {
      {
        int_T i1;
        real_T *dw_AOVoltages =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOVoltages[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_AOWatchdog;
        }
      }

      result = hil_watchdog_set_analog_expiration_state
        (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
         q_2xamd1_stII_q8_usb_P.HILInitialize_AOChannels, 8U,
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    result = hil_set_digital_directions
      (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
       q_2xamd1_stII_q8_usb_P.HILInitialize_DIChannels, 5U,
       q_2xamd1_stII_q8_usb_P.HILInitialize_DOChannels, 8U);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
      return;
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_DOStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_DOEnter && is_switching)) {
      {
        int_T i1;
        boolean_T *dw_DOBits = &q_2xamd1_stII_q8_usb_DW.HILInitialize_DOBits[0];
        for (i1=0; i1 < 8; i1++) {
          dw_DOBits[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_DOInitial;
        }
      }

      result = hil_write_digital(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
        q_2xamd1_stII_q8_usb_P.HILInitialize_DOChannels, 8U, (t_boolean *)
        &q_2xamd1_stII_q8_usb_DW.HILInitialize_DOBits[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if (q_2xamd1_stII_q8_usb_P.HILInitialize_DOReset) {
      {
        int_T i1;
        int32_T *dw_DOStates = &q_2xamd1_stII_q8_usb_DW.HILInitialize_DOStates[0];
        for (i1=0; i1 < 8; i1++) {
          dw_DOStates[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_DOWatchdog;
        }
      }

      result = hil_watchdog_set_digital_expiration_state
        (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
         q_2xamd1_stII_q8_usb_P.HILInitialize_DOChannels, 8U, (const
          t_digital_state *) &q_2xamd1_stII_q8_usb_DW.HILInitialize_DOStates[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_EIPStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_EIPEnter && is_switching)) {
      {
        int_T i1;
        int32_T *dw_QuadratureModes =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_QuadratureModes[0];
        for (i1=0; i1 < 8; i1++) {
          dw_QuadratureModes[i1] =
            q_2xamd1_stII_q8_usb_P.HILInitialize_EIQuadrature;
        }
      }

      result = hil_set_encoder_quadrature_mode
        (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
         q_2xamd1_stII_q8_usb_P.HILInitialize_EIChannels, 8U,
         (t_encoder_quadrature_mode *)
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_EIStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_EIEnter && is_switching)) {
      {
        int_T i1;
        int32_T *dw_InitialEICounts =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_InitialEICounts[0];
        for (i1=0; i1 < 8; i1++) {
          dw_InitialEICounts[i1] =
            q_2xamd1_stII_q8_usb_P.HILInitialize_EIInitial;
        }
      }

      result = hil_set_encoder_counts(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
        q_2xamd1_stII_q8_usb_P.HILInitialize_EIChannels, 8U,
        &q_2xamd1_stII_q8_usb_DW.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_POPStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_POPEnter && is_switching)) {
      uint32_T num_duty_cycle_modes = 0;
      uint32_T num_frequency_modes = 0;

      {
        int_T i1;
        int32_T *dw_POModeValues =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_POModeValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POModeValues[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_POModes;
        }
      }

      result = hil_set_pwm_mode(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
        q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels, 8U, (t_pwm_mode *)
        &q_2xamd1_stII_q8_usb_DW.HILInitialize_POModeValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }

      {
        int_T i1;
        const uint32_T *p_HILInitialize_POChannels =
          q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels;
        int32_T *dw_POModeValues =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_POModeValues[0];
        for (i1=0; i1 < 8; i1++) {
          if (dw_POModeValues[i1] == PWM_DUTY_CYCLE_MODE || dw_POModeValues[i1] ==
              PWM_ONE_SHOT_MODE || dw_POModeValues[i1] == PWM_TIME_MODE ||
              dw_POModeValues[i1] == PWM_RAW_MODE) {
            q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedChans[num_duty_cycle_modes]
              = (p_HILInitialize_POChannels[i1]);
            q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs[num_duty_cycle_modes]
              = q_2xamd1_stII_q8_usb_P.HILInitialize_POFrequency;
            num_duty_cycle_modes++;
          } else {
            q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedChans[7U -
              num_frequency_modes] = (p_HILInitialize_POChannels[i1]);
            q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs[7U -
              num_frequency_modes] =
              q_2xamd1_stII_q8_usb_P.HILInitialize_POFrequency;
            num_frequency_modes++;
          }
        }
      }

      if (num_duty_cycle_modes > 0) {
        result = hil_set_pwm_frequency
          (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
           &q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedChans[0],
           num_duty_cycle_modes,
           &q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs[0]);
        if (result < 0) {
          msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
            (_rt_error_message));
          rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
          return;
        }
      }

      if (num_frequency_modes > 0) {
        result = hil_set_pwm_duty_cycle
          (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
           &q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedChans[num_duty_cycle_modes],
           num_frequency_modes,
           &q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs[num_duty_cycle_modes]);
        if (result < 0) {
          msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
            (_rt_error_message));
          rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
          return;
        }
      }

      {
        int_T i1;
        int32_T *dw_POModeValues =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_POModeValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POModeValues[i1] =
            q_2xamd1_stII_q8_usb_P.HILInitialize_POConfiguration;
        }
      }

      {
        int_T i1;
        int32_T *dw_POAlignValues =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_POAlignValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POAlignValues[i1] =
            q_2xamd1_stII_q8_usb_P.HILInitialize_POAlignment;
        }
      }

      {
        int_T i1;
        int32_T *dw_POPolarityVals =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_POPolarityVals[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POPolarityVals[i1] =
            q_2xamd1_stII_q8_usb_P.HILInitialize_POPolarity;
        }
      }

      result = hil_set_pwm_configuration
        (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
         q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels, 8U,
         (t_pwm_configuration *)
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_POModeValues[0],
         (t_pwm_alignment *)
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_POAlignValues[0],
         (t_pwm_polarity *)
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_POPolarityVals[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }

      {
        int_T i1;
        real_T *dw_POSortedFreqs =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POSortedFreqs[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_POLeading;
        }
      }

      {
        int_T i1;
        real_T *dw_POValues = &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_POTrailing;
        }
      }

      result = hil_set_pwm_deadband(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
        q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels, 8U,
        &q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs[0],
        &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_POStart && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_POEnter && is_switching)) {
      {
        int_T i1;
        real_T *dw_POValues = &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_POInitial;
        }
      }

      result = hil_write_pwm(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
        q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels, 8U,
        &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }

    if (q_2xamd1_stII_q8_usb_P.HILInitialize_POReset) {
      {
        int_T i1;
        real_T *dw_POValues = &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_POWatchdog;
        }
      }

      result = hil_watchdog_set_pwm_expiration_state
        (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
         q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels, 8U,
         &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_timebase_block): '<S7>/HIL Read Timebase' */

  /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL Read Timebase (hil_read_timebase_block) */
  {
    t_error result;
    result = hil_task_create_reader(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
      q_2xamd1_stII_q8_usb_P.HILReadTimebase_SamplesInBuffer,
      q_2xamd1_stII_q8_usb_P.HILReadTimebase_AnalogChannels, 3U,
      q_2xamd1_stII_q8_usb_P.HILReadTimebase_EncoderChannels, 3U,
      q_2xamd1_stII_q8_usb_P.HILReadTimebase_DigitalChannels, 5U,
      &q_2xamd1_stII_q8_usb_P.HILReadTimebase_OtherChannels, 1U,
      &q_2xamd1_stII_q8_usb_DW.HILReadTimebase_Task);
    if (result >= 0) {
      result = hil_task_set_buffer_overflow_mode
        (q_2xamd1_stII_q8_usb_DW.HILReadTimebase_Task, (t_buffer_overflow_mode)
         (q_2xamd1_stII_q8_usb_P.HILReadTimebase_OverflowMode - 1));
    }

    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
    }
  }

  /* Start for Enabled SubSystem: '<S7>/Check for amp fault' */
  q_2xamd1_stII_q8_usb_DW.Checkforampfault_MODE = false;

  /* End of Start for SubSystem: '<S7>/Check for amp fault' */

  /* Start for Enabled SubSystem: '<S7>/Check if stage at home' */
  q_2xamd1_stII_q8_usb_DW.Checkifstageathome_MODE = false;

  /* End of Start for SubSystem: '<S7>/Check if stage at home' */

  /* Start for S-Function (continuous_sigmoid_block): '<S10>/Continuous Sigmoid' */

  /* S-Function Block: q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Continuous Sigmoid (continuous_sigmoid_block) */
  {
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T1] = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T2] = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_T3] = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X0] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X1] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_X2] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_XD] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_V0] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_VP] = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_AP] = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[SIGMOID_PARAMETER_HOLD_OFF]
      = q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_Hold;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Target =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PPos =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PVel =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VI;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MVel =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MAcc =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax;
  }

  /* Start for S-Function (continuous_sigmoid_block): '<S9>/Continuous Sigmoid' */

  /* S-Function Block: q_2xamd1_stII_q8_usb/Sine Sweep Setpoint/Continuous Sigmoid (continuous_sigmoid_block) */
  {
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T1] =
      0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T2] =
      0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_T3] =
      0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X0] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI_c;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X1] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI_c;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_X2] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI_c;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_XD] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI_c;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_V0] =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VI_h;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_VP] =
      0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_AP] =
      0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[SIGMOID_PARAMETER_HOLD_OFF]
      = q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_Hold_e;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Target =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI_c;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PPos =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_XI_c;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PVel =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VI_h;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Flag_b = 0;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MVel =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_VMax_j;
    q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MAcc =
      q_2xamd1_stII_q8_usb_P.ContinuousSigmoid_AMax_a;
  }

  /* Start for Constant: '<S51>/x0' */
  q_2xamd1_stII_q8_usb_B.x0 = q_2xamd1_stII_q8_usb_P.x0_Value;

  /* Start for S-Function (multistage_limiter_block): '<S48>/Voltage Limit' */
  {
    q_2xamd1_stII_q8_usb_DW.VoltageLimit_IWORK.State = 0;
  }

  /* Start for S-Function (hil_watchdog_block): '<S35>/HIL Watchdog' */

  /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL STII Watchdog/HIL Watchdog (hil_watchdog_block) */
  {
    q_2xamd1_stII_q8_usb_DW.HILWatchdog_IsStarted = false;
  }

  /* Start for Constant: '<S16>/x0' */
  q_2xamd1_stII_q8_usb_B.x0_f = q_2xamd1_stII_q8_usb_P.x0_Value_f;

  /* Start for Constant: '<S52>/x0' */
  q_2xamd1_stII_q8_usb_B.x0_o = q_2xamd1_stII_q8_usb_P.x0_Value_h;

  /* Start for Atomic SubSystem: '<S12>/Bias Removal' */
  /* Start for Enabled SubSystem: '<S15>/Enabled Moving Average' */
  q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_MODE = false;

  /* End of Start for SubSystem: '<S15>/Enabled Moving Average' */

  /* Start for SwitchCase: '<S15>/Switch Case' */
  q_2xamd1_stII_q8_usb_DW.SwitchCase_ActiveSubsystem = -1;

  /* End of Start for SubSystem: '<S12>/Bias Removal' */

  /* Start for S-Function (smooth_signal_generator_block): '<S10>/Smooth Signal Generator' */

  /* S-Function Block: q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Smooth Signal Generator (smooth_signal_generator_block) */
  {
    real_T T = 1.0 / q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_Frequency;
    q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp =
      q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_Amplitude;
    q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.W = TWO_PI *
      q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_Frequency;
    q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_dt = sin
      (q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.W * 0.0005);
    q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_dt = cos
      (q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.W * 0.0005);
    q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t = sin
      (q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_InitialPh);
    q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t = cos
      (q_2xamd1_stII_q8_usb_P.SmoothSignalGenerator_InitialPh);
  }

  /* Start for Enabled SubSystem: '<S10>/Ramp Signal' */
  q_2xamd1_stII_q8_usb_DW.RampSignal_MODE = false;

  /* End of Start for SubSystem: '<S10>/Ramp Signal' */
  {
    int32_T i;

    /* InitializeConditions for S-Function (debouncer_block): '<S43>/debouncer_block' incorporates:
     *  Constant: '<S43>/threshold'
     */
    {
      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[0] = 0.0;
      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] = 0.0;
      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[2] = 0.0;
    }

    /* InitializeConditions for S-Function (debouncer_block): '<S44>/debouncer_block' incorporates:
     *  Constant: '<S44>/threshold'
     */
    {
      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[0] = 0.0;
      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] = 0.0;
      q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[2] = 0.0;
    }

    /* InitializeConditions for Integrator: '<S51>/Integrator1' */
    if (rtmIsFirstInitCond(q_2xamd1_stII_q8_usb_M)) {
      q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE = 0.0;
    }

    q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK = 1;

    /* End of InitializeConditions for Integrator: '<S51>/Integrator1' */

    /* InitializeConditions for Integrator: '<S63>/Integrator' */
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_X.Integrator_CSTATE[i] =
        q_2xamd1_stII_q8_usb_P.Integrator_IC_e;
    }

    /* End of InitializeConditions for Integrator: '<S63>/Integrator' */

    /* InitializeConditions for Integrator: '<S16>/Integrator1' */
    if (rtmIsFirstInitCond(q_2xamd1_stII_q8_usb_M)) {
      q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[0] = 0.0;
      q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[1] = 0.0;
      q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_j[2] = 0.0;
    }

    q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_i = 1;

    /* End of InitializeConditions for Integrator: '<S16>/Integrator1' */

    /* InitializeConditions for StateSpace: '<S11>/State-Space' */
    for (i = 0; i < 16; i++) {
      q_2xamd1_stII_q8_usb_X.StateSpace_CSTATE[i] =
        q_2xamd1_stII_q8_usb_P.StateSpace_InitialCondition;
    }

    /* End of InitializeConditions for StateSpace: '<S11>/State-Space' */

    /* InitializeConditions for Integrator: '<S52>/Integrator1' */
    if (rtmIsFirstInitCond(q_2xamd1_stII_q8_usb_M)) {
      q_2xamd1_stII_q8_usb_X.Integrator1_CSTATE_jt = 0.0;
    }

    q_2xamd1_stII_q8_usb_DW.Integrator1_IWORK_p = 1;

    /* End of InitializeConditions for Integrator: '<S52>/Integrator1' */

    /* InitializeConditions for DiscreteIntegrator: '<S54>/Discrete-Time Integrator' */
    q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_DSTATE =
      q_2xamd1_stII_q8_usb_P.DiscreteTimeIntegrator_IC;
    q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_PrevRese = 2;

    /* InitializeConditions for Derivative: '<S49>/Derivative' */
    q_2xamd1_stII_q8_usb_DW.TimeStampA = (rtInf);
    q_2xamd1_stII_q8_usb_DW.TimeStampB = (rtInf);

    /* InitializeConditions for Integrator: '<S51>/Integrator2' */
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE_i =
      q_2xamd1_stII_q8_usb_P.Integrator2_IC_i;

    /* InitializeConditions for Integrator: '<S52>/Integrator2' */
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE_m =
      q_2xamd1_stII_q8_usb_P.Integrator2_IC_c;

    /* InitializeConditions for DiscreteTransferFcn: '<S59>/Discrete Transfer Fcn' */
    q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_states =
      q_2xamd1_stII_q8_usb_P.DiscreteTransferFcn_InitialStat;

    /* SystemInitialize for Atomic SubSystem: '<S12>/Bias Removal' */
    /* SystemInitialize for Enabled SubSystem: '<S15>/Enabled Moving Average' */
    /* InitializeConditions for UnitDelay: '<S21>/Unit Delay' */
    q_2xamd1_stII_q8_usb_DW.UnitDelay_DSTATE =
      q_2xamd1_stII_q8_usb_P.UnitDelay_InitialCondition;

    /* End of SystemInitialize for SubSystem: '<S15>/Enabled Moving Average' */

    /* SystemInitialize for IfAction SubSystem: '<S15>/Switch Case Action Subsystem' */
    /* SystemInitialize for Outport: '<S18>/zero' incorporates:
     *  Constant: '<S18>/Constant'
     */
    q_2xamd1_stII_q8_usb_B.Constant = q_2xamd1_stII_q8_usb_P.zero_Y0;

    /* End of SystemInitialize for SubSystem: '<S15>/Switch Case Action Subsystem' */
    /* End of SystemInitialize for SubSystem: '<S12>/Bias Removal' */

    /* InitializeConditions for Integrator: '<S16>/Integrator2' */
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[0] =
      q_2xamd1_stII_q8_usb_P.Integrator2_IC;

    /* SystemInitialize for Atomic SubSystem: '<S12>/Bias Removal' */
    /* SystemInitialize for Enabled SubSystem: '<S15>/Enabled Moving Average' */
    /* InitializeConditions for UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )' */
    q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[0] =
      q_2xamd1_stII_q8_usb_P.Sumk1n1xk_InitialCondition;

    /* End of SystemInitialize for SubSystem: '<S15>/Enabled Moving Average' */

    /* SystemInitialize for IfAction SubSystem: '<S15>/Switch Case Action Subsystem1' */
    /* SystemInitialize for Outport: '<S19>/Vbiased' incorporates:
     *  Inport: '<S19>/Vin'
     */
    q_2xamd1_stII_q8_usb_B.Vin[0] = q_2xamd1_stII_q8_usb_P.Vbiased_Y0;

    /* End of SystemInitialize for SubSystem: '<S15>/Switch Case Action Subsystem1' */

    /* SystemInitialize for IfAction SubSystem: '<S15>/Switch Case Action Subsystem2' */
    /* SystemInitialize for Outport: '<S20>/Vunbiased' incorporates:
     *  Inport: '<S20>/V-Vavg'
     */
    q_2xamd1_stII_q8_usb_B.VVavg[0] = q_2xamd1_stII_q8_usb_P.Vunbiased_Y0;

    /* End of SystemInitialize for SubSystem: '<S15>/Switch Case Action Subsystem2' */
    /* End of SystemInitialize for SubSystem: '<S12>/Bias Removal' */

    /* InitializeConditions for Integrator: '<S16>/Integrator2' */
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[1] =
      q_2xamd1_stII_q8_usb_P.Integrator2_IC;

    /* SystemInitialize for Atomic SubSystem: '<S12>/Bias Removal' */
    /* SystemInitialize for Enabled SubSystem: '<S15>/Enabled Moving Average' */
    /* InitializeConditions for UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )' */
    q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[1] =
      q_2xamd1_stII_q8_usb_P.Sumk1n1xk_InitialCondition;

    /* End of SystemInitialize for SubSystem: '<S15>/Enabled Moving Average' */

    /* SystemInitialize for IfAction SubSystem: '<S15>/Switch Case Action Subsystem1' */
    /* SystemInitialize for Outport: '<S19>/Vbiased' incorporates:
     *  Inport: '<S19>/Vin'
     */
    q_2xamd1_stII_q8_usb_B.Vin[1] = q_2xamd1_stII_q8_usb_P.Vbiased_Y0;

    /* End of SystemInitialize for SubSystem: '<S15>/Switch Case Action Subsystem1' */

    /* SystemInitialize for IfAction SubSystem: '<S15>/Switch Case Action Subsystem2' */
    /* SystemInitialize for Outport: '<S20>/Vunbiased' incorporates:
     *  Inport: '<S20>/V-Vavg'
     */
    q_2xamd1_stII_q8_usb_B.VVavg[1] = q_2xamd1_stII_q8_usb_P.Vunbiased_Y0;

    /* End of SystemInitialize for SubSystem: '<S15>/Switch Case Action Subsystem2' */
    /* End of SystemInitialize for SubSystem: '<S12>/Bias Removal' */

    /* InitializeConditions for Integrator: '<S16>/Integrator2' */
    q_2xamd1_stII_q8_usb_X.Integrator2_CSTATE[2] =
      q_2xamd1_stII_q8_usb_P.Integrator2_IC;

    /* SystemInitialize for Atomic SubSystem: '<S12>/Bias Removal' */
    /* SystemInitialize for Enabled SubSystem: '<S15>/Enabled Moving Average' */
    /* InitializeConditions for UnitDelay: '<S17>/Sum( k=1,n-1, x(k) )' */
    q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[2] =
      q_2xamd1_stII_q8_usb_P.Sumk1n1xk_InitialCondition;

    /* End of SystemInitialize for SubSystem: '<S15>/Enabled Moving Average' */

    /* SystemInitialize for IfAction SubSystem: '<S15>/Switch Case Action Subsystem1' */
    /* SystemInitialize for Outport: '<S19>/Vbiased' incorporates:
     *  Inport: '<S19>/Vin'
     */
    q_2xamd1_stII_q8_usb_B.Vin[2] = q_2xamd1_stII_q8_usb_P.Vbiased_Y0;

    /* End of SystemInitialize for SubSystem: '<S15>/Switch Case Action Subsystem1' */

    /* SystemInitialize for IfAction SubSystem: '<S15>/Switch Case Action Subsystem2' */
    /* SystemInitialize for Outport: '<S20>/Vunbiased' incorporates:
     *  Inport: '<S20>/V-Vavg'
     */
    q_2xamd1_stII_q8_usb_B.VVavg[2] = q_2xamd1_stII_q8_usb_P.Vunbiased_Y0;

    /* End of SystemInitialize for SubSystem: '<S15>/Switch Case Action Subsystem2' */
    /* End of SystemInitialize for SubSystem: '<S12>/Bias Removal' */

    /* SystemInitialize for Enabled SubSystem: '<S10>/Ramp Signal' */
    /* InitializeConditions for Integrator: '<S60>/Integrator' */
    q_2xamd1_stII_q8_usb_X.Integrator_CSTATE_p =
      q_2xamd1_stII_q8_usb_P.Integrator_IC;

    /* End of SystemInitialize for SubSystem: '<S10>/Ramp Signal' */

    /* set "at time zero" to false */
    if (rtmIsFirstInitCond(q_2xamd1_stII_q8_usb_M)) {
      rtmSetFirstInitCond(q_2xamd1_stII_q8_usb_M, 0);
    }
  }
}

/* Model terminate function */
void q_2xamd1_stII_q8_usb_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<S7>/HIL Initialize' */

  /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    t_uint32 num_final_digital_outputs = 0;
    t_uint32 num_final_pwm_outputs = 0;
    hil_task_stop_all(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    hil_monitor_stop_all(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    is_switching = false;

    /* S-Function Block: q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL STII Watchdog/HIL Watchdog (hil_watchdog_block) */
    {
      hil_watchdog_stop(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_AOTerminate && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_AOExit && is_switching)) {
      {
        int_T i1;
        real_T *dw_AOVoltages =
          &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[0];
        for (i1=0; i1 < 8; i1++) {
          dw_AOVoltages[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_AOFinal;
        }
      }

      num_final_analog_outputs = 8U;
    } else {
      num_final_analog_outputs = 0;
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_DOTerminate && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_DOExit && is_switching)) {
      {
        int_T i1;
        boolean_T *dw_DOBits = &q_2xamd1_stII_q8_usb_DW.HILInitialize_DOBits[0];
        for (i1=0; i1 < 8; i1++) {
          dw_DOBits[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_DOFinal;
        }
      }

      num_final_digital_outputs = 8U;
    } else {
      num_final_digital_outputs = 0;
    }

    if ((q_2xamd1_stII_q8_usb_P.HILInitialize_POTerminate && !is_switching) ||
        (q_2xamd1_stII_q8_usb_P.HILInitialize_POExit && is_switching)) {
      {
        int_T i1;
        real_T *dw_POValues = &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0];
        for (i1=0; i1 < 8; i1++) {
          dw_POValues[i1] = q_2xamd1_stII_q8_usb_P.HILInitialize_POFinal;
        }
      }

      num_final_pwm_outputs = 8U;
    } else {
      num_final_pwm_outputs = 0;
    }

    if (0
        || num_final_analog_outputs > 0
        || num_final_pwm_outputs > 0
        || num_final_digital_outputs > 0
        ) {
      /* Attempt to write the final outputs atomically (due to firmware issue in old Q2-USB). Otherwise write channels individually */
      result = hil_write(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card
                         , q_2xamd1_stII_q8_usb_P.HILInitialize_AOChannels,
                         num_final_analog_outputs
                         , q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels,
                         num_final_pwm_outputs
                         , q_2xamd1_stII_q8_usb_P.HILInitialize_DOChannels,
                         num_final_digital_outputs
                         , NULL, 0
                         , &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[0]
                         , &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0]
                         , (t_boolean *)
                         &q_2xamd1_stII_q8_usb_DW.HILInitialize_DOBits[0]
                         , NULL
                         );
      if (result == -QERR_HIL_WRITE_NOT_SUPPORTED) {
        t_error local_result;
        result = 0;

        /* The hil_write operation is not supported by this card. Write final outputs for each channel type */
        if (num_final_analog_outputs > 0) {
          local_result = hil_write_analog
            (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
             q_2xamd1_stII_q8_usb_P.HILInitialize_AOChannels,
             num_final_analog_outputs,
             &q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[0]);
          if (local_result < 0) {
            result = local_result;
          }
        }

        if (num_final_pwm_outputs > 0) {
          local_result = hil_write_pwm
            (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
             q_2xamd1_stII_q8_usb_P.HILInitialize_POChannels,
             num_final_pwm_outputs,
             &q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[0]);
          if (local_result < 0) {
            result = local_result;
          }
        }

        if (num_final_digital_outputs > 0) {
          local_result = hil_write_digital
            (q_2xamd1_stII_q8_usb_DW.HILInitialize_Card,
             q_2xamd1_stII_q8_usb_P.HILInitialize_DOChannels,
             num_final_digital_outputs, (t_boolean *)
             &q_2xamd1_stII_q8_usb_DW.HILInitialize_DOBits[0]);
          if (local_result < 0) {
            result = local_result;
          }
        }

        if (result < 0) {
          msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
            (_rt_error_message));
          rtmSetErrorStatus(q_2xamd1_stII_q8_usb_M, _rt_error_message);
        }
      }
    }

    hil_task_delete_all(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    hil_monitor_delete_all(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    hil_close(q_2xamd1_stII_q8_usb_DW.HILInitialize_Card);
    q_2xamd1_stII_q8_usb_DW.HILInitialize_Card = NULL;
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/

/* Solver interface called by GRT_Main */
#ifndef USE_GENERATED_SOLVER

void rt_ODECreateIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEDestroyIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEUpdateContinuousStates(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

#endif

void MdlOutputs(int_T tid)
{
  q_2xamd1_stII_q8_usb_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  q_2xamd1_stII_q8_usb_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  q_2xamd1_stII_q8_usb_initialize();
}

void MdlTerminate(void)
{
  q_2xamd1_stII_q8_usb_terminate();
}

/* Registration function */
RT_MODEL_q_2xamd1_stII_q8_usb_T *q_2xamd1_stII_q8_usb(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)q_2xamd1_stII_q8_usb_M, 0,
                sizeof(RT_MODEL_q_2xamd1_stII_q8_usb_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
                          &q_2xamd1_stII_q8_usb_M->Timing.simTimeStep);
    rtsiSetTPtr(&q_2xamd1_stII_q8_usb_M->solverInfo, &rtmGetTPtr
                (q_2xamd1_stII_q8_usb_M));
    rtsiSetStepSizePtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
                       &q_2xamd1_stII_q8_usb_M->Timing.stepSize0);
    rtsiSetdXPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
                 &q_2xamd1_stII_q8_usb_M->derivs);
    rtsiSetContStatesPtr(&q_2xamd1_stII_q8_usb_M->solverInfo, (real_T **)
                         &q_2xamd1_stII_q8_usb_M->contStates);
    rtsiSetNumContStatesPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
      &q_2xamd1_stII_q8_usb_M->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
      &q_2xamd1_stII_q8_usb_M->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
      &q_2xamd1_stII_q8_usb_M->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
      &q_2xamd1_stII_q8_usb_M->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
                          (&rtmGetErrorStatus(q_2xamd1_stII_q8_usb_M)));
    rtsiSetRTModelPtr(&q_2xamd1_stII_q8_usb_M->solverInfo,
                      q_2xamd1_stII_q8_usb_M);
  }

  rtsiSetSimTimeStep(&q_2xamd1_stII_q8_usb_M->solverInfo, MAJOR_TIME_STEP);
  q_2xamd1_stII_q8_usb_M->intgData.y = q_2xamd1_stII_q8_usb_M->odeY;
  q_2xamd1_stII_q8_usb_M->intgData.f[0] = q_2xamd1_stII_q8_usb_M->odeF[0];
  q_2xamd1_stII_q8_usb_M->intgData.f[1] = q_2xamd1_stII_q8_usb_M->odeF[1];
  q_2xamd1_stII_q8_usb_M->intgData.f[2] = q_2xamd1_stII_q8_usb_M->odeF[2];
  q_2xamd1_stII_q8_usb_M->intgData.f[3] = q_2xamd1_stII_q8_usb_M->odeF[3];
  q_2xamd1_stII_q8_usb_M->contStates = ((real_T *) &q_2xamd1_stII_q8_usb_X);
  rtsiSetSolverData(&q_2xamd1_stII_q8_usb_M->solverInfo, (void *)
                    &q_2xamd1_stII_q8_usb_M->intgData);
  rtsiSetSolverName(&q_2xamd1_stII_q8_usb_M->solverInfo,"ode4");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = q_2xamd1_stII_q8_usb_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    q_2xamd1_stII_q8_usb_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    q_2xamd1_stII_q8_usb_M->Timing.sampleTimes =
      (&q_2xamd1_stII_q8_usb_M->Timing.sampleTimesArray[0]);
    q_2xamd1_stII_q8_usb_M->Timing.offsetTimes =
      (&q_2xamd1_stII_q8_usb_M->Timing.offsetTimesArray[0]);

    /* task periods */
    q_2xamd1_stII_q8_usb_M->Timing.sampleTimes[0] = (0.0);
    q_2xamd1_stII_q8_usb_M->Timing.sampleTimes[1] = (0.0005);

    /* task offsets */
    q_2xamd1_stII_q8_usb_M->Timing.offsetTimes[0] = (0.0);
    q_2xamd1_stII_q8_usb_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(q_2xamd1_stII_q8_usb_M, &q_2xamd1_stII_q8_usb_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = q_2xamd1_stII_q8_usb_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    q_2xamd1_stII_q8_usb_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(q_2xamd1_stII_q8_usb_M, 20.0);
  q_2xamd1_stII_q8_usb_M->Timing.stepSize0 = 0.0005;
  q_2xamd1_stII_q8_usb_M->Timing.stepSize1 = 0.0005;
  rtmSetFirstInitCond(q_2xamd1_stII_q8_usb_M, 1);

  /* External mode info */
  q_2xamd1_stII_q8_usb_M->Sizes.checksums[0] = (4222123335U);
  q_2xamd1_stII_q8_usb_M->Sizes.checksums[1] = (3457791298U);
  q_2xamd1_stII_q8_usb_M->Sizes.checksums[2] = (2254480788U);
  q_2xamd1_stII_q8_usb_M->Sizes.checksums[3] = (3193314352U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[15];
    q_2xamd1_stII_q8_usb_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.EnabledMovingAverage_SubsysRanB;
    systemRan[2] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem_Subsy;
    systemRan[3] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem1_Subs;
    systemRan[4] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.SwitchCaseActionSubsystem2_Subs;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.BottomCartPositionWatchdog_Subs;
    systemRan[7] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.TopCartPositionWatchdog_SubsysR;
    systemRan[8] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.Checkforampfault_SubsysRanBC;
    systemRan[9] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.Checkifstageathome_SubsysRanBC;
    systemRan[10] = (sysRanDType *)
      &q_2xamd1_stII_q8_usb_DW.RampSignal_SubsysRanBC;
    systemRan[11] = &rtAlwaysEnabled;
    systemRan[12] = &rtAlwaysEnabled;
    systemRan[13] = &rtAlwaysEnabled;
    systemRan[14] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(q_2xamd1_stII_q8_usb_M->extModeInfo,
      &q_2xamd1_stII_q8_usb_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(q_2xamd1_stII_q8_usb_M->extModeInfo,
                        q_2xamd1_stII_q8_usb_M->Sizes.checksums);
    rteiSetTPtr(q_2xamd1_stII_q8_usb_M->extModeInfo, rtmGetTPtr
                (q_2xamd1_stII_q8_usb_M));
  }

  q_2xamd1_stII_q8_usb_M->solverInfoPtr = (&q_2xamd1_stII_q8_usb_M->solverInfo);
  q_2xamd1_stII_q8_usb_M->Timing.stepSize = (0.0005);
  rtsiSetFixedStepSize(&q_2xamd1_stII_q8_usb_M->solverInfo, 0.0005);
  rtsiSetSolverMode(&q_2xamd1_stII_q8_usb_M->solverInfo,
                    SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  q_2xamd1_stII_q8_usb_M->blockIO = ((void *) &q_2xamd1_stII_q8_usb_B);
  (void) memset(((void *) &q_2xamd1_stII_q8_usb_B), 0,
                sizeof(B_q_2xamd1_stII_q8_usb_T));

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_B.Integrator[i] = 0.0;
    }

    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_B.Sum3[i] = 0.0;
    }

    q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.HILReadTimebase_o1[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion5 = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion2 = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion3 = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion5_h = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion2_b = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion3_a = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1 = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2 = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3 = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4 = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o1_g = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o2_m = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o3_b = 0.0;
    q_2xamd1_stII_q8_usb_B.ContinuousSigmoid_o4_k = 0.0;
    q_2xamd1_stII_q8_usb_B.SetpointSwitch[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.SetpointSwitch[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.EncoderCalibrationmcount = 0.0;
    q_2xamd1_stII_q8_usb_B.ProportionalGainVm = 0.0;
    q_2xamd1_stII_q8_usb_B.x0 = 0.0;
    q_2xamd1_stII_q8_usb_B.Add = 0.0;
    q_2xamd1_stII_q8_usb_B.VoltageLimit = 0.0;
    q_2xamd1_stII_q8_usb_B.AMDControlMode = 0.0;
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.AmplifierVoltageSaturation[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.AmplifierGainPreCompensation[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.AmplifierGainPreCompensation[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Conversiontom1 = 0.0;
    q_2xamd1_stII_q8_usb_B.Conversiontom2 = 0.0;
    q_2xamd1_stII_q8_usb_B.x0_f = 0.0;
    q_2xamd1_stII_q8_usb_B.floor1gtoms2 = 0.0;
    q_2xamd1_stII_q8_usb_B.floor1gtoms1 = 0.0;
    q_2xamd1_stII_q8_usb_B.ControlMode[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.ControlMode[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.mtocm[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.mtocm[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.x0_o = 0.0;
    q_2xamd1_stII_q8_usb_B.ms2tog = 0.0;
    q_2xamd1_stII_q8_usb_B.mtomm = 0.0;
    q_2xamd1_stII_q8_usb_B.mtomm1 = 0.0;
    q_2xamd1_stII_q8_usb_B.Product[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.Product[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Product[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.Product1[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.Product1[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Product1[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.Kp_c2741[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.Kp_c2741[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Kp_c2741_a[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.Kp_c2741_a[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Abs1 = 0.0;
    q_2xamd1_stII_q8_usb_B.DiscreteTimeIntegrator = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion = 0.0;
    q_2xamd1_stII_q8_usb_B.countsstoms = 0.0;
    q_2xamd1_stII_q8_usb_B.Product_m = 0.0;
    q_2xamd1_stII_q8_usb_B.Product1_g = 0.0;
    q_2xamd1_stII_q8_usb_B.Product_k = 0.0;
    q_2xamd1_stII_q8_usb_B.Product1_f = 0.0;
    q_2xamd1_stII_q8_usb_B.DataTypeConversion_b = 0.0;
    q_2xamd1_stII_q8_usb_B.DiscreteTransferFcn = 0.0;
    q_2xamd1_stII_q8_usb_B.SweepPositionLimitm = 0.0;
    q_2xamd1_stII_q8_usb_B.cmtom = 0.0;
    q_2xamd1_stII_q8_usb_B.PositionLimit = 0.0;
    q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.TmpSignalConversionAtStateSpace[3] = 0.0;
    q_2xamd1_stII_q8_usb_B.Gain = 0.0;
    q_2xamd1_stII_q8_usb_B.Product_e = 0.0;
    q_2xamd1_stII_q8_usb_B.Switch[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.Switch[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Switch[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.VVavg[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.VVavg[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.VVavg[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.Vin[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.Vin[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Vin[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.Constant = 0.0;
    q_2xamd1_stII_q8_usb_B.Count = 0.0;
    q_2xamd1_stII_q8_usb_B.Sum[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.Sum[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.Sum[2] = 0.0;
    q_2xamd1_stII_q8_usb_B.div[0] = 0.0;
    q_2xamd1_stII_q8_usb_B.div[1] = 0.0;
    q_2xamd1_stII_q8_usb_B.div[2] = 0.0;
  }

  /* parameters */
  q_2xamd1_stII_q8_usb_M->defaultParam = ((real_T *)&q_2xamd1_stII_q8_usb_P);

  /* states (continuous) */
  {
    real_T *x = (real_T *) &q_2xamd1_stII_q8_usb_X;
    q_2xamd1_stII_q8_usb_M->contStates = (x);
    (void) memset((void *)&q_2xamd1_stII_q8_usb_X, 0,
                  sizeof(X_q_2xamd1_stII_q8_usb_T));
  }

  /* states (dwork) */
  q_2xamd1_stII_q8_usb_M->dwork = ((void *) &q_2xamd1_stII_q8_usb_DW);
  (void) memset((void *)&q_2xamd1_stII_q8_usb_DW, 0,
                sizeof(DW_q_2xamd1_stII_q8_usb_T));
  q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[0] = 0.0;
  q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[1] = 0.0;
  q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE[2] = 0.0;
  q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[0] = 0.0;
  q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[1] = 0.0;
  q_2xamd1_stII_q8_usb_DW.debouncer_block_DSTATE_a[2] = 0.0;
  q_2xamd1_stII_q8_usb_DW.DiscreteTimeIntegrator_DSTATE = 0.0;
  q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_states = 0.0;
  q_2xamd1_stII_q8_usb_DW.UnitDelay_DSTATE = 0.0;
  q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[0] = 0.0;
  q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[1] = 0.0;
  q_2xamd1_stII_q8_usb_DW.Sumk1n1xk_DSTATE[2] = 0.0;

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMinimums[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_AIMaximums[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMinimums[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_AOMaximums[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_AOVoltages[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_FilterFrequency[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_POSortedFreqs[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 8; i++) {
      q_2xamd1_stII_q8_usb_DW.HILInitialize_POValues[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 11; i++) {
      q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid[i] = 0.0;
    }
  }

  {
    int32_T i;
    for (i = 0; i < 11; i++) {
      q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_Sigmoid_l[i] = 0.0;
    }
  }

  q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer[0] = 0.0;
  q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer[1] = 0.0;
  q_2xamd1_stII_q8_usb_DW.HILWrite_AnalogBuffer[2] = 0.0;
  q_2xamd1_stII_q8_usb_DW.TimeStampA = 0.0;
  q_2xamd1_stII_q8_usb_DW.LastUAtTimeA = 0.0;
  q_2xamd1_stII_q8_usb_DW.TimeStampB = 0.0;
  q_2xamd1_stII_q8_usb_DW.LastUAtTimeB = 0.0;
  q_2xamd1_stII_q8_usb_DW.DiscreteTransferFcn_tmp = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Time = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.Target = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PPos = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.PVel = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MVel = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK.MAcc = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Time = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.Target = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PPos = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.PVel = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MVel = 0.0;
  q_2xamd1_stII_q8_usb_DW.ContinuousSigmoid_RWORK_k.MAcc = 0.0;
  q_2xamd1_stII_q8_usb_DW.VoltageLimit_RWORK.Mean = 0.0;
  q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Amp = 0.0;
  q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.W = 0.0;
  q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_dt = 0.0;
  q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_dt = 0.0;
  q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Sin_w_t = 0.0;
  q_2xamd1_stII_q8_usb_DW.SmoothSignalGenerator_RWORK.Cos_w_t = 0.0;

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    q_2xamd1_stII_q8_usb_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 18;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Initialize Sizes */
  q_2xamd1_stII_q8_usb_M->Sizes.numContStates = (35);/* Number of continuous states */
  q_2xamd1_stII_q8_usb_M->Sizes.numPeriodicContStates = (0);
                                      /* Number of periodic continuous states */
  q_2xamd1_stII_q8_usb_M->Sizes.numY = (0);/* Number of model outputs */
  q_2xamd1_stII_q8_usb_M->Sizes.numU = (0);/* Number of model inputs */
  q_2xamd1_stII_q8_usb_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  q_2xamd1_stII_q8_usb_M->Sizes.numSampTimes = (2);/* Number of sample times */
  q_2xamd1_stII_q8_usb_M->Sizes.numBlocks = (269);/* Number of blocks */
  q_2xamd1_stII_q8_usb_M->Sizes.numBlockIO = (79);/* Number of block outputs */
  q_2xamd1_stII_q8_usb_M->Sizes.numBlockPrms = (1221);/* Sum of parameter "widths" */
  return q_2xamd1_stII_q8_usb_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
