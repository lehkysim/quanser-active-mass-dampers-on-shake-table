/*
 * q_2xamd1_stII_q8_usb.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "q_2xamd1_stII_q8_usb".
 *
 * Model version              : 6.24
 * Simulink Coder version : 9.5 (R2021a) 14-Nov-2020
 * C source code generated on : Wed May 15 16:05:53 2024
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_q_2xamd1_stII_q8_usb_h_
#define RTW_HEADER_q_2xamd1_stII_q8_usb_h_
#include <float.h>
#include <math.h>
#include <string.h>
#ifndef q_2xamd1_stII_q8_usb_COMMON_INCLUDES_
#define q_2xamd1_stII_q8_usb_COMMON_INCLUDES_
#include <math.h>
#include "rtwtypes.h"
#include "zero_crossing_types.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "dt_info.h"
#include "ext_work.h"
#include "hil.h"
#include "quanser_messages.h"
#include "quanser_sigmoid.h"
#include "math.h"
#include "quanser_extern.h"
#endif                               /* q_2xamd1_stII_q8_usb_COMMON_INCLUDES_ */

#include "q_2xamd1_stII_q8_usb_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"
#include "rt_defines.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlockIO
#define rtmGetBlockIO(rtm)             ((rtm)->blockIO)
#endif

#ifndef rtmSetBlockIO
#define rtmSetBlockIO(rtm, val)        ((rtm)->blockIO = (val))
#endif

#ifndef rtmGetChecksums
#define rtmGetChecksums(rtm)           ((rtm)->Sizes.checksums)
#endif

#ifndef rtmSetChecksums
#define rtmSetChecksums(rtm, val)      ((rtm)->Sizes.checksums = (val))
#endif

#ifndef rtmGetConstBlockIO
#define rtmGetConstBlockIO(rtm)        ((rtm)->constBlockIO)
#endif

#ifndef rtmSetConstBlockIO
#define rtmSetConstBlockIO(rtm, val)   ((rtm)->constBlockIO = (val))
#endif

#ifndef rtmGetContStateDisabled
#define rtmGetContStateDisabled(rtm)   ((rtm)->contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
#define rtmSetContStateDisabled(rtm, val) ((rtm)->contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
#define rtmGetContStates(rtm)          ((rtm)->contStates)
#endif

#ifndef rtmSetContStates
#define rtmSetContStates(rtm, val)     ((rtm)->contStates = (val))
#endif

#ifndef rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag
#define rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm) ((rtm)->CTOutputIncnstWithState)
#endif

#ifndef rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag
#define rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm, val) ((rtm)->CTOutputIncnstWithState = (val))
#endif

#ifndef rtmGetCtrlRateMdlRefTiming
#define rtmGetCtrlRateMdlRefTiming(rtm) ()
#endif

#ifndef rtmSetCtrlRateMdlRefTiming
#define rtmSetCtrlRateMdlRefTiming(rtm, val) ()
#endif

#ifndef rtmGetCtrlRateMdlRefTimingPtr
#define rtmGetCtrlRateMdlRefTimingPtr(rtm) ()
#endif

#ifndef rtmSetCtrlRateMdlRefTimingPtr
#define rtmSetCtrlRateMdlRefTimingPtr(rtm, val) ()
#endif

#ifndef rtmGetCtrlRateNumTicksToNextHit
#define rtmGetCtrlRateNumTicksToNextHit(rtm) ()
#endif

#ifndef rtmSetCtrlRateNumTicksToNextHit
#define rtmSetCtrlRateNumTicksToNextHit(rtm, val) ()
#endif

#ifndef rtmGetDataMapInfo
#define rtmGetDataMapInfo(rtm)         ()
#endif

#ifndef rtmSetDataMapInfo
#define rtmSetDataMapInfo(rtm, val)    ()
#endif

#ifndef rtmGetDefaultParam
#define rtmGetDefaultParam(rtm)        ((rtm)->defaultParam)
#endif

#ifndef rtmSetDefaultParam
#define rtmSetDefaultParam(rtm, val)   ((rtm)->defaultParam = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
#define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
#define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetDirectFeedThrough
#define rtmGetDirectFeedThrough(rtm)   ((rtm)->Sizes.sysDirFeedThru)
#endif

#ifndef rtmSetDirectFeedThrough
#define rtmSetDirectFeedThrough(rtm, val) ((rtm)->Sizes.sysDirFeedThru = (val))
#endif

#ifndef rtmGetErrorStatusFlag
#define rtmGetErrorStatusFlag(rtm)     ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatusFlag
#define rtmSetErrorStatusFlag(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetFinalTime
#define rtmGetFinalTime(rtm)           ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetFinalTime
#define rtmSetFinalTime(rtm, val)      ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetFirstInitCondFlag
#define rtmGetFirstInitCondFlag(rtm)   ((rtm)->Timing.firstInitCondFlag)
#endif

#ifndef rtmSetFirstInitCondFlag
#define rtmSetFirstInitCondFlag(rtm, val) ((rtm)->Timing.firstInitCondFlag = (val))
#endif

#ifndef rtmGetIntgData
#define rtmGetIntgData(rtm)            ((rtm)->intgData)
#endif

#ifndef rtmSetIntgData
#define rtmSetIntgData(rtm, val)       ((rtm)->intgData = (val))
#endif

#ifndef rtmGetMdlRefGlobalRuntimeEventIndices
#define rtmGetMdlRefGlobalRuntimeEventIndices(rtm) ()
#endif

#ifndef rtmSetMdlRefGlobalRuntimeEventIndices
#define rtmSetMdlRefGlobalRuntimeEventIndices(rtm, val) ()
#endif

#ifndef rtmGetMdlRefGlobalTID
#define rtmGetMdlRefGlobalTID(rtm)     ()
#endif

#ifndef rtmSetMdlRefGlobalTID
#define rtmSetMdlRefGlobalTID(rtm, val) ()
#endif

#ifndef rtmGetMdlRefTriggerTID
#define rtmGetMdlRefTriggerTID(rtm)    ()
#endif

#ifndef rtmSetMdlRefTriggerTID
#define rtmSetMdlRefTriggerTID(rtm, val) ()
#endif

#ifndef rtmGetModelMappingInfo
#define rtmGetModelMappingInfo(rtm)    ((rtm)->SpecialInfo.mappingInfo)
#endif

#ifndef rtmSetModelMappingInfo
#define rtmSetModelMappingInfo(rtm, val) ((rtm)->SpecialInfo.mappingInfo = (val))
#endif

#ifndef rtmGetModelName
#define rtmGetModelName(rtm)           ((rtm)->modelName)
#endif

#ifndef rtmSetModelName
#define rtmSetModelName(rtm, val)      ((rtm)->modelName = (val))
#endif

#ifndef rtmGetNonInlinedSFcns
#define rtmGetNonInlinedSFcns(rtm)     ()
#endif

#ifndef rtmSetNonInlinedSFcns
#define rtmSetNonInlinedSFcns(rtm, val) ()
#endif

#ifndef rtmGetNumBlockIO
#define rtmGetNumBlockIO(rtm)          ((rtm)->Sizes.numBlockIO)
#endif

#ifndef rtmSetNumBlockIO
#define rtmSetNumBlockIO(rtm, val)     ((rtm)->Sizes.numBlockIO = (val))
#endif

#ifndef rtmGetNumBlockParams
#define rtmGetNumBlockParams(rtm)      ((rtm)->Sizes.numBlockPrms)
#endif

#ifndef rtmSetNumBlockParams
#define rtmSetNumBlockParams(rtm, val) ((rtm)->Sizes.numBlockPrms = (val))
#endif

#ifndef rtmGetNumBlocks
#define rtmGetNumBlocks(rtm)           ((rtm)->Sizes.numBlocks)
#endif

#ifndef rtmSetNumBlocks
#define rtmSetNumBlocks(rtm, val)      ((rtm)->Sizes.numBlocks = (val))
#endif

#ifndef rtmGetNumContStates
#define rtmGetNumContStates(rtm)       ((rtm)->Sizes.numContStates)
#endif

#ifndef rtmSetNumContStates
#define rtmSetNumContStates(rtm, val)  ((rtm)->Sizes.numContStates = (val))
#endif

#ifndef rtmGetNumDWork
#define rtmGetNumDWork(rtm)            ((rtm)->Sizes.numDwork)
#endif

#ifndef rtmSetNumDWork
#define rtmSetNumDWork(rtm, val)       ((rtm)->Sizes.numDwork = (val))
#endif

#ifndef rtmGetNumInputPorts
#define rtmGetNumInputPorts(rtm)       ((rtm)->Sizes.numIports)
#endif

#ifndef rtmSetNumInputPorts
#define rtmSetNumInputPorts(rtm, val)  ((rtm)->Sizes.numIports = (val))
#endif

#ifndef rtmGetNumNonSampledZCs
#define rtmGetNumNonSampledZCs(rtm)    ((rtm)->Sizes.numNonSampZCs)
#endif

#ifndef rtmSetNumNonSampledZCs
#define rtmSetNumNonSampledZCs(rtm, val) ((rtm)->Sizes.numNonSampZCs = (val))
#endif

#ifndef rtmGetNumOutputPorts
#define rtmGetNumOutputPorts(rtm)      ((rtm)->Sizes.numOports)
#endif

#ifndef rtmSetNumOutputPorts
#define rtmSetNumOutputPorts(rtm, val) ((rtm)->Sizes.numOports = (val))
#endif

#ifndef rtmGetNumPeriodicContStates
#define rtmGetNumPeriodicContStates(rtm) ((rtm)->Sizes.numPeriodicContStates)
#endif

#ifndef rtmSetNumPeriodicContStates
#define rtmSetNumPeriodicContStates(rtm, val) ((rtm)->Sizes.numPeriodicContStates = (val))
#endif

#ifndef rtmGetNumSFcnParams
#define rtmGetNumSFcnParams(rtm)       ((rtm)->Sizes.numSFcnPrms)
#endif

#ifndef rtmSetNumSFcnParams
#define rtmSetNumSFcnParams(rtm, val)  ((rtm)->Sizes.numSFcnPrms = (val))
#endif

#ifndef rtmGetNumSFunctions
#define rtmGetNumSFunctions(rtm)       ((rtm)->Sizes.numSFcns)
#endif

#ifndef rtmSetNumSFunctions
#define rtmSetNumSFunctions(rtm, val)  ((rtm)->Sizes.numSFcns = (val))
#endif

#ifndef rtmGetNumSampleTimes
#define rtmGetNumSampleTimes(rtm)      ((rtm)->Sizes.numSampTimes)
#endif

#ifndef rtmSetNumSampleTimes
#define rtmSetNumSampleTimes(rtm, val) ((rtm)->Sizes.numSampTimes = (val))
#endif

#ifndef rtmGetNumU
#define rtmGetNumU(rtm)                ((rtm)->Sizes.numU)
#endif

#ifndef rtmSetNumU
#define rtmSetNumU(rtm, val)           ((rtm)->Sizes.numU = (val))
#endif

#ifndef rtmGetNumY
#define rtmGetNumY(rtm)                ((rtm)->Sizes.numY)
#endif

#ifndef rtmSetNumY
#define rtmSetNumY(rtm, val)           ((rtm)->Sizes.numY = (val))
#endif

#ifndef rtmGetOdeF
#define rtmGetOdeF(rtm)                ((rtm)->odeF)
#endif

#ifndef rtmSetOdeF
#define rtmSetOdeF(rtm, val)           ((rtm)->odeF = (val))
#endif

#ifndef rtmGetOdeY
#define rtmGetOdeY(rtm)                ((rtm)->odeY)
#endif

#ifndef rtmSetOdeY
#define rtmSetOdeY(rtm, val)           ((rtm)->odeY = (val))
#endif

#ifndef rtmGetOffsetTimeArray
#define rtmGetOffsetTimeArray(rtm)     ((rtm)->Timing.offsetTimesArray)
#endif

#ifndef rtmSetOffsetTimeArray
#define rtmSetOffsetTimeArray(rtm, val) ((rtm)->Timing.offsetTimesArray = (val))
#endif

#ifndef rtmGetOffsetTimePtr
#define rtmGetOffsetTimePtr(rtm)       ((rtm)->Timing.offsetTimes)
#endif

#ifndef rtmSetOffsetTimePtr
#define rtmSetOffsetTimePtr(rtm, val)  ((rtm)->Timing.offsetTimes = (val))
#endif

#ifndef rtmGetOptions
#define rtmGetOptions(rtm)             ((rtm)->Sizes.options)
#endif

#ifndef rtmSetOptions
#define rtmSetOptions(rtm, val)        ((rtm)->Sizes.options = (val))
#endif

#ifndef rtmGetParamIsMalloced
#define rtmGetParamIsMalloced(rtm)     ()
#endif

#ifndef rtmSetParamIsMalloced
#define rtmSetParamIsMalloced(rtm, val) ()
#endif

#ifndef rtmGetPath
#define rtmGetPath(rtm)                ((rtm)->path)
#endif

#ifndef rtmSetPath
#define rtmSetPath(rtm, val)           ((rtm)->path = (val))
#endif

#ifndef rtmGetPerTaskSampleHits
#define rtmGetPerTaskSampleHits(rtm)   ()
#endif

#ifndef rtmSetPerTaskSampleHits
#define rtmSetPerTaskSampleHits(rtm, val) ()
#endif

#ifndef rtmGetPerTaskSampleHitsArray
#define rtmGetPerTaskSampleHitsArray(rtm) ((rtm)->Timing.perTaskSampleHitsArray)
#endif

#ifndef rtmSetPerTaskSampleHitsArray
#define rtmSetPerTaskSampleHitsArray(rtm, val) ((rtm)->Timing.perTaskSampleHitsArray = (val))
#endif

#ifndef rtmGetPerTaskSampleHitsPtr
#define rtmGetPerTaskSampleHitsPtr(rtm) ((rtm)->Timing.perTaskSampleHits)
#endif

#ifndef rtmSetPerTaskSampleHitsPtr
#define rtmSetPerTaskSampleHitsPtr(rtm, val) ((rtm)->Timing.perTaskSampleHits = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
#define rtmGetPeriodicContStateIndices(rtm) ((rtm)->periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
#define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
#define rtmGetPeriodicContStateRanges(rtm) ((rtm)->periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
#define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->periodicContStateRanges = (val))
#endif

#ifndef rtmGetPrevZCSigState
#define rtmGetPrevZCSigState(rtm)      ((rtm)->prevZCSigState)
#endif

#ifndef rtmSetPrevZCSigState
#define rtmSetPrevZCSigState(rtm, val) ((rtm)->prevZCSigState = (val))
#endif

#ifndef rtmGetRTWExtModeInfo
#define rtmGetRTWExtModeInfo(rtm)      ((rtm)->extModeInfo)
#endif

#ifndef rtmSetRTWExtModeInfo
#define rtmSetRTWExtModeInfo(rtm, val) ((rtm)->extModeInfo = (val))
#endif

#ifndef rtmGetRTWGeneratedSFcn
#define rtmGetRTWGeneratedSFcn(rtm)    ((rtm)->Sizes.rtwGenSfcn)
#endif

#ifndef rtmSetRTWGeneratedSFcn
#define rtmSetRTWGeneratedSFcn(rtm, val) ((rtm)->Sizes.rtwGenSfcn = (val))
#endif

#ifndef rtmGetRTWLogInfo
#define rtmGetRTWLogInfo(rtm)          ()
#endif

#ifndef rtmSetRTWLogInfo
#define rtmSetRTWLogInfo(rtm, val)     ()
#endif

#ifndef rtmGetRTWRTModelMethodsInfo
#define rtmGetRTWRTModelMethodsInfo(rtm) ()
#endif

#ifndef rtmSetRTWRTModelMethodsInfo
#define rtmSetRTWRTModelMethodsInfo(rtm, val) ()
#endif

#ifndef rtmGetRTWSfcnInfo
#define rtmGetRTWSfcnInfo(rtm)         ((rtm)->sfcnInfo)
#endif

#ifndef rtmSetRTWSfcnInfo
#define rtmSetRTWSfcnInfo(rtm, val)    ((rtm)->sfcnInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfo
#define rtmGetRTWSolverInfo(rtm)       ((rtm)->solverInfo)
#endif

#ifndef rtmSetRTWSolverInfo
#define rtmSetRTWSolverInfo(rtm, val)  ((rtm)->solverInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfoPtr
#define rtmGetRTWSolverInfoPtr(rtm)    ((rtm)->solverInfoPtr)
#endif

#ifndef rtmSetRTWSolverInfoPtr
#define rtmSetRTWSolverInfoPtr(rtm, val) ((rtm)->solverInfoPtr = (val))
#endif

#ifndef rtmGetReservedForXPC
#define rtmGetReservedForXPC(rtm)      ((rtm)->SpecialInfo.xpcData)
#endif

#ifndef rtmSetReservedForXPC
#define rtmSetReservedForXPC(rtm, val) ((rtm)->SpecialInfo.xpcData = (val))
#endif

#ifndef rtmGetRootDWork
#define rtmGetRootDWork(rtm)           ((rtm)->dwork)
#endif

#ifndef rtmSetRootDWork
#define rtmSetRootDWork(rtm, val)      ((rtm)->dwork = (val))
#endif

#ifndef rtmGetSFunctions
#define rtmGetSFunctions(rtm)          ((rtm)->childSfunctions)
#endif

#ifndef rtmSetSFunctions
#define rtmSetSFunctions(rtm, val)     ((rtm)->childSfunctions = (val))
#endif

#ifndef rtmGetSampleHitArray
#define rtmGetSampleHitArray(rtm)      ((rtm)->Timing.sampleHitArray)
#endif

#ifndef rtmSetSampleHitArray
#define rtmSetSampleHitArray(rtm, val) ((rtm)->Timing.sampleHitArray = (val))
#endif

#ifndef rtmGetSampleHitPtr
#define rtmGetSampleHitPtr(rtm)        ((rtm)->Timing.sampleHits)
#endif

#ifndef rtmSetSampleHitPtr
#define rtmSetSampleHitPtr(rtm, val)   ((rtm)->Timing.sampleHits = (val))
#endif

#ifndef rtmGetSampleTimeArray
#define rtmGetSampleTimeArray(rtm)     ((rtm)->Timing.sampleTimesArray)
#endif

#ifndef rtmSetSampleTimeArray
#define rtmSetSampleTimeArray(rtm, val) ((rtm)->Timing.sampleTimesArray = (val))
#endif

#ifndef rtmGetSampleTimePtr
#define rtmGetSampleTimePtr(rtm)       ((rtm)->Timing.sampleTimes)
#endif

#ifndef rtmSetSampleTimePtr
#define rtmSetSampleTimePtr(rtm, val)  ((rtm)->Timing.sampleTimes = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDArray
#define rtmGetSampleTimeTaskIDArray(rtm) ((rtm)->Timing.sampleTimeTaskIDArray)
#endif

#ifndef rtmSetSampleTimeTaskIDArray
#define rtmSetSampleTimeTaskIDArray(rtm, val) ((rtm)->Timing.sampleTimeTaskIDArray = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDPtr
#define rtmGetSampleTimeTaskIDPtr(rtm) ((rtm)->Timing.sampleTimeTaskIDPtr)
#endif

#ifndef rtmSetSampleTimeTaskIDPtr
#define rtmSetSampleTimeTaskIDPtr(rtm, val) ((rtm)->Timing.sampleTimeTaskIDPtr = (val))
#endif

#ifndef rtmGetSelf
#define rtmGetSelf(rtm)                ()
#endif

#ifndef rtmSetSelf
#define rtmSetSelf(rtm, val)           ()
#endif

#ifndef rtmGetSimMode
#define rtmGetSimMode(rtm)             ((rtm)->simMode)
#endif

#ifndef rtmSetSimMode
#define rtmSetSimMode(rtm, val)        ((rtm)->simMode = (val))
#endif

#ifndef rtmGetSimTimeStep
#define rtmGetSimTimeStep(rtm)         ((rtm)->Timing.simTimeStep)
#endif

#ifndef rtmSetSimTimeStep
#define rtmSetSimTimeStep(rtm, val)    ((rtm)->Timing.simTimeStep = (val))
#endif

#ifndef rtmGetStartTime
#define rtmGetStartTime(rtm)           ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetStartTime
#define rtmSetStartTime(rtm, val)      ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetStepSize
#define rtmGetStepSize(rtm)            ((rtm)->Timing.stepSize)
#endif

#ifndef rtmSetStepSize
#define rtmSetStepSize(rtm, val)       ((rtm)->Timing.stepSize = (val))
#endif

#ifndef rtmGetStopRequestedFlag
#define rtmGetStopRequestedFlag(rtm)   ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequestedFlag
#define rtmSetStopRequestedFlag(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetTaskCounters
#define rtmGetTaskCounters(rtm)        ()
#endif

#ifndef rtmSetTaskCounters
#define rtmSetTaskCounters(rtm, val)   ()
#endif

#ifndef rtmGetTaskTimeArray
#define rtmGetTaskTimeArray(rtm)       ((rtm)->Timing.tArray)
#endif

#ifndef rtmSetTaskTimeArray
#define rtmSetTaskTimeArray(rtm, val)  ((rtm)->Timing.tArray = (val))
#endif

#ifndef rtmGetTimePtr
#define rtmGetTimePtr(rtm)             ((rtm)->Timing.t)
#endif

#ifndef rtmSetTimePtr
#define rtmSetTimePtr(rtm, val)        ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTimingData
#define rtmGetTimingData(rtm)          ((rtm)->Timing.timingData)
#endif

#ifndef rtmSetTimingData
#define rtmSetTimingData(rtm, val)     ((rtm)->Timing.timingData = (val))
#endif

#ifndef rtmGetU
#define rtmGetU(rtm)                   ((rtm)->inputs)
#endif

#ifndef rtmSetU
#define rtmSetU(rtm, val)              ((rtm)->inputs = (val))
#endif

#ifndef rtmGetVarNextHitTimesListPtr
#define rtmGetVarNextHitTimesListPtr(rtm) ((rtm)->Timing.varNextHitTimesList)
#endif

#ifndef rtmSetVarNextHitTimesListPtr
#define rtmSetVarNextHitTimesListPtr(rtm, val) ((rtm)->Timing.varNextHitTimesList = (val))
#endif

#ifndef rtmGetY
#define rtmGetY(rtm)                   ((rtm)->outputs)
#endif

#ifndef rtmSetY
#define rtmSetY(rtm, val)              ((rtm)->outputs = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
#define rtmGetZCCacheNeedsReset(rtm)   ((rtm)->zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
#define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetZCSignalValues
#define rtmGetZCSignalValues(rtm)      ((rtm)->zcSignalValues)
#endif

#ifndef rtmSetZCSignalValues
#define rtmSetZCSignalValues(rtm, val) ((rtm)->zcSignalValues = (val))
#endif

#ifndef rtmGet_TimeOfLastOutput
#define rtmGet_TimeOfLastOutput(rtm)   ((rtm)->Timing.timeOfLastOutput)
#endif

#ifndef rtmSet_TimeOfLastOutput
#define rtmSet_TimeOfLastOutput(rtm, val) ((rtm)->Timing.timeOfLastOutput = (val))
#endif

#ifndef rtmGetdX
#define rtmGetdX(rtm)                  ((rtm)->derivs)
#endif

#ifndef rtmSetdX
#define rtmSetdX(rtm, val)             ((rtm)->derivs = (val))
#endif

#ifndef rtmGettimingBridge
#define rtmGettimingBridge(rtm)        ()
#endif

#ifndef rtmSettimingBridge
#define rtmSettimingBridge(rtm, val)   ()
#endif

#ifndef rtmGetChecksumVal
#define rtmGetChecksumVal(rtm, idx)    ((rtm)->Sizes.checksums[idx])
#endif

#ifndef rtmSetChecksumVal
#define rtmSetChecksumVal(rtm, idx, val) ((rtm)->Sizes.checksums[idx] = (val))
#endif

#ifndef rtmGetDWork
#define rtmGetDWork(rtm, idx)          ((rtm)->dwork[idx])
#endif

#ifndef rtmSetDWork
#define rtmSetDWork(rtm, idx, val)     ((rtm)->dwork[idx] = (val))
#endif

#ifndef rtmGetOffsetTime
#define rtmGetOffsetTime(rtm, idx)     ((rtm)->Timing.offsetTimes[idx])
#endif

#ifndef rtmSetOffsetTime
#define rtmSetOffsetTime(rtm, idx, val) ((rtm)->Timing.offsetTimes[idx] = (val))
#endif

#ifndef rtmGetSFunction
#define rtmGetSFunction(rtm, idx)      ((rtm)->childSfunctions[idx])
#endif

#ifndef rtmSetSFunction
#define rtmSetSFunction(rtm, idx, val) ((rtm)->childSfunctions[idx] = (val))
#endif

#ifndef rtmGetSampleTime
#define rtmGetSampleTime(rtm, idx)     ((rtm)->Timing.sampleTimes[idx])
#endif

#ifndef rtmSetSampleTime
#define rtmSetSampleTime(rtm, idx, val) ((rtm)->Timing.sampleTimes[idx] = (val))
#endif

#ifndef rtmGetSampleTimeTaskID
#define rtmGetSampleTimeTaskID(rtm, idx) ((rtm)->Timing.sampleTimeTaskIDPtr[idx])
#endif

#ifndef rtmSetSampleTimeTaskID
#define rtmSetSampleTimeTaskID(rtm, idx, val) ((rtm)->Timing.sampleTimeTaskIDPtr[idx] = (val))
#endif

#ifndef rtmGetVarNextHitTimeList
#define rtmGetVarNextHitTimeList(rtm, idx) ((rtm)->Timing.varNextHitTimesList[idx])
#endif

#ifndef rtmSetVarNextHitTimeList
#define rtmSetVarNextHitTimeList(rtm, idx, val) ((rtm)->Timing.varNextHitTimesList[idx] = (val))
#endif

#ifndef rtmIsContinuousTask
#define rtmIsContinuousTask(rtm, tid)  ((tid) == 0)
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmSetFirstInitCond
#define rtmSetFirstInitCond(rtm, val)  ((rtm)->Timing.firstInitCondFlag = (val))
#endif

#ifndef rtmIsFirstInitCond
#define rtmIsFirstInitCond(rtm)        ((rtm)->Timing.firstInitCondFlag)
#endif

#ifndef rtmIsMajorTimeStep
#define rtmIsMajorTimeStep(rtm)        (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
#define rtmIsMinorTimeStep(rtm)        (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmIsSampleHit
#define rtmIsSampleHit(rtm, sti, tid)  ((rtmIsMajorTimeStep((rtm)) && (rtm)->Timing.sampleHits[(rtm)->Timing.sampleTimeTaskIDPtr[sti]]))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmSetT
#define rtmSetT(rtm, val)                                        /* Do Nothing */
#endif

#ifndef rtmGetTFinal
#define rtmGetTFinal(rtm)              ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetTFinal
#define rtmSetTFinal(rtm, val)         ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
#define rtmSetTPtr(rtm, val)           ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTStart
#define rtmGetTStart(rtm)              ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetTStart
#define rtmSetTStart(rtm, val)         ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetTaskTime
#define rtmGetTaskTime(rtm, sti)       (rtmGetTPtr((rtm))[(rtm)->Timing.sampleTimeTaskIDPtr[sti]])
#endif

#ifndef rtmSetTaskTime
#define rtmSetTaskTime(rtm, sti, val)  (rtmGetTPtr((rtm))[sti] = (val))
#endif

#ifndef rtmGetTimeOfLastOutput
#define rtmGetTimeOfLastOutput(rtm)    ((rtm)->Timing.timeOfLastOutput)
#endif

#ifdef rtmGetRTWSolverInfo
#undef rtmGetRTWSolverInfo
#endif

#define rtmGetRTWSolverInfo(rtm)       &((rtm)->solverInfo)

/* Definition for use in the target main file */
#define q_2xamd1_stII_q8_usb_rtModel   RT_MODEL_q_2xamd1_stII_q8_usb_T

/* Block signals (default storage) */
typedef struct {
  real_T HILReadTimebase_o1[3];        /* '<S7>/HIL Read Timebase' */
  real_T DataTypeConversion5;          /* '<S43>/Data Type Conversion5' */
  real_T DataTypeConversion2;          /* '<S43>/Data Type Conversion2' */
  real_T DataTypeConversion3;          /* '<S43>/Data Type Conversion3' */
  real_T DataTypeConversion5_h;        /* '<S44>/Data Type Conversion5' */
  real_T DataTypeConversion2_b;        /* '<S44>/Data Type Conversion2' */
  real_T DataTypeConversion3_a;        /* '<S44>/Data Type Conversion3' */
  real_T ContinuousSigmoid_o1;         /* '<S10>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_o2;         /* '<S10>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_o3;         /* '<S10>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_o4;         /* '<S10>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_o1_g;       /* '<S9>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_o2_m;       /* '<S9>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_o3_b;       /* '<S9>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_o4_k;       /* '<S9>/Continuous Sigmoid' */
  real_T SetpointSwitch[2];            /* '<Root>/Setpoint Switch' */
  real_T EncoderCalibrationmcount;  /* '<S49>/Encoder Calibration (m//count)' */
  real_T ProportionalGainVm;           /* '<S47>/Proportional Gain (V//m)' */
  real_T x0;                           /* '<S51>/x0' */
  real_T Add;                          /* '<S47>/Add' */
  real_T VoltageLimit;                 /* '<S48>/Voltage Limit' */
  real_T AMDControlMode;               /* '<Root>/AMD Control Mode' */
  real_T Integrator[8];                /* '<S63>/Integrator' */
  real_T AmplifierVoltageSaturation[2];
                                     /* '<Root>/Amplifier Voltage Saturation' */
  real_T AmplifierGainPreCompensation[2];
                                    /* '<S1>/Amplifier Gain Pre-Compensation' */
  real_T Conversiontom1;               /* '<S1>/Conversion to m1' */
  real_T Conversiontom2;               /* '<S1>/Conversion to m2' */
  real_T x0_f;                         /* '<S16>/x0' */
  real_T floor1gtoms2;                 /* '<S1>/floor 1: g to m//s^2' */
  real_T floor1gtoms1;                 /* '<S1>/floor 1: g to m//s^1' */
  real_T ControlMode[2];               /* '<S11>/Control Mode' */
  real_T mtocm[2];                     /* '<Root>/m to cm' */
  real_T x0_o;                         /* '<S52>/x0' */
  real_T ms2tog;                       /* '<S49>/m//s^2 to g' */
  real_T mtomm;                        /* '<Root>/m to mm' */
  real_T mtomm1;                       /* '<Root>/m to mm1' */
  real_T Product[3];                   /* '<S16>/Product' */
  real_T Product1[3];                  /* '<S16>/Product1' */
  real_T Kp_c2741[2];                  /* '<S5>/Kp_c: 274.1' */
  real_T Kp_c2741_a[2];                /* '<S6>/Kp_c: 274.1' */
  real_T Abs1;                         /* '<S50>/Abs1' */
  real_T DiscreteTimeIntegrator;       /* '<S54>/Discrete-Time Integrator' */
  real_T DataTypeConversion;           /* '<S54>/Data Type Conversion' */
  real_T countsstoms;                  /* '<S49>/counts//s to m//s' */
  real_T Product_m;                    /* '<S51>/Product' */
  real_T Product1_g;                   /* '<S51>/Product1' */
  real_T Product_k;                    /* '<S52>/Product' */
  real_T Product1_f;                   /* '<S52>/Product1' */
  real_T DataTypeConversion_b;         /* '<S57>/Data Type Conversion' */
  real_T DiscreteTransferFcn;          /* '<S59>/Discrete Transfer Fcn' */
  real_T SweepPositionLimitm;          /* '<S9>/Sweep Position  Limit (m)' */
  real_T cmtom;                        /* '<S10>/cm to m' */
  real_T PositionLimit;                /* '<S10>/Position Limit' */
  real_T TmpSignalConversionAtStateSpace[4];
  real_T Sum3[8];                      /* '<S63>/Sum3' */
  real_T Gain;                         /* '<S60>/Gain' */
  real_T Product_e;                    /* '<S60>/Product' */
  real_T Switch[3];                    /* '<S15>/Switch' */
  real_T VVavg[3];                     /* '<S20>/V-Vavg' */
  real_T Vin[3];                       /* '<S19>/Vin' */
  real_T Constant;                     /* '<S18>/Constant' */
  real_T Count;                        /* '<S21>/Count' */
  real_T Sum[3];                       /* '<S17>/Sum' */
  real_T div[3];                       /* '<S17>/div' */
  boolean_T HILReadTimebase_o3[5];     /* '<S7>/HIL Read Timebase' */
  boolean_T debouncer_block;           /* '<S43>/debouncer_block' */
  boolean_T debouncer_block_p;         /* '<S44>/debouncer_block' */
  boolean_T RelationalOperator;        /* '<S54>/Relational Operator' */
  boolean_T LogicalOperator1;          /* '<S54>/Logical Operator1' */
  boolean_T RelationalOperator2;       /* '<S54>/Relational Operator2' */
  boolean_T Compare;                   /* '<S61>/Compare' */
  boolean_T HiddenBuf_InsertedFor_RampSigna;/* '<S10>/Start at 1 sec' */
  boolean_T LogicalOperator;           /* '<S32>/Logical Operator' */
  boolean_T CheckMaximumAllowedPosition;
                                    /* '<S14>/Check Maximum Allowed Position' */
  boolean_T CheckMinimumAllowedPosition;
                                    /* '<S14>/Check Minimum Allowed Position' */
  boolean_T CheckMaximumAllowedPosition_g;
                                    /* '<S13>/Check Maximum Allowed Position' */
  boolean_T CheckMinimumAllowedPosition_g;
                                    /* '<S13>/Check Minimum Allowed Position' */
} B_q_2xamd1_stII_q8_usb_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T debouncer_block_DSTATE[3];    /* '<S43>/debouncer_block' */
  real_T debouncer_block_DSTATE_a[3];  /* '<S44>/debouncer_block' */
  real_T DiscreteTimeIntegrator_DSTATE;/* '<S54>/Discrete-Time Integrator' */
  real_T DiscreteTransferFcn_states;   /* '<S59>/Discrete Transfer Fcn' */
  real_T UnitDelay_DSTATE;             /* '<S21>/Unit Delay' */
  real_T Sumk1n1xk_DSTATE[3];          /* '<S17>/Sum( k=1,n-1, x(k) )' */
  real_T HILInitialize_AIMinimums[8];  /* '<S7>/HIL Initialize' */
  real_T HILInitialize_AIMaximums[8];  /* '<S7>/HIL Initialize' */
  real_T HILInitialize_AOMinimums[8];  /* '<S7>/HIL Initialize' */
  real_T HILInitialize_AOMaximums[8];  /* '<S7>/HIL Initialize' */
  real_T HILInitialize_AOVoltages[8];  /* '<S7>/HIL Initialize' */
  real_T HILInitialize_FilterFrequency[8];/* '<S7>/HIL Initialize' */
  real_T HILInitialize_POSortedFreqs[8];/* '<S7>/HIL Initialize' */
  real_T HILInitialize_POValues[8];    /* '<S7>/HIL Initialize' */
  real_T ContinuousSigmoid_Sigmoid[11];/* '<S10>/Continuous Sigmoid' */
  real_T ContinuousSigmoid_Sigmoid_l[11];/* '<S9>/Continuous Sigmoid' */
  real_T HILWrite_AnalogBuffer[3];     /* '<S7>/HIL Write' */
  real_T TimeStampA;                   /* '<S49>/Derivative' */
  real_T LastUAtTimeA;                 /* '<S49>/Derivative' */
  real_T TimeStampB;                   /* '<S49>/Derivative' */
  real_T LastUAtTimeB;                 /* '<S49>/Derivative' */
  real_T DiscreteTransferFcn_tmp;      /* '<S59>/Discrete Transfer Fcn' */
  t_uint64 ToHostFile_PointsWritten;   /* '<Root>/To Host File' */
  t_card HILInitialize_Card;           /* '<S7>/HIL Initialize' */
  t_task HILReadTimebase_Task;         /* '<S7>/HIL Read Timebase' */
  struct {
    real_T Time;
    real_T Target;
    real_T PPos;
    real_T PVel;
    real_T MVel;
    real_T MAcc;
  } ContinuousSigmoid_RWORK;           /* '<S10>/Continuous Sigmoid' */

  struct {
    real_T Time;
    real_T Target;
    real_T PPos;
    real_T PVel;
    real_T MVel;
    real_T MAcc;
  } ContinuousSigmoid_RWORK_k;         /* '<S9>/Continuous Sigmoid' */

  struct {
    real_T Mean;
  } VoltageLimit_RWORK;                /* '<S48>/Voltage Limit' */

  struct {
    real_T Amp;
    real_T W;
    real_T Sin_w_dt;
    real_T Cos_w_dt;
    real_T Sin_w_t;
    real_T Cos_w_t;
  } SmoothSignalGenerator_RWORK;       /* '<S10>/Smooth Signal Generator' */

  void *HILWrite_PWORK;                /* '<S7>/HIL Write' */
  void *HILWatchdog_PWORK;             /* '<S35>/HIL Watchdog' */
  struct {
    void *LoggedData;
  } Scope_PWORK;                       /* '<Root>/Scope' */

  struct {
    void *LoggedData;
  } Scope1_PWORK;                      /* '<Root>/Scope1' */

  struct {
    void *LoggedData;
  } TableMotorVoltageV_PWORK;          /* '<Root>/Table Motor Voltage (V)' */

  struct {
    void *LoggedData;
  } TablePositioncm_PWORK;             /* '<Root>/Table Position (cm)' */

  void *ToHostFile_PWORK[2];           /* '<Root>/To Host File' */
  struct {
    void *LoggedData;
  } VmV_PWORK;                         /* '<Root>/Vm (V)' */

  struct {
    void *LoggedData;
  } x_ddotg_PWORK;                     /* '<Root>/x_ddot (g)' */

  struct {
    void *LoggedData;
  } xf1mm_PWORK;                       /* '<Root>/xf1 (mm)' */

  struct {
    void *LoggedData;
  } xf1_ddotms2_PWORK;                 /* '<Root>/xf1_ddot (m//s^2)' */

  struct {
    void *LoggedData;
  } xf2mm_PWORK;                       /* '<Root>/xf2 (mm)' */

  struct {
    void *LoggedData;
  } xf2_ddotms2_PWORK;                 /* '<Root>/xf2_ddot (m//s^2)' */

  struct {
    void *LoggedData;
  } PVControlxc1mm_PWORK;              /* '<S5>/PV Control: xc1 (mm)' */

  struct {
    void *LoggedData;
  } PVControlxc2mm_PWORK;              /* '<S6>/PV Control: xc2 (mm)' */

  int32_T HILInitialize_ClockModes[3]; /* '<S7>/HIL Initialize' */
  int32_T HILInitialize_DOStates[8];   /* '<S7>/HIL Initialize' */
  int32_T HILInitialize_QuadratureModes[8];/* '<S7>/HIL Initialize' */
  int32_T HILInitialize_InitialEICounts[8];/* '<S7>/HIL Initialize' */
  int32_T HILInitialize_POModeValues[8];/* '<S7>/HIL Initialize' */
  int32_T HILInitialize_POAlignValues[8];/* '<S7>/HIL Initialize' */
  int32_T HILInitialize_POPolarityVals[8];/* '<S7>/HIL Initialize' */
  int32_T HILReadTimebase_EncoderBuffer[3];/* '<S7>/HIL Read Timebase' */
  uint32_T HILInitialize_POSortedChans[8];/* '<S7>/HIL Initialize' */
  uint32_T ToHostFile_SamplesCount;    /* '<Root>/To Host File' */
  uint32_T ToHostFile_ArrayNameLength; /* '<Root>/To Host File' */
  int_T Integrator1_IWORK;             /* '<S51>/Integrator1' */
  struct {
    int_T State;
    int_T Count;
  } VoltageLimit_IWORK;                /* '<S48>/Voltage Limit' */

  int_T Integrator1_IWORK_i;           /* '<S16>/Integrator1' */
  int_T Integrator1_IWORK_p;           /* '<S52>/Integrator1' */
  int8_T DiscreteTimeIntegrator_PrevRese;/* '<S54>/Discrete-Time Integrator' */
  int8_T RampSignal_SubsysRanBC;       /* '<S10>/Ramp Signal' */
  int8_T Checkifstageathome_SubsysRanBC;/* '<S7>/Check if stage at home' */
  int8_T Checkforampfault_SubsysRanBC; /* '<S7>/Check for amp fault' */
  int8_T TopCartPositionWatchdog_SubsysR;/* '<S1>/Top Cart Position Watchdog' */
  int8_T BottomCartPositionWatchdog_Subs;
                                      /* '<S1>/Bottom Cart Position Watchdog' */
  int8_T SwitchCase_ActiveSubsystem;   /* '<S15>/Switch Case' */
  int8_T SwitchCaseActionSubsystem2_Subs;
                                     /* '<S15>/Switch Case Action Subsystem2' */
  int8_T SwitchCaseActionSubsystem1_Subs;
                                     /* '<S15>/Switch Case Action Subsystem1' */
  int8_T SwitchCaseActionSubsystem_Subsy;
                                      /* '<S15>/Switch Case Action Subsystem' */
  int8_T EnabledMovingAverage_SubsysRanB;/* '<S15>/Enabled Moving Average' */
  uint8_T ContinuousSigmoid_Flag;      /* '<S10>/Continuous Sigmoid' */
  uint8_T ContinuousSigmoid_Flag_b;    /* '<S9>/Continuous Sigmoid' */
  boolean_T HILInitialize_DOBits[8];   /* '<S7>/HIL Initialize' */
  boolean_T HILWatchdog_IsStarted;     /* '<S35>/HIL Watchdog' */
  t_boolean HILWrite_DigitalBuffer[2]; /* '<S7>/HIL Write' */
  boolean_T RampSignal_MODE;           /* '<S10>/Ramp Signal' */
  boolean_T Checkifstageathome_MODE;   /* '<S7>/Check if stage at home' */
  boolean_T Checkforampfault_MODE;     /* '<S7>/Check for amp fault' */
  boolean_T EnabledMovingAverage_MODE; /* '<S15>/Enabled Moving Average' */
} DW_q_2xamd1_stII_q8_usb_T;

/* Continuous states (default storage) */
typedef struct {
  real_T Integrator1_CSTATE;           /* '<S51>/Integrator1' */
  real_T Integrator_CSTATE[8];         /* '<S63>/Integrator' */
  real_T Integrator1_CSTATE_j[3];      /* '<S16>/Integrator1' */
  real_T StateSpace_CSTATE[16];        /* '<S11>/State-Space' */
  real_T Integrator1_CSTATE_jt;        /* '<S52>/Integrator1' */
  real_T Integrator2_CSTATE[3];        /* '<S16>/Integrator2' */
  real_T Integrator2_CSTATE_i;         /* '<S51>/Integrator2' */
  real_T Integrator2_CSTATE_m;         /* '<S52>/Integrator2' */
  real_T Integrator_CSTATE_p;          /* '<S60>/Integrator' */
} X_q_2xamd1_stII_q8_usb_T;

/* State derivatives (default storage) */
typedef struct {
  real_T Integrator1_CSTATE;           /* '<S51>/Integrator1' */
  real_T Integrator_CSTATE[8];         /* '<S63>/Integrator' */
  real_T Integrator1_CSTATE_j[3];      /* '<S16>/Integrator1' */
  real_T StateSpace_CSTATE[16];        /* '<S11>/State-Space' */
  real_T Integrator1_CSTATE_jt;        /* '<S52>/Integrator1' */
  real_T Integrator2_CSTATE[3];        /* '<S16>/Integrator2' */
  real_T Integrator2_CSTATE_i;         /* '<S51>/Integrator2' */
  real_T Integrator2_CSTATE_m;         /* '<S52>/Integrator2' */
  real_T Integrator_CSTATE_p;          /* '<S60>/Integrator' */
} XDot_q_2xamd1_stII_q8_usb_T;

/* State disabled  */
typedef struct {
  boolean_T Integrator1_CSTATE;        /* '<S51>/Integrator1' */
  boolean_T Integrator_CSTATE[8];      /* '<S63>/Integrator' */
  boolean_T Integrator1_CSTATE_j[3];   /* '<S16>/Integrator1' */
  boolean_T StateSpace_CSTATE[16];     /* '<S11>/State-Space' */
  boolean_T Integrator1_CSTATE_jt;     /* '<S52>/Integrator1' */
  boolean_T Integrator2_CSTATE[3];     /* '<S16>/Integrator2' */
  boolean_T Integrator2_CSTATE_i;      /* '<S51>/Integrator2' */
  boolean_T Integrator2_CSTATE_m;      /* '<S52>/Integrator2' */
  boolean_T Integrator_CSTATE_p;       /* '<S60>/Integrator' */
} XDis_q_2xamd1_stII_q8_usb_T;

#ifndef ODE4_INTG
#define ODE4_INTG

/* ODE4 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[4];                        /* derivatives */
} ODE4_IntgData;

#endif

/* Backward compatible GRT Identifiers */
#define rtB                            q_2xamd1_stII_q8_usb_B
#define BlockIO                        B_q_2xamd1_stII_q8_usb_T
#define rtX                            q_2xamd1_stII_q8_usb_X
#define ContinuousStates               X_q_2xamd1_stII_q8_usb_T
#define rtXdot                         q_2xamd1_stII_q8_usb_XDot
#define StateDerivatives               XDot_q_2xamd1_stII_q8_usb_T
#define tXdis                          q_2xamd1_stII_q8_usb_XDis
#define StateDisabled                  XDis_q_2xamd1_stII_q8_usb_T
#define rtP                            q_2xamd1_stII_q8_usb_P
#define Parameters                     P_q_2xamd1_stII_q8_usb_T
#define rtDWork                        q_2xamd1_stII_q8_usb_DW
#define D_Work                         DW_q_2xamd1_stII_q8_usb_T

/* Parameters (default storage) */
struct P_q_2xamd1_stII_q8_usb_T_ {
  real_T A[64];                        /* Variable: A
                                        * Referenced by: '<S63>/Matrix Gain1'
                                        */
  real_T B[16];                        /* Variable: B
                                        * Referenced by: '<S63>/Matrix Gain'
                                        */
  real_T C[32];                        /* Variable: C
                                        * Referenced by: '<S63>/Matrix Gain2'
                                        */
  real_T D[8];                         /* Variable: D
                                        * Referenced by: '<S63>/Matrix Gain3'
                                        */
  real_T G[32];                        /* Variable: G
                                        * Referenced by: '<S63>/Matrix Gain4'
                                        */
  real_T K_AMD_AMP;                    /* Variable: K_AMD_AMP
                                        * Referenced by: '<S1>/Amplifier Gain Pre-Compensation'
                                        */
  real_T K_AMP;                        /* Variable: K_AMP
                                        * Referenced by: '<S48>/Inverse Amp  Gain (V//A)'
                                        */
  real_T K_EC;                         /* Variable: K_EC
                                        * Referenced by:
                                        *   '<S1>/Conversion to m1'
                                        *   '<S1>/Conversion to m2'
                                        */
  real_T K_ENC;                        /* Variable: K_ENC
                                        * Referenced by:
                                        *   '<S49>/Encoder Calibration (m//count)'
                                        *   '<S49>/counts//s to m//s'
                                        */
  real_T K_G2MS;                       /* Variable: K_G2MS
                                        * Referenced by:
                                        *   '<S1>/floor 1: g to m//s^1'
                                        *   '<S1>/floor 1: g to m//s^2'
                                        */
  real_T K_MS2G;                       /* Variable: K_MS2G
                                        * Referenced by: '<S49>/m//s^2 to g'
                                        */
  real_T Kamd[16];                     /* Variable: Kamd
                                        * Referenced by: '<S11>/State-Feedback  Gain Vector'
                                        */
  real_T Kp;                           /* Variable: Kp
                                        * Referenced by:
                                        *   '<S5>/Kp_c: 274.62'
                                        *   '<S6>/Kp_c: 274.62'
                                        */
  real_T Kv;                           /* Variable: Kv
                                        * Referenced by:
                                        *   '<S5>/Kv_c: 5.56'
                                        *   '<S6>/Kv_c: 5.56'
                                        */
  real_T P_MAX;                        /* Variable: P_MAX
                                        * Referenced by: '<S10>/Position Limit'
                                        */
  real_T SWEEP_MAX;                    /* Variable: SWEEP_MAX
                                        * Referenced by: '<S9>/Sweep Position  Limit (m)'
                                        */
  real_T VMAX_AMP_AMD;                 /* Variable: VMAX_AMP_AMD
                                        * Referenced by: '<Root>/Amplifier Voltage Saturation'
                                        */
  real_T XC_LIM_ENABLE;                /* Variable: XC_LIM_ENABLE
                                        * Referenced by:
                                        *   '<S1>/Constant1'
                                        *   '<S1>/Constant2'
                                        */
  real_T XC_MAX;                       /* Variable: XC_MAX
                                        * Referenced by:
                                        *   '<S13>/X MAX'
                                        *   '<S14>/X MAX'
                                        */
  real_T XC_MIN;                       /* Variable: XC_MIN
                                        * Referenced by:
                                        *   '<S13>/X IN'
                                        *   '<S14>/X IN'
                                        */
  real_T dbnc_rise_time;               /* Variable: dbnc_rise_time
                                        * Referenced by:
                                        *   '<S43>/debounce_rise_time'
                                        *   '<S44>/debounce_rise_time'
                                        */
  real_T dbnc_threshold;               /* Variable: dbnc_threshold
                                        * Referenced by:
                                        *   '<S43>/threshold'
                                        *   '<S44>/threshold'
                                        */
  real_T kd;                           /* Variable: kd
                                        * Referenced by: '<S47>/Derivative Gain (V-s//m)'
                                        */
  real_T kp;                           /* Variable: kp
                                        * Referenced by: '<S47>/Proportional Gain (V//m)'
                                        */
  real_T wa;                           /* Variable: wa
                                        * Referenced by:
                                        *   '<S16>/wn'
                                        *   '<S52>/wn'
                                        */
  real_T wd;                           /* Variable: wd
                                        * Referenced by: '<S51>/wn'
                                        */
  real_T za;                           /* Variable: za
                                        * Referenced by:
                                        *   '<S16>/zt'
                                        *   '<S52>/zt'
                                        */
  real_T zd;                           /* Variable: zd
                                        * Referenced by: '<S51>/zt'
                                        */
  real_T RepeatingChirp_A;             /* Mask Parameter: RepeatingChirp_A
                                        * Referenced by: '<S56>/Gain'
                                        */
  real_T RepeatingChirp_D;             /* Mask Parameter: RepeatingChirp_D
                                        * Referenced by:
                                        *   '<S56>/Constant4'
                                        *   '<S56>/Dead Zone'
                                        */
  real_T RepeatingChirp_T;             /* Mask Parameter: RepeatingChirp_T
                                        * Referenced by:
                                        *   '<S56>/Constant4'
                                        *   '<S56>/Gain1'
                                        */
  real_T After2sec_const;              /* Mask Parameter: After2sec_const
                                        * Referenced by: '<S30>/Constant'
                                        */
  real_T CompareToConstant_const;     /* Mask Parameter: CompareToConstant_const
                                       * Referenced by: '<S33>/Constant'
                                       */
  real_T Startat1sec_const;            /* Mask Parameter: Startat1sec_const
                                        * Referenced by: '<S61>/Constant'
                                        */
  real_T StallMonitor_control_threshold;
                               /* Mask Parameter: StallMonitor_control_threshold
                                * Referenced by: '<S54>/control threshold'
                                */
  real_T StallMonitor_duration_s;     /* Mask Parameter: StallMonitor_duration_s
                                       * Referenced by: '<S54>/control threshold1'
                                       */
  real_T BiasRemoval_end_time;         /* Mask Parameter: BiasRemoval_end_time
                                        * Referenced by: '<S15>/Step: end_time'
                                        */
  real_T RepeatingChirp_f1;            /* Mask Parameter: RepeatingChirp_f1
                                        * Referenced by:
                                        *   '<S56>/Constant2'
                                        *   '<S56>/Gain1'
                                        */
  real_T RepeatingChirp_f2;            /* Mask Parameter: RepeatingChirp_f2
                                        * Referenced by: '<S56>/Gain1'
                                        */
  real_T debouncerleft_fall_time;     /* Mask Parameter: debouncerleft_fall_time
                                       * Referenced by: '<S43>/debounce_fall_time'
                                       */
  real_T debouncerright_fall_time;   /* Mask Parameter: debouncerright_fall_time
                                      * Referenced by: '<S44>/debounce_fall_time'
                                      */
  real_T VelocitySetpointWeight_gain;
                                  /* Mask Parameter: VelocitySetpointWeight_gain
                                   * Referenced by: '<S62>/Slider Gain'
                                   */
  real_T VelocitySetpointWeight_gain_l;
                                /* Mask Parameter: VelocitySetpointWeight_gain_l
                                 * Referenced by: '<S58>/Slider Gain'
                                 */
  real_T Amplitudecm_gain;             /* Mask Parameter: Amplitudecm_gain
                                        * Referenced by: '<S2>/Slider Gain'
                                        */
  real_T FrequencyHz_gain;             /* Mask Parameter: FrequencyHz_gain
                                        * Referenced by: '<S4>/Slider Gain'
                                        */
  real_T StallMonitor_motion_threshold;
                                /* Mask Parameter: StallMonitor_motion_threshold
                                 * Referenced by: '<S54>/motion threshold'
                                 */
  real_T BiasRemoval_start_time;       /* Mask Parameter: BiasRemoval_start_time
                                        * Referenced by: '<S15>/Step: start_time'
                                        */
  real_T BiasRemoval_switch_id;        /* Mask Parameter: BiasRemoval_switch_id
                                        * Referenced by: '<S15>/Constant'
                                        */
  uint32_T HILWrite_analog_channels[3];
                                     /* Mask Parameter: HILWrite_analog_channels
                                      * Referenced by: '<S7>/HIL Write'
                                      */
  uint32_T HILWrite_digital_channels[2];
                                    /* Mask Parameter: HILWrite_digital_channels
                                     * Referenced by: '<S7>/HIL Write'
                                     */
  int8_T StopwithMessage_message_icon;
                                 /* Mask Parameter: StopwithMessage_message_icon
                                  * Referenced by: '<S22>/Show Message on Host'
                                  */
  int8_T StopwithMessage1_message_icon;
                                /* Mask Parameter: StopwithMessage1_message_icon
                                 * Referenced by: '<S23>/Show Message on Host'
                                 */
  int8_T StopwithMessage_message_icon_i;
                               /* Mask Parameter: StopwithMessage_message_icon_i
                                * Referenced by: '<S26>/Show Message on Host'
                                */
  int8_T StopwithMessage1_message_icon_e;
                              /* Mask Parameter: StopwithMessage1_message_icon_e
                               * Referenced by: '<S27>/Show Message on Host'
                               */
  int8_T StopwithMessageAmpFault_message;
                              /* Mask Parameter: StopwithMessageAmpFault_message
                               * Referenced by: '<S39>/Show Message on Host'
                               */
  int8_T StopwithMessage_message_icon_e;
                               /* Mask Parameter: StopwithMessage_message_icon_e
                                * Referenced by: '<S41>/Show Message on Host'
                                */
  int8_T StopwithMessageLeftLimit_messag;
                              /* Mask Parameter: StopwithMessageLeftLimit_messag
                               * Referenced by: '<S37>/Show Message on Host'
                               */
  int8_T StopwithMessageRightLimit_messa;
                              /* Mask Parameter: StopwithMessageRightLimit_messa
                               * Referenced by: '<S38>/Show Message on Host'
                               */
  int8_T Encodernotreading_message_icon;
                               /* Mask Parameter: Encodernotreading_message_icon
                                * Referenced by: '<S53>/Show Message on Host'
                                */
  real_T unity_Value;                  /* Expression: 1
                                        * Referenced by: '<S21>/unity'
                                        */
  real_T UnitDelay_InitialCondition;   /* Expression: 0
                                        * Referenced by: '<S21>/Unit Delay'
                                        */
  real_T Sumk1n1xk_InitialCondition;   /* Expression: 0
                                        * Referenced by: '<S17>/Sum( k=1,n-1, x(k) )'
                                        */
  real_T zero_Y0;                      /* Expression: [0]
                                        * Referenced by: '<S18>/zero'
                                        */
  real_T Constant_Value;               /* Expression: 0
                                        * Referenced by: '<S18>/Constant'
                                        */
  real_T Vbiased_Y0;                   /* Expression: [0]
                                        * Referenced by: '<S19>/Vbiased'
                                        */
  real_T Vunbiased_Y0;                 /* Expression: [0]
                                        * Referenced by: '<S20>/Vunbiased'
                                        */
  real_T Stepstart_time_Y0;            /* Expression: 0
                                        * Referenced by: '<S15>/Step: start_time'
                                        */
  real_T Stepstart_time_YFinal;        /* Expression: 1
                                        * Referenced by: '<S15>/Step: start_time'
                                        */
  real_T Stepend_time_Y0;              /* Expression: 0
                                        * Referenced by: '<S15>/Step: end_time'
                                        */
  real_T Stepend_time_YFinal;          /* Expression: 1
                                        * Referenced by: '<S15>/Step: end_time'
                                        */
  real_T Constant_Value_g;             /* Expression: 1
                                        * Referenced by: '<S60>/Constant'
                                        */
  real_T Integrator_IC;                /* Expression: 0
                                        * Referenced by: '<S60>/Integrator'
                                        */
  real_T Gain_Gain;                    /* Expression: 2
                                        * Referenced by: '<S60>/Gain'
                                        */
  real_T On_Value;                     /* Expression: 2
                                        * Referenced by: '<Root>/On'
                                        */
  real_T Off_Value;                    /* Expression: 1
                                        * Referenced by: '<Root>/Off'
                                        */
  real_T PositionSetpointm_Value;      /* Expression: 0
                                        * Referenced by: '<S3>/Position Setpoint (m)'
                                        */
  real_T VelocitySetpointms_Value;     /* Expression: 0
                                        * Referenced by: '<S3>/Velocity Setpoint (m//s)'
                                        */
  real_T HILInitialize_OOTerminate;/* Expression: set_other_outputs_at_terminate
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  real_T HILInitialize_OOExit;    /* Expression: set_other_outputs_at_switch_out
                                   * Referenced by: '<S7>/HIL Initialize'
                                   */
  real_T HILInitialize_OOStart;        /* Expression: set_other_outputs_at_start
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_OOEnter;    /* Expression: set_other_outputs_at_switch_in
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  real_T HILInitialize_AOFinal;        /* Expression: final_analog_outputs
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_POFinal;        /* Expression: final_pwm_outputs
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_AIHigh;         /* Expression: analog_input_maximums
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_AILow;          /* Expression: analog_input_minimums
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_AOHigh;         /* Expression: analog_output_maximums
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_AOLow;          /* Expression: analog_output_minimums
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_AOInitial;      /* Expression: initial_analog_outputs
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_AOWatchdog;     /* Expression: watchdog_analog_outputs
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_POFrequency;    /* Expression: pwm_frequency
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_POLeading;      /* Expression: pwm_leading_deadband
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_POTrailing;     /* Expression: pwm_trailing_deadband
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_POInitial;      /* Expression: initial_pwm_outputs
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T HILInitialize_POWatchdog;     /* Expression: watchdog_pwm_outputs
                                        * Referenced by: '<S7>/HIL Initialize'
                                        */
  real_T To_Samples_Gain;              /* Expression: 1/qc_get_step_size
                                        * Referenced by: '<S43>/To_Samples'
                                        */
  real_T To_Samples1_Gain;             /* Expression: 1/qc_get_step_size
                                        * Referenced by: '<S43>/To_Samples1'
                                        */
  real_T debouncer_block_init_state;   /* Expression: init_state
                                        * Referenced by: '<S43>/debouncer_block'
                                        */
  real_T To_Samples_Gain_c;            /* Expression: 1/qc_get_step_size
                                        * Referenced by: '<S44>/To_Samples'
                                        */
  real_T To_Samples1_Gain_n;           /* Expression: 1/qc_get_step_size
                                        * Referenced by: '<S44>/To_Samples1'
                                        */
  real_T debouncer_block_init_state_c; /* Expression: init_state
                                        * Referenced by: '<S44>/debouncer_block'
                                        */
  real_T ShakerIISetpointSelection_Value;/* Expression: 3
                                          * Referenced by: '<Root>/Shaker II Setpoint Selection'
                                          */
  real_T ContinuousSigmoid_XI;         /* Expression: initial_position
                                        * Referenced by: '<S10>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_VI;         /* Expression: initial_velocity
                                        * Referenced by: '<S10>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_Hold;       /* Expression: hold_off
                                        * Referenced by: '<S10>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_VMax;       /* Expression: i_max_velocity
                                        * Referenced by: '<S10>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_AMax;       /* Expression: i_max_acceleration
                                        * Referenced by: '<S10>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_XI_c;       /* Expression: initial_position
                                        * Referenced by: '<S9>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_VI_h;       /* Expression: initial_velocity
                                        * Referenced by: '<S9>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_Hold_e;     /* Expression: hold_off
                                        * Referenced by: '<S9>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_VMax_j;     /* Expression: i_max_velocity
                                        * Referenced by: '<S9>/Continuous Sigmoid'
                                        */
  real_T ContinuousSigmoid_AMax_a;     /* Expression: i_max_acceleration
                                        * Referenced by: '<S9>/Continuous Sigmoid'
                                        */
  real_T x0_Value;                     /* Expression: input_init
                                        * Referenced by: '<S51>/x0'
                                        */
  real_T VoltageLimit_PLim;            /* Expression: i_peak_lim
                                        * Referenced by: '<S48>/Voltage Limit'
                                        */
  real_T VoltageLimit_PTime;           /* Expression: i_peak_time_lim
                                        * Referenced by: '<S48>/Voltage Limit'
                                        */
  real_T VoltageLimit_CLim;            /* Expression: i_cont_lim
                                        * Referenced by: '<S48>/Voltage Limit'
                                        */
  real_T VoltageLimit_CTime;           /* Expression: i_cont_hold_time
                                        * Referenced by: '<S48>/Voltage Limit'
                                        */
  real_T Constant_Value_g3;            /* Expression: 0
                                        * Referenced by: '<S5>/Constant'
                                        */
  real_T Integrator_IC_e;              /* Expression: 0
                                        * Referenced by: '<S63>/Integrator'
                                        */
  real_T Constant_Value_e;             /* Expression: 0
                                        * Referenced by: '<S6>/Constant'
                                        */
  real_T HILWatchdog_Timeout;          /* Expression: timeout
                                        * Referenced by: '<S35>/HIL Watchdog'
                                        */
  real_T x0_Value_f;                   /* Expression: input_init
                                        * Referenced by: '<S16>/x0'
                                        */
  real_T Foraccelerationtobesamedirectio;/* Expression: -1
                                          * Referenced by: '<S12>/For acceleration to be same direction as position'
                                          */
  real_T StateSpace_A_pr[256];         /* Computed Parameter: StateSpace_A_pr
                                        * Referenced by: '<S11>/State-Space'
                                        */
  real_T StateSpace_B_pr[64];          /* Computed Parameter: StateSpace_B_pr
                                        * Referenced by: '<S11>/State-Space'
                                        */
  real_T StateSpace_C_pr[32];          /* Computed Parameter: StateSpace_C_pr
                                        * Referenced by: '<S11>/State-Space'
                                        */
  real_T StateSpace_InitialCondition;  /* Expression: 0
                                        * Referenced by: '<S11>/State-Space'
                                        */
  real_T mtocm_Gain;                   /* Expression: 100
                                        * Referenced by: '<Root>/m to cm'
                                        */
  real_T x0_Value_h;                   /* Expression: input_init
                                        * Referenced by: '<S52>/x0'
                                        */
  real_T mtomm_Gain;                   /* Expression: 1000
                                        * Referenced by: '<Root>/m to mm'
                                        */
  real_T mtomm1_Gain;                  /* Expression: 1000
                                        * Referenced by: '<Root>/m to mm1'
                                        */
  real_T Constant_Value_b;             /* Expression: 2
                                        * Referenced by: '<S16>/Constant'
                                        */
  real_T Integrator2_IC;               /* Expression: 0
                                        * Referenced by: '<S16>/Integrator2'
                                        */
  real_T Constant_Value_a;             /* Expression: 1
                                        * Referenced by: '<Root>/Constant'
                                        */
  real_T Kp_c2741_Gain;                /* Expression:  1e3
                                        * Referenced by: '<S5>/Kp_c: 274.1'
                                        */
  real_T Kp_c2741_Gain_m;              /* Expression:  1e3
                                        * Referenced by: '<S6>/Kp_c: 274.1'
                                        */
  real_T DiscreteTimeIntegrator_gainval;
                           /* Computed Parameter: DiscreteTimeIntegrator_gainval
                            * Referenced by: '<S54>/Discrete-Time Integrator'
                            */
  real_T DiscreteTimeIntegrator_IC;    /* Expression: 0
                                        * Referenced by: '<S54>/Discrete-Time Integrator'
                                        */
  real_T Constant_Value_d;             /* Expression: 2
                                        * Referenced by: '<S51>/Constant'
                                        */
  real_T Integrator2_IC_i;             /* Expression: 0
                                        * Referenced by: '<S51>/Integrator2'
                                        */
  real_T Constant_Value_h;             /* Expression: 2
                                        * Referenced by: '<S52>/Constant'
                                        */
  real_T Integrator2_IC_c;             /* Expression: 0
                                        * Referenced by: '<S52>/Integrator2'
                                        */
  real_T UPMInitTimes_Value;           /* Expression: 1.0
                                        * Referenced by: '<S57>/UPM Init Time (s)'
                                        */
  real_T DiscreteTransferFcn_NumCoef[2];/* Expression: numz
                                         * Referenced by: '<S59>/Discrete Transfer Fcn'
                                         */
  real_T DiscreteTransferFcn_DenCoef[2];/* Expression: denz
                                         * Referenced by: '<S59>/Discrete Transfer Fcn'
                                         */
  real_T DiscreteTransferFcn_InitialStat;/* Expression: 0
                                          * Referenced by: '<S59>/Discrete Transfer Fcn'
                                          */
  real_T Constant3_Value;              /* Expression: pi
                                        * Referenced by: '<S56>/Constant3'
                                        */
  real_T DeadZone_Start;               /* Expression: 0
                                        * Referenced by: '<S56>/Dead Zone'
                                        */
  real_T SineWaveFunction_Amp;         /* Expression: 1
                                        * Referenced by: '<S56>/Sine Wave Function'
                                        */
  real_T SineWaveFunction_Bias;        /* Expression: 0
                                        * Referenced by: '<S56>/Sine Wave Function'
                                        */
  real_T SineWaveFunction_Freq;        /* Expression: 1
                                        * Referenced by: '<S56>/Sine Wave Function'
                                        */
  real_T SineWaveFunction_Phase;       /* Expression: 0
                                        * Referenced by: '<S56>/Sine Wave Function'
                                        */
  real_T SineSweepAmplitudem_Gain;     /* Expression: 7.5e-3
                                        * Referenced by: '<S9>/Sine Sweep  Amplitude (m)'
                                        */
  real_T SmoothSignalGenerator_InitialPh;/* Expression: initial_phase
                                          * Referenced by: '<S10>/Smooth Signal Generator'
                                          */
  real_T SmoothSignalGenerator_Amplitude;/* Expression: i_amplitude
                                          * Referenced by: '<S10>/Smooth Signal Generator'
                                          */
  real_T SmoothSignalGenerator_Frequency;/* Expression: i_frequency
                                          * Referenced by: '<S10>/Smooth Signal Generator'
                                          */
  real_T cmtom_Gain;                   /* Expression: 1/100
                                        * Referenced by: '<S10>/cm to m'
                                        */
  int32_T HILInitialize_CKChannels[3];
                                 /* Computed Parameter: HILInitialize_CKChannels
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  int32_T HILInitialize_DOWatchdog;
                                 /* Computed Parameter: HILInitialize_DOWatchdog
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  int32_T HILInitialize_EIInitial;/* Computed Parameter: HILInitialize_EIInitial
                                   * Referenced by: '<S7>/HIL Initialize'
                                   */
  int32_T HILInitialize_POModes;    /* Computed Parameter: HILInitialize_POModes
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  int32_T HILInitialize_POConfiguration;
                            /* Computed Parameter: HILInitialize_POConfiguration
                             * Referenced by: '<S7>/HIL Initialize'
                             */
  int32_T HILInitialize_POAlignment;
                                /* Computed Parameter: HILInitialize_POAlignment
                                 * Referenced by: '<S7>/HIL Initialize'
                                 */
  int32_T HILInitialize_POPolarity;
                                 /* Computed Parameter: HILInitialize_POPolarity
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  int32_T HILReadTimebase_Clock;    /* Computed Parameter: HILReadTimebase_Clock
                                     * Referenced by: '<S7>/HIL Read Timebase'
                                     */
  uint32_T HILInitialize_AIChannels[8];
                                 /* Computed Parameter: HILInitialize_AIChannels
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  uint32_T HILInitialize_AOChannels[8];
                                 /* Computed Parameter: HILInitialize_AOChannels
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  uint32_T HILInitialize_DIChannels[5];
                                 /* Computed Parameter: HILInitialize_DIChannels
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  uint32_T HILInitialize_DOChannels[8];
                                 /* Computed Parameter: HILInitialize_DOChannels
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  uint32_T HILInitialize_EIChannels[8];
                                 /* Computed Parameter: HILInitialize_EIChannels
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  uint32_T HILInitialize_EIQuadrature;
                               /* Computed Parameter: HILInitialize_EIQuadrature
                                * Referenced by: '<S7>/HIL Initialize'
                                */
  uint32_T HILInitialize_POChannels[8];
                                 /* Computed Parameter: HILInitialize_POChannels
                                  * Referenced by: '<S7>/HIL Initialize'
                                  */
  uint32_T HILReadTimebase_SamplesInBuffer;
                          /* Computed Parameter: HILReadTimebase_SamplesInBuffer
                           * Referenced by: '<S7>/HIL Read Timebase'
                           */
  uint32_T HILReadTimebase_AnalogChannels[3];
                           /* Computed Parameter: HILReadTimebase_AnalogChannels
                            * Referenced by: '<S7>/HIL Read Timebase'
                            */
  uint32_T HILReadTimebase_EncoderChannels[3];
                          /* Computed Parameter: HILReadTimebase_EncoderChannels
                           * Referenced by: '<S7>/HIL Read Timebase'
                           */
  uint32_T HILReadTimebase_DigitalChannels[5];
                          /* Computed Parameter: HILReadTimebase_DigitalChannels
                           * Referenced by: '<S7>/HIL Read Timebase'
                           */
  uint32_T HILReadTimebase_OtherChannels;
                            /* Computed Parameter: HILReadTimebase_OtherChannels
                             * Referenced by: '<S7>/HIL Read Timebase'
                             */
  uint32_T StateSpace_A_ir[256];       /* Computed Parameter: StateSpace_A_ir
                                        * Referenced by: '<S11>/State-Space'
                                        */
  uint32_T StateSpace_A_jc[17];        /* Computed Parameter: StateSpace_A_jc
                                        * Referenced by: '<S11>/State-Space'
                                        */
  uint32_T StateSpace_B_ir[64];        /* Computed Parameter: StateSpace_B_ir
                                        * Referenced by: '<S11>/State-Space'
                                        */
  uint32_T StateSpace_B_jc[5];         /* Computed Parameter: StateSpace_B_jc
                                        * Referenced by: '<S11>/State-Space'
                                        */
  uint32_T StateSpace_C_ir[32];        /* Computed Parameter: StateSpace_C_ir
                                        * Referenced by: '<S11>/State-Space'
                                        */
  uint32_T StateSpace_C_jc[17];        /* Computed Parameter: StateSpace_C_jc
                                        * Referenced by: '<S11>/State-Space'
                                        */
  uint32_T ToHostFile_Decimation;   /* Computed Parameter: ToHostFile_Decimation
                                     * Referenced by: '<Root>/To Host File'
                                     */
  uint32_T ToHostFile_BitRate;         /* Computed Parameter: ToHostFile_BitRate
                                        * Referenced by: '<Root>/To Host File'
                                        */
  boolean_T Constant_Value_h2;         /* Computed Parameter: Constant_Value_h2
                                        * Referenced by: '<S24>/Constant'
                                        */
  boolean_T Constant_Value_gt;         /* Computed Parameter: Constant_Value_gt
                                        * Referenced by: '<S25>/Constant'
                                        */
  boolean_T Constant_Value_p;          /* Computed Parameter: Constant_Value_p
                                        * Referenced by: '<S28>/Constant'
                                        */
  boolean_T Constant_Value_k;          /* Computed Parameter: Constant_Value_k
                                        * Referenced by: '<S29>/Constant'
                                        */
  boolean_T Constant_Value_l;          /* Computed Parameter: Constant_Value_l
                                        * Referenced by: '<S40>/Constant'
                                        */
  boolean_T Constant_Value_c;          /* Computed Parameter: Constant_Value_c
                                        * Referenced by: '<S42>/Constant'
                                        */
  boolean_T Constant_Value_kw;         /* Computed Parameter: Constant_Value_kw
                                        * Referenced by: '<S45>/Constant'
                                        */
  boolean_T Constant_Value_c1;         /* Computed Parameter: Constant_Value_c1
                                        * Referenced by: '<S46>/Constant'
                                        */
  boolean_T Constant_Value_di;         /* Computed Parameter: Constant_Value_di
                                        * Referenced by: '<S55>/Constant'
                                        */
  boolean_T HILInitialize_Active;    /* Computed Parameter: HILInitialize_Active
                                      * Referenced by: '<S7>/HIL Initialize'
                                      */
  boolean_T HILInitialize_AOTerminate;
                                /* Computed Parameter: HILInitialize_AOTerminate
                                 * Referenced by: '<S7>/HIL Initialize'
                                 */
  boolean_T HILInitialize_AOExit;    /* Computed Parameter: HILInitialize_AOExit
                                      * Referenced by: '<S7>/HIL Initialize'
                                      */
  boolean_T HILInitialize_DOTerminate;
                                /* Computed Parameter: HILInitialize_DOTerminate
                                 * Referenced by: '<S7>/HIL Initialize'
                                 */
  boolean_T HILInitialize_DOExit;    /* Computed Parameter: HILInitialize_DOExit
                                      * Referenced by: '<S7>/HIL Initialize'
                                      */
  boolean_T HILInitialize_POTerminate;
                                /* Computed Parameter: HILInitialize_POTerminate
                                 * Referenced by: '<S7>/HIL Initialize'
                                 */
  boolean_T HILInitialize_POExit;    /* Computed Parameter: HILInitialize_POExit
                                      * Referenced by: '<S7>/HIL Initialize'
                                      */
  boolean_T HILInitialize_CKPStart;/* Computed Parameter: HILInitialize_CKPStart
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_CKPEnter;/* Computed Parameter: HILInitialize_CKPEnter
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_CKStart;  /* Computed Parameter: HILInitialize_CKStart
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_CKEnter;  /* Computed Parameter: HILInitialize_CKEnter
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_AIPStart;/* Computed Parameter: HILInitialize_AIPStart
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_AIPEnter;/* Computed Parameter: HILInitialize_AIPEnter
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_AOPStart;/* Computed Parameter: HILInitialize_AOPStart
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_AOPEnter;/* Computed Parameter: HILInitialize_AOPEnter
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_AOStart;  /* Computed Parameter: HILInitialize_AOStart
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_AOEnter;  /* Computed Parameter: HILInitialize_AOEnter
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_AOReset;  /* Computed Parameter: HILInitialize_AOReset
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_DOPStart;/* Computed Parameter: HILInitialize_DOPStart
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_DOPEnter;/* Computed Parameter: HILInitialize_DOPEnter
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_DOStart;  /* Computed Parameter: HILInitialize_DOStart
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_DOEnter;  /* Computed Parameter: HILInitialize_DOEnter
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_DOReset;  /* Computed Parameter: HILInitialize_DOReset
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_EIPStart;/* Computed Parameter: HILInitialize_EIPStart
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_EIPEnter;/* Computed Parameter: HILInitialize_EIPEnter
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_EIStart;  /* Computed Parameter: HILInitialize_EIStart
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_EIEnter;  /* Computed Parameter: HILInitialize_EIEnter
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_POPStart;/* Computed Parameter: HILInitialize_POPStart
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_POPEnter;/* Computed Parameter: HILInitialize_POPEnter
                                    * Referenced by: '<S7>/HIL Initialize'
                                    */
  boolean_T HILInitialize_POStart;  /* Computed Parameter: HILInitialize_POStart
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_POEnter;  /* Computed Parameter: HILInitialize_POEnter
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_POReset;  /* Computed Parameter: HILInitialize_POReset
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_OOReset;  /* Computed Parameter: HILInitialize_OOReset
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_DOFinal;  /* Computed Parameter: HILInitialize_DOFinal
                                     * Referenced by: '<S7>/HIL Initialize'
                                     */
  boolean_T HILInitialize_DOInitial;
                                  /* Computed Parameter: HILInitialize_DOInitial
                                   * Referenced by: '<S7>/HIL Initialize'
                                   */
  boolean_T HILReadTimebase_Active;/* Computed Parameter: HILReadTimebase_Active
                                    * Referenced by: '<S7>/HIL Read Timebase'
                                    */
  boolean_T CalibrateSignalOFF_Value;
                                 /* Computed Parameter: CalibrateSignalOFF_Value
                                  * Referenced by: '<S34>/Calibrate Signal = OFF'
                                  */
  boolean_T EnableSignalON_Value;    /* Computed Parameter: EnableSignalON_Value
                                      * Referenced by: '<S34>/Enable Signal = ON'
                                      */
  boolean_T HILWrite_Active;           /* Computed Parameter: HILWrite_Active
                                        * Referenced by: '<S7>/HIL Write'
                                        */
  boolean_T HILWatchdog_Active;        /* Computed Parameter: HILWatchdog_Active
                                        * Referenced by: '<S35>/HIL Watchdog'
                                        */
  uint8_T HILReadTimebase_OverflowMode;
                             /* Computed Parameter: HILReadTimebase_OverflowMode
                              * Referenced by: '<S7>/HIL Read Timebase'
                              */
  uint8_T AMDControlMode_CurrentSetting;
                            /* Computed Parameter: AMDControlMode_CurrentSetting
                             * Referenced by: '<Root>/AMD Control Mode'
                             */
  uint8_T ControlMode_CurrentSetting;
                               /* Computed Parameter: ControlMode_CurrentSetting
                                * Referenced by: '<S11>/Control Mode'
                                */
  uint8_T ToHostFile_file_name[29];    /* Expression: file_name_argument
                                        * Referenced by: '<Root>/To Host File'
                                        */
  uint8_T ToHostFile_VarName[5];       /* Expression: variable_name_argument
                                        * Referenced by: '<Root>/To Host File'
                                        */
  uint8_T ToHostFile_FileFormat;    /* Computed Parameter: ToHostFile_FileFormat
                                     * Referenced by: '<Root>/To Host File'
                                     */
};

/* Real-time Model Data Structure */
struct tag_RTM_q_2xamd1_stII_q8_usb_T {
  const char_T *path;
  const char_T *modelName;
  struct SimStruct_tag * *childSfunctions;
  const char_T *errorStatus;
  SS_SimMode simMode;
  RTWExtModeInfo *extModeInfo;
  RTWSolverInfo solverInfo;
  RTWSolverInfo *solverInfoPtr;
  void *sfcnInfo;
  void *blockIO;
  const void *constBlockIO;
  void *defaultParam;
  ZCSigState *prevZCSigState;
  real_T *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  void *zcSignalValues;
  void *inputs;
  void *outputs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T CTOutputIncnstWithState;
  real_T odeY[35];
  real_T odeF[4][35];
  ODE4_IntgData intgData;
  void *dwork;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
    uint32_T options;
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numU;
    int_T numY;
    int_T numSampTimes;
    int_T numBlocks;
    int_T numBlockIO;
    int_T numBlockPrms;
    int_T numDwork;
    int_T numSFcnPrms;
    int_T numSFcns;
    int_T numIports;
    int_T numOports;
    int_T numNonSampZCs;
    int_T sysDirFeedThru;
    int_T rtwGenSfcn;
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
    void *xpcData;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T stepSize;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T stepSize1;
    boolean_T firstInitCondFlag;
    time_T tStart;
    time_T tFinal;
    time_T timeOfLastOutput;
    void *timingData;
    real_T *varNextHitTimesList;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *sampleTimes;
    time_T *offsetTimes;
    int_T *sampleTimeTaskIDPtr;
    int_T *sampleHits;
    int_T *perTaskSampleHits;
    time_T *t;
    time_T sampleTimesArray[2];
    time_T offsetTimesArray[2];
    int_T sampleTimeTaskIDArray[2];
    int_T sampleHitArray[2];
    int_T perTaskSampleHitsArray[4];
    time_T tArray[2];
  } Timing;
};

/* Block parameters (default storage) */
extern P_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_P;

/* Block signals (default storage) */
extern B_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_B;

/* Continuous states (default storage) */
extern X_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_X;

/* Block states (default storage) */
extern DW_q_2xamd1_stII_q8_usb_T q_2xamd1_stII_q8_usb_DW;

/* Model entry point functions */
extern void q_2xamd1_stII_q8_usb_initialize(void);
extern void q_2xamd1_stII_q8_usb_output(void);
extern void q_2xamd1_stII_q8_usb_update(void);
extern void q_2xamd1_stII_q8_usb_terminate(void);

/*====================*
 * External functions *
 *====================*/
extern q_2xamd1_stII_q8_usb_rtModel *q_2xamd1_stII_q8_usb(void);
extern void MdlInitializeSizes(void);
extern void MdlInitializeSampleTimes(void);
extern void MdlInitialize(void);
extern void MdlStart(void);
extern void MdlOutputs(int_T tid);
extern void MdlUpdate(int_T tid);
extern void MdlTerminate(void);

/* Real-time Model object */
extern RT_MODEL_q_2xamd1_stII_q8_usb_T *const q_2xamd1_stII_q8_usb_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'q_2xamd1_stII_q8_usb'
 * '<S1>'   : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers '
 * '<S2>'   : 'q_2xamd1_stII_q8_usb/Amplitude (cm)'
 * '<S3>'   : 'q_2xamd1_stII_q8_usb/Constant Setpoint'
 * '<S4>'   : 'q_2xamd1_stII_q8_usb/Frequency (Hz)'
 * '<S5>'   : 'q_2xamd1_stII_q8_usb/PV Position Controller:  Cart 1 (first floor)'
 * '<S6>'   : 'q_2xamd1_stII_q8_usb/PV Position Controller:  Cart 2 (2nd floor)'
 * '<S7>'   : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface'
 * '<S8>'   : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System'
 * '<S9>'   : 'q_2xamd1_stII_q8_usb/Sine Sweep Setpoint'
 * '<S10>'  : 'q_2xamd1_stII_q8_usb/Smooth Sine Setpoint'
 * '<S11>'  : 'q_2xamd1_stII_q8_usb/Subsystem'
 * '<S12>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers'
 * '<S13>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Bottom Cart Position Watchdog'
 * '<S14>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Top Cart Position Watchdog'
 * '<S15>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers/Bias Removal'
 * '<S16>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers/Second-Order Low-Pass Filter'
 * '<S17>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers/Bias Removal/Enabled Moving Average'
 * '<S18>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers/Bias Removal/Switch Case Action Subsystem'
 * '<S19>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers/Bias Removal/Switch Case Action Subsystem1'
 * '<S20>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers/Bias Removal/Switch Case Action Subsystem2'
 * '<S21>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Accelerometers/Bias Removal/Enabled Moving Average/Increment'
 * '<S22>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Bottom Cart Position Watchdog/Stop with Message'
 * '<S23>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Bottom Cart Position Watchdog/Stop with Message1'
 * '<S24>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Bottom Cart Position Watchdog/Stop with Message/Compare'
 * '<S25>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Bottom Cart Position Watchdog/Stop with Message1/Compare'
 * '<S26>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Top Cart Position Watchdog/Stop with Message'
 * '<S27>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Top Cart Position Watchdog/Stop with Message1'
 * '<S28>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Top Cart Position Watchdog/Stop with Message/Compare'
 * '<S29>'  : 'q_2xamd1_stII_q8_usb/AMD-2-E w// Accelerometers /Top Cart Position Watchdog/Stop with Message1/Compare'
 * '<S30>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/After 2 sec'
 * '<S31>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Check for amp fault'
 * '<S32>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Check if stage at home'
 * '<S33>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Compare To Constant'
 * '<S34>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Enable Amplifier'
 * '<S35>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/HIL STII Watchdog'
 * '<S36>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Limit Switches'
 * '<S37>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Stop with Message: Left Limit'
 * '<S38>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Stop with Message: Right Limit'
 * '<S39>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Check for amp fault/Stop with Message: Amp Fault'
 * '<S40>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Check for amp fault/Stop with Message: Amp Fault/Compare'
 * '<S41>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Check if stage at home/Stop with Message'
 * '<S42>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Check if stage at home/Stop with Message/Compare'
 * '<S43>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Limit Switches/debouncer: left'
 * '<S44>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Limit Switches/debouncer: right'
 * '<S45>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Stop with Message: Left Limit/Compare'
 * '<S46>'  : 'q_2xamd1_stII_q8_usb/STII + 2xAMD-1 Interface/Stop with Message: Right Limit/Compare'
 * '<S47>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Position Controller'
 * '<S48>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II '
 * '<S49>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II /Encoder'
 * '<S50>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II /Encoder Check'
 * '<S51>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II /Encoder/Second-Order Low-Pass Filter'
 * '<S52>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II /Encoder/Second-Order Low-Pass Filter1'
 * '<S53>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II /Encoder Check/Encoder not reading'
 * '<S54>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II /Encoder Check/Stall Monitor'
 * '<S55>'  : 'q_2xamd1_stII_q8_usb/Shake Table II - Control System/Shake Table II /Encoder Check/Encoder not reading/Compare'
 * '<S56>'  : 'q_2xamd1_stII_q8_usb/Sine Sweep Setpoint/Repeating Chirp'
 * '<S57>'  : 'q_2xamd1_stII_q8_usb/Sine Sweep Setpoint/Setpoint Delay'
 * '<S58>'  : 'q_2xamd1_stII_q8_usb/Sine Sweep Setpoint/Velocity Setpoint  Weight'
 * '<S59>'  : 'q_2xamd1_stII_q8_usb/Sine Sweep Setpoint/Setpoint Delay/Discretized Transfer Function'
 * '<S60>'  : 'q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Ramp Signal'
 * '<S61>'  : 'q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Start at 1 sec'
 * '<S62>'  : 'q_2xamd1_stII_q8_usb/Smooth Sine Setpoint/Velocity Setpoint  Weight'
 * '<S63>'  : 'q_2xamd1_stII_q8_usb/Subsystem/Full-Order Observer'
 */
#endif                                 /* RTW_HEADER_q_2xamd1_stII_q8_usb_h_ */
